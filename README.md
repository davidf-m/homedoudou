# README #

### HOMEDOUDOU ###

Le site de domotique qu'il vous faut!

### Installation

installez meteor

https://www.meteor.com/

dans le dossier du projet dans un terminal tapez: meteor

puis rendez-vous  l'adresse

http://localhost:3000

### About

Projet de domotique utilisant le framework javascript Meteor, un framework temps réel.

![group_img](public/git/group.jpg)

![scenarios_img](public/git/scenarios.jpg)
