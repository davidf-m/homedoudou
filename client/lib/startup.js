import { Random } from 'meteor/random';
import { Cookies } from 'meteor/ostrio:cookies';

Meteor.subscribe('usersConnections');

global.SHOW_CONNECTION_ISSUE_KEY = 'showConnectionIssue';

Session.setDefault(SHOW_CONNECTION_ISSUE_KEY, false);

const CONNECTION_ISSUE_TIMEOUT = 5000;

// appHd.hardwareId :  un identifiant unique par cookies pour permettre une config personalisé par appareil
const cookies = new Cookies();
if (typeof appHd === 'undefined') appHd = {};
appHd.hardwareId = cookies.get("hardwareId");
if (!appHd.hardwareId)
  cookies.set("hardwareId", Random.id())
appHd.hardwareId = cookies.get("hardwareId");

Meteor.startup(function () {
  // Only show the connection error box if it has been 5 seconds since
  // the app started
  setTimeout(function () {
    // Show the connection error box
    Session.set(SHOW_CONNECTION_ISSUE_KEY, true);
  }, CONNECTION_ISSUE_TIMEOUT);

  // serviceWorker.js
  // navigator.serviceWorker
  //   .register('/sw.js')
  //   .then(() => console.info('service worker registered'))
  //   .catch(error => {
  //     console.log('ServiceWorker registration failed: ', error)
  //   })

});
