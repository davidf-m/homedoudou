// NAMESPACE
if(typeof (appHd) === "undefined") appHd = {};

Template.registerHelper('aliasNameFromHardId', function (hard_id) {
  const Hard = Hardwares.findOne(hard_id);
  if(Hard && Hard.alias)
    return Hard.alias;
  return 'John Doe';
});

Template.registerHelper('getHardOne', function (hard_id) {
  return Hardwares.findOne(hard_id)
});

Template.registerHelper('getHardBySessionId', function (sessionId) {
  return Hardwares.findOne({ddpId: sessionId})
});

Template.registerHelper('hardwareMine', function () {
  return appHd.getHardwareMine();
});

Template.registerHelper('hardwareShared', function () {
  return appHd.getHardwareShared();
});

Template.registerHelper('arduinosMine', function () {
  return appHd.getArduinosMine();
});

Template.registerHelper('arduinosShared', function () {
  return appHd.getArduinosShared();
});

Template.registerHelper('bootsMine', function () {
  const hardwareIdMine = appHd.getHardwareMine().fetch().map(function (h) {
    return h._id
  });
  return Boots.find({
    hard_id: {$in: hardwareIdMine},
    archived: {
      $exists: false
    }
  }, {sort: {date: -1}});
});

Template.registerHelper('bootsShared', function () {
  const hardwareIdShared = appHd.getHardwareShared().fetch().map(function (h) {
    return h._id
  });
  return Boots.find({
    hard_id: {$in: hardwareIdShared},
    archived: {
      $exists: false
    }
  }, {sort: {date: -1}});
});

appHd.getHardwareMine = function () {
  return Hardwares.find({owner_id: Meteor.userId()}, {sort: {order: 1}})
};

appHd.getHardwareShared = function () {
  // filtered by publish
  const h = Hardwares.find({owner_id: {$ne: Meteor.userId()}, shared: {$elemMatch: {"email": dfm.getUserEmail(Meteor.userId)}}}, {sort: {order: 1}});
  return h.count() ? h : false;
};

appHd.getArduinosMine = function () {
  return Hardwares.find({owner_id: Meteor.userId(), type: "arduinodoudou"}, {sort: {order: 1}});
};

appHd.getArduinosShared = function () {
  // filtered by publish
  const h = Hardwares.find({owner_id: {$ne: Meteor.userId()}, type: "arduinodoudou"}, {sort: {order: 1}});
  return h.count() ? h : false;
};