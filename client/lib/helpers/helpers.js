import DfmConnections from 'meteor/davidfm:core/api/accounts/lib/collections.js';

import './hardware.js';
import '../../../imports/lib/helpers/variables.js';
import '../../../imports/lib/helpers/helpers.js';

// NAMESPACE
globalThis.appHd = globalThis.appHd || {};

// REGISTER HELPERS

Template.registerHelper('getScenarios', function () {
  return Scenarios.find({ scenarioType: "scenario" }, { sort: { orderScenarios: 1, createdAt: 1 } });
});

Template.registerHelper('milli2second', function (date) {
  date = date || this.date || this.createdAt;
  return parseInt(date / 1000);
});

Template.registerHelper('httpOrHttps', function () {
  return window.location.protocol === 'https:' ? 'https' : 'http';
});

Template.registerHelper('equals', function (a, b) {
  return a === b;
});

Template.registerHelper('isModeEdition', function () {
  return Session.get('isModeEdition');
});

Template.registerHelper('unit', function () {
  if (this.unit) return this.unit;
  if (this.type === "temp") return "°C";
  if (this.type === "hum") return "%";
});

Template.registerHelper('aliasname', function (context) {
  context = context || this;
  return appHd.aliasname(context);
});

Template.registerHelper('isConnected', function () {
  // user connected with his account
  return appHd.isConnected();
});

Template.registerHelper('connectionIssue', function () {
  return (Session.get(SHOW_CONNECTION_ISSUE_KEY)) ? Meteor.status().connected : true;
});

Template.registerHelper('myUsername', function () {
  const un = Meteor.user().username;
  if (un) return un;
  const email = dfm.getUserEmail(Meteor.userId());
  return email.substring(0, email.indexOf('@'));
});

Template.registerHelper('getUserFullname', function (uId) {
  const u = Meteor.users.findOne(uId);
  if (!u) return 'userNotFound';
  if (u.services && u.services.google) return u.services.google.name;
  if (u.username) return u.username;
  const email = dfm.getUserEmail(uId);
  return email.substring(0, email.indexOf('@'));
});

Template.registerHelper('getUsername', function (uId) {
  return dfm.getUsername(uId);
});

Template.registerHelper('getUserEmail', function (uId) {
  const u = Meteor.users.findOne(uId);
  if (!u) return 'userNotFound';
  return dfm.getUserEmail(uId);
});

Template.registerHelper('getUserServices', function (uId) {
  return appHd.getUserServices(uId);
});

Template.registerHelper('hasDeviceType', function (type_id) {
  if (!this.devices) return false;
  return appHd.arrayObjectIndexOf(this.devices, type_id, 'type_id') > -1;
});

Template.registerHelper('getKeyTypes', function () {
  return appHd.keyTypes;
});

Template.registerHelper('getIconType', function (type) {
  const i = appHd.arrayObjectIndexOf(appHd.keyTypes, type, "type");
  return i > -1 ? '<span class="material-symbols-outlined">' + appHd.keyTypes[i].icon + '</span>' : '';
});

Template.registerHelper('getIconTypeButton', function (type) {
  const i = appHd.arrayObjectIndexOf(appHd.keyTypesButton, type, "type");
  return i > -1 ? '<span class="material-symbols-outlined">' + appHd.keyTypesButton[i].icon + '</span>' : '';
});

Template.registerHelper('keyStyles', function () {
  return appHd.keyStyles;
});

Template.registerHelper('getIconStyle', function (style) {
  const i = appHd.arrayObjectIndexOf(appHd.keyStyles, style, 'value')
  return i > -1 ? '<span class="material-symbols-outlined">' + appHd.keyStyles[i].icon + '</span>' : '';
});

Template.registerHelper('getMyHardwares', function () {
  return appHd.getMyHardwares();
});

Template.registerHelper('getMyKeysByRoomId', function (roomId) {
  return appHd.getMyKeysByRoomId(roomId);
});

Template.registerHelper('getMyKeys', function () {
  return appHd.getMyKeys();
});

Template.registerHelper('getMyTempKeys', function () {
  return appHd.getMyTempKeys();
});

Template.registerHelper('getMyKeysByHardId', function (hardId) {
  return appHd.getMyKeysByHardId(hardId);
});

Template.registerHelper('getHardOneByKeyId', function (keyId) {
  return appHd.getHardOneByKeyId(keyId);
});

Template.registerHelper('getMyScenarios', function () {
  return appHd.getMyScenarios();
});

Template.registerHelper('getMyScenariosByRoomId', function (roomId) {
  return appHd.getMyScenariosByRoomId(roomId);
});

Template.registerHelper('getMyActions', function () {
  return appHd.getMyActions();
});

Template.registerHelper('getAction', function (actionId) {
  return appHd.getAction(actionId);
});

Template.registerHelper('getMyButtons', function (roomId) {
  return appHd.getMyButtons(roomId);
});

Template.registerHelper('getMyRooms', function () {
  return Rooms.find({ $or: [{ userId: Meteor.userId() }, { shared: Meteor.userId() }] }, { sort: { order: 1 } });
});

Template.registerHelper('getRoom', function (id) {
  return appHd.getRoom(id);
});

Template.registerHelper('getKeyById', function (keyId) {
  return appHd.getKeyById(keyId)
});

Template.registerHelper('getKeysByHardId', function (hard_id) {
  return appHd.getKeysByHardId(hard_id)
});

Template.registerHelper('notUndefined', function (v) {
  return v !== undefined
});

Template.registerHelper('formatValueFromKey', function (key) {
  if (!key) return "";
  let lastValue = key.lastData?.value;
  if (key.type === "temp")
    lastValue = Math.round(lastValue * 10) / 10;
  return lastValue;
});

Template.registerHelper('lastSeenText', function (date) {
  if (!date) return '';
  const now = Date.now();
  const since = Math.round(now / 1000 - date / 1000);
  if (since > 604800)
    return '';
  if (since > 86400)
    return '> jour';
  else if (since > 1800)
    return '> demi-heure';
  else
    return 'actif';
});

Template.registerHelper('lastSeenClass', function (date) {
  return appHd.lastSeenClass(date);
});

Template.registerHelper('hardwaresTypes', function () {
  return appHd.hardwaresTypes;
});

// Template.registerHelper('userId', function () {
//   return Meteor.userId() ? Meteor.userId() : false;
// });

Template.registerHelper('isDdpConnected', function (ddpId) {
  ddpId = ddpId || this.ddpId;
  return DfmConnections.findOne({ id: ddpId });
});

Template.registerHelper('isUserSettingsActive', function (settings) {
  return appHd.isUserSettingsActive(settings) ? 'active' : '';
});

Template.registerHelper('isTabletMode', function () {
  // appHd.hardwareId :  un identifiant unique par cookies pour permettre une config personalisé par appareil
  return appHd.isUserSettingsActive('tabletMode_' + appHd.hardwareId) ? 'active' : '';
});

Template.registerHelper('hardwareId', function () {
  return appHd.hardwareId;
});

Template.registerHelper('isHardwareMine', function (hardId) {
  hardId = hardId || this._id;
  const HardOne = Hardwares.findOne(hardId);
  return HardOne.owner_id === Meteor.userId()
});

Template.registerHelper('JSONstringify', function (obj) {
  return JSON.stringify(obj, null, 4)
});

Template.registerHelper('currentTime', function () {
  return dfm.formatDate({ date: Chronos.date(), format: 'shortFullTime2' })
});

Template.registerHelper('darkMode', function (cssClass) {
  const hour = Chronos.date(60000).getHours()
  return (hour >= 22 || hour < 7) ? (cssClass ? cssClass : true) : (cssClass ? '' : false);
});

Template.registerHelper('utcToLocalTime', function (time) {
  return appHd.utcToLocalTime(time);
});

Template.registerHelper('getScenarioType', function () {
  return appHd.scenarios.types;
});

Template.registerHelper('getButton', function (buttonId) {
  return Buttons.findOne(buttonId);
});

Template.registerHelper('getButtonsTypes', function () {
  return appHd.buttons.types;
});

Template.registerHelper('getButtonsTypesName', function (type) {
  const i = appHd.arrayObjectIndexOf(appHd.buttons.types, type, 'type')
  return appHd.buttons.types[i]?.name;
});

// FUNCTIONS

appHd.utcToLocalTime = function (time) {
  time = time.split(':');
  const now = new Date();
  now.setHours(time[0]);
  now.setMinutes(time[1]);
  const minutesFromUtc = now.getTimezoneOffset();
  const local = new Date(now.getTime() - (minutesFromUtc * 60 * 1000));
  // const local = moment(now).subtract(minutesFromUtc, "minutes").toDate();
  return ("0" + local.getHours()).slice(-2) + ":" + ("0" + local.getMinutes()).slice(-2)
};

appHd.isUserSettingsActive = function (settings) {
  return Meteor.user() && Meteor.user().settings && Meteor.user().settings[settings];
};

appHd.buildBreadcrumb = function (arrayString) {
  if (!appHd.isUserSettingsActive('hideHome')) arrayString.unshift("home");

  const Breadcrumb = [];
  arrayString.forEach(function (value, iValue) {

    if (typeof value === 'string') {
      const i = appHd.arrayObjectIndexOf(appHd.configPages, value, 'name');
      if (i > -1) {
        const cp = Object.assign({}, appHd.configPages[i]); // clone object
        if (iValue !== arrayString.length - 1)
          cp.link = Router.path(appHd.configPages[i].name);
        Breadcrumb.push(cp);
      }
      else {
        const obj = { title: value };
        if (iValue > 1 && Breadcrumb[iValue - 1].class) {
          obj.class = Breadcrumb[iValue - 1].class
        }
        Breadcrumb.push(obj);
      }
    }
    else if (typeof value === 'object') Breadcrumb.push(value);

    // Si dernier lien, mettre actif
    if (iValue === arrayString.length - 1) Breadcrumb[iValue].class += " active";
  });
  return Breadcrumb;
};

appHd.stringify = function (obj, func, spaces) {
  if (!spaces) spaces = 4;
  if (obj === undefined) return 'undefined';
  if (obj === null) return 'null';
  if (obj === false) return 'false';
  return JSON.stringify(obj, func, spaces);
};

appHd.lastSeenClass = function (date) {
  if (!date) return 'mdb-color lighten-5';
  const now = Date.now();
  const since = Math.round(now / 1000 - date / 1000);
  if (since > 604800)
    return 'longtime';
  if (since > 86400)
    return 'red';
  else if (since > 1800)
    return 'orange';
  else
    return 'teal';
};

appHd.getUserServices = function (uId) {
  const u = Meteor.users.findOne(uId);
  if (!u) return 'userNotFound';

  const services = [];
  if (u.services) {
    if (u.services.password) services.push('password');
    if (u.services.google) services.push('google');
    if (u.services.facebook) services.push('facebook');
    if (u.services.linkedin) services.push('linkedin');
    if (u.services.twitter) services.push('twitter');
    if (u.services.instagram) services.push('instagram');
  }
  return services
};

/* appHd custom bootbox */
let template, html
/* alert */

// Small 'small', 'sm'
// Large 'large', 'lg'
// Extra large 'extra-large', 'xl'
// xl marche pas ?

template = document.createElement('template');
html = `
<div class="apphd-modal alert">
  <div class="bloc">
    <div class="header title"></div>
    <div class="body"></div>
    <div class="footer text-right"><a class="btn btn-sm btn-primary yes">ok</a></div>
  </div>
</div>`
html = html.trim();
template.innerHTML = html;
const alertModal = template.content.firstChild;

appHd.alert = function (msg, size, callback) {
  const clone = alertModal.cloneNode(true);
  clone.querySelector(".title").innerHTML = msg && msg.title ? msg.title : "";
  clone.querySelector(".body").innerHTML = msg && msg.message ? msg.message : (msg ? msg : "");
  document.body.appendChild(clone);

  clone.querySelector('.yes').addEventListener('click', function () {
    if (callback) callback(true);
    clone.remove();
  });
}