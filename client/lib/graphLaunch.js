import Highcharts from 'highcharts';
import Exporting from 'highcharts/modules/exporting';

Exporting(Highcharts);

Highcharts.setOptions({
  global: {
    useUTC: false
  }
});

// FUNCTIONS

const setDateOnZoom = function (event) {
  // log the min and max of the primary, datetime x-axis
  if (event.xAxis) {
    Session.set('graphDateFrom', Math.floor(event.xAxis[0].min))
    Session.set('graphDateTo', Math.floor(event.xAxis[0].max))
  }
  else {
    Session.set('graphDateFrom', false)
    Session.set('graphDateTo', false)
  }
}

// NAMESPACE
if (typeof appHd === "undefined") appHd = {};

appHd.edfPrice = function (key, howLongHour) {
  if (howLongHour === "debug") return;

  const now = Date.now();
  const dateAfter = now - (key.howLongHour * 60 * 60 * 1000);
  const data = DataByKey[key._id].find({ date: { $gt: dateAfter } }, { sort: { date: 1 } }).fetch();
  let price = {
    kw: 0
  };
  for (let i = 1; i < data.length; i++) {
    price.kw += (data[i].valeur - data[i - 1].valeur);
  }
  price.price = price.kw * (key.kWhPrice / 1000);
  if (price.price)
    return "Cout : " + price.price + "€, (" + key.kWhPrice + " kw/h)";
  else
    return "Cout : " + price.price + "€ (" + price.kw + "Kw/h), (" + key.kWhPrice + " kw/h)";
};

appHd.graphLaunch = function (key, howLongHour) {

  const now = Date.now();
  const dateAfter = now - (howLongHour * 60 * 60 * 1000);

  Session.set('graphDateFrom', dateAfter)
  Session.set('graphDateTo', now)

  if (key.style === "stock") stock(key, dateAfter);
  else if (key.style === "column") column(key, dateAfter);
  else if (key.style === "column_cumul") column_cumul(key, dateAfter);
  else if (key.style === "column_hour") column_hour(key, dateAfter);
  else if (key.style === "pie") pie(key, dateAfter);
  else if (key.style === "creneau") creneau(key, dateAfter);
  else stock(key, dateAfter);
};

function stock(key, dateAfter) {
  const data = DataByKey[key._id].find({ cle: key.name, date: { $gt: dateAfter } }, { sort: { date: 1 } }).fetch();
  let volume = [];
  const dataLength = data.length;

  for (let i = 0; i < dataLength; i++) {
    volume.push([
      parseFloat(data[i].date),
      parseFloat(data[i].valeur)
    ]);
  }

  const unit = key.unit || "";
  Highcharts.chart('container', {
    chart: {
      zoomType: 'x',
      events: {
        selection: setDateOnZoom
      }
    },
    title: {
      text: appHd.aliasname(key) + ' ' + unit
    },
    xAxis: {
      type: 'datetime',
      dateTimeLabelFormats: { // don't display the dummy year
        month: '%e. %b',
        year: '%b'
      },
      title: {
        text: 'Date'
      }
    },
    yAxis: {
      title: {
        text: unit
      }
    },
    tooltip: {
      headerFormat: '{point.x:%e %b} <b>{point.x:%H:%M}</b><br>',
      pointFormat: '{point.y}',
      valueDecimals: 1
    },
    series: [{
      name: appHd.aliasname(key),
      data: volume,
      lineWidth: 1,
      marker: {
        radius: 1
      }
    }],
    credits: {
      enabled: false
    }
  });
}


function creneau(key, dateAfter) {
  const data = DataByKey[key._id].find({ cle: key.name, date: { $gt: dateAfter } }, { sort: { date: 1 } }).fetch();

  let volume = [];
  const dataLength = data.length;

  for (let i = 0; i < dataLength; i++) {
    if (i > 0) {
      volume.push([
        parseFloat(data[i].date) - 1,
        parseFloat(data[i - 1].valeur)
      ]);
    }
    volume.push([
      parseFloat(data[i].date),
      parseFloat(data[i].valeur)
    ]);

  }

  const unit = key.unit || "";
  Highcharts.chart('container', {
    chart: {
      type: 'area',
      zoomType: 'x',
      events: {
        selection: setDateOnZoom
      }
    },
    title: {
      text: appHd.aliasname(key) + ' ' + unit
    },
    xAxis: {
      type: 'datetime',
      dateTimeLabelFormats: { // don't display the dummy year
        month: '%e. %b',
        year: '%b'
      },
      title: {
        text: 'Date'
      }
    },
    yAxis: {
      title: {
        text: 'Température °'
      },
      min: 0
    },
    tooltip: {
      headerFormat: '{point.x:%e %b} <b>{point.x:%H:%M}</b><br>',
      pointFormat: '{point.y}',
      valueDecimals: 1
    },
    series: [{
      name: appHd.aliasname(key),
      data: volume,
      lineWidth: 1,
      marker: {
        radius: 1
      }
    }],
    credits: {
      enabled: false
    }
  });
}

function column(key, dateAfter) {
  const data = DataByKey[key._id].find({ cle: key.name, date: { $gt: dateAfter } }, { sort: { date: 1 } }).fetch();

  let volume = [];
  const dataLength = data.length;

  for (let i = 0; i < dataLength; i++) {
    volume.push([
      parseFloat(data[i].date),
      parseFloat(data[i].valeur)
    ]);
  }

  const unit = key.unit || "";
  Highcharts.chart('container', {
    chart: {
      type: 'column',
      zoomType: 'x',
      events: {
        selection: setDateOnZoom
      }
    },
    title: {
      text: appHd.aliasname(key) + ' ' + unit
    },
    xAxis: {
      type: 'datetime',
      labels: {
        rotation: -45,
        style: {
          fontSize: '13px',
          fontFamily: 'Verdana, sans-serif'
        }
      }
    },
    yAxis: {
      title: {
        text: '...'
      }
    },
    legend: {
      enabled: false
    },
    tooltip: {
      pointFormat: '<b>{point.y:.1f}</b>'
    },
    series: [{
      name: appHd.aliasname(key),
      data: volume,
      dataLabels: {
        enabled: true,
        rotation: -90,
        color: '#FFFFFF',
        align: 'right',
        format: '{point.y:.1f}', // one decimal
        y: 10, // 10 pixels down from the top
        style: {
          fontSize: '13px',
          fontFamily: 'Verdana, sans-serif'
        }
      }
    }],
    credits: {
      enabled: false
    }
  });
}

function column_cumul(key, dateAfter) {
  const data = DataByKey[key._id].find({ cle: key.name, date: { $gt: dateAfter } }, { sort: { date: 1 } }).fetch();

  let volume = [];
  const len = data.length - 1;
  let backDate = 0;
  let backi = 0;
  for (let i = 0; i < len; i++) {
    if (data[i].date - backDate > 3600000) {
      backDate = data[i].date;
      volume.push([parseInt(data[i].date), parseFloat(data[i + 1].valeur) - parseFloat(data[backi].valeur)]);
      backi = i;
    }
  }

  const unit = key.unit || "";
  Highcharts.chart('container', {
    chart: {
      alignTicks: false,
      type: 'column',
      zoomType: 'x',
      events: {
        selection: setDateOnZoom
      }
    },
    title: {
      text: appHd.aliasname(key) + ' ' + unit
    },
    xAxis: {
      type: 'datetime',
      labels: {
        rotation: -45,
        style: {
          fontSize: '13px',
          fontFamily: 'Verdana, sans-serif'
        }
      }
    },
    yAxis: {
      title: {
        text: '...'
      }
    },
    legend: {
      enabled: false
    },
    tooltip: {
      formatter: function () {
        return Highcharts.dateFormat('%A %e %b à %H:%M', new Date(this.x)) + '<br><b>' + this.y + '</b>';
      }
    },
    series: [{
      type: 'column',
      name: appHd.aliasname(key),
      data: volume,
      dataLabels: {
        enabled: false,
        rotation: -90,
        color: '#FFFFFF',
        align: 'right',
        format: '{point.y:.1f}', // one decimal
        y: 10, // 10 pixels down from the top
        style: {
          fontSize: '13px',
          fontFamily: 'Verdana, sans-serif'
        }
      },
      dataGrouping: {
        units: [[
          'week', // unit name
          [1] // allowed multiples
        ], [
          'month',
          [1, 2, 3, 4, 6]
        ]]
      }
    }],
    credits: {
      enabled: false
    }
  });
}

function column_hour(key, dateAfter) {
  const data = DataByKey[key._id].find({ cle: key.name, date: { $gt: dateAfter } }, { sort: { date: 1 } }).fetch();

  let volume = [];
  const len = data.length - 1;
  let backDate = data[0].date;
  let dataTemp = parseFloat(data[0].valeur);

  for (let i = 1; i < len; i++) {
    if (data[i].date - backDate > 3600000) { // 1 heure
      volume.push([parseInt(data[i].date), parseFloat(dataTemp)]);
      backDate = data[i].date;
      dataTemp = parseFloat(data[i].valeur);
    }
    else {
      dataTemp += parseFloat(data[i].valeur);
    }
  }

  const unit = key.unit || "";
  Highcharts.chart('container', {
    chart: {
      type: 'column',
      zoomType: 'x',
      events: {
        selection: setDateOnZoom
      }
    },
    title: {
      text: appHd.aliasname(key) + ' ' + unit
    },
    xAxis: {
      type: 'datetime',
      labels: {
        rotation: -45,
        style: {
          fontSize: '13px',
          fontFamily: 'Verdana, sans-serif'
        }
      }
    },
    yAxis: {
      title: {
        text: '...'
      }
    },
    legend: {
      enabled: false
    },
    tooltip: {
      formatter: function () {
        return Highcharts.dateFormat('%A %e %b à %H:%M', new Date(this.x)) + '<br><b>' + this.y + '</b>';
      }
    },
    series: [{
      name: appHd.aliasname(key),
      data: volume,
      dataLabels: {
        enabled: false,
        rotation: -90,
        color: '#FFFFFF',
        align: 'right',
        format: '{point.y:.1f}', // one decimal
        y: 10, // 10 pixels down from the top
        style: {
          fontSize: '13px',
          fontFamily: 'Verdana, sans-serif'
        }
      }
    }],
    credits: {
      enabled: false
    }
  });
}


function pie(key, dateAfter) {
  const data = DataByKey[key._id].find({ cle: key.name, date: { $gt: dateAfter } }, { sort: { date: 1 } }).fetch();

  const volume = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
  const rosewind = ['N', 'NNE', 'NE', 'ENE', 'E', 'ESE', 'SE', 'SSE', 'S', 'SSW', 'SW', 'WSW', 'W', 'WNW', 'NW', 'NNW'];
  // FIXME : Vérifié la logique rosewind...
  const nb = data.length;

  for (const k in data) {
    const i = rosewind.map(function (e) {
      return e;
    }).indexOf(data[k].valeur);

    if (i > -1) volume[i] += (100 / nb);
  }

  Highcharts.chart('container', {
    series: [
      {
        name: appHd.aliasname(key),
        data: volume
      }
    ],
    chart: {
      polar: true,
      type: 'column',
      borderWidth: 0,
      backgroundColor: 'transparent',
      plotBackgroundColor: 'transparent',
      plotBorderWidth: 0,
    },
    title: {
      text: '...'
    },
    tooltip: {
      pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>',
      valueSuffix: '%',
      followPointer: true
    },
    xAxis: {
      categories: ['N', 'NNE', 'NE', 'NEE', 'E', 'EES', 'ES', 'ESE', 'S', 'SSW', 'SW', 'SWW', 'W', 'WWN', 'NW', 'NNW'],
    },
    yAxis: {
      min: 0,
      endOnTick: false,

      showLastLabel: true,
      labels: {
        formatter: function () {
          return this.value + '%';
        }
      }
    },
    plotOptions: {
      series: {
        stacking: 'normal',
        shadow: false,
        groupPadding: 0,
        pointPlacement: 'on'
      }
    },
    credits: {
      enabled: false
    }
  });
}