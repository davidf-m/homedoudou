/**
* Created by David FAIVRE-MAÇON  (15 December 2023)
* https://www.davidfm.me
*/

globalThis.appHd = globalThis.appHd || {};

const webPushPublicKey = Meteor.settings?.public?.notifications?.push?.webpush?.publicKey;

function urlBase64ToUint8Array(base64String) {
  const padding = '='.repeat((4 - base64String.length % 4) % 4);
  const base64 = (base64String + padding).replace(/\-/g, '+').replace(/_/g, '/');
  const rawData = window.atob(base64);
  return Uint8Array.from([...rawData].map((char) => char.charCodeAt(0)));
}

appHd.showNotification = () => {
  Notification.requestPermission().then(function (permission) {
    // If the user accepts, let's create a notification
    if (permission === "granted") {
      navigator.serviceWorker.ready.then(function (registration) {
        registration.showNotification('Vibration Sample', {
          body: 'Buzz! Buzz!',
          icon: '../images/touch/chrome-touch-icon-192x192.png',
          vibrate: [200, 100, 200, 100, 200, 100, 200],
          tag: 'vibration-sample'
        });
      });
    }
  });
};

appHd.askForPermissionToReceiveNotifications = () => {

  if ('serviceWorker' in navigator) {
    navigator.serviceWorker.ready.then(function (reg) {
      console.log('A service worker is active:', reg.active);
      // on Safari there is a pb
      // const func = reg.pushManager ? reg.pushManager.subscribe : window.safari.pushNotification;
      if (!reg || !reg.pushManager || !reg.pushManager.subscribe) {
        return appHd.alert('!reg.pushManager.subscribe');
      }
      reg.pushManager.subscribe({
        userVisibleOnly: true, // By setting this to true, the browser ensures that every incoming message has a matching (and visible) notification.
        applicationServerKey: webPushPublicKey,
        //applicationServerKey: urlBase64ToUint8Array(webPushPublicKey),
      }).then(function (sub) {
        // We have a new subscription, update the database

        Notification.requestPermission().then(function (permission) {
          // If the user accepts, let's create a notification
          if (permission === "granted") {
            reg.showNotification('Bonjour', {
              body: 'Buzz! Buzz!',
              icon: '../images/touch/chrome-touch-icon-192x192.png',
              vibrate: [200, 100, 200, 100, 200, 100, 200],
              tag: 'vibration-sample'
            });
            Meteor.callAsync('notificationsSubsUpdate', JSON.stringify(sub)); // will insert if does not exists
          }
        });

      }).catch(function (e) {
        if (Notification.permission === 'denied') {
          appHd.alert('pushPermDenied');
        }
        else {
          appHd.alert('pushPermError');
        }
        throw e;
      });
    }).catch(function (e) {
      appHd.alert('ready error: ' + e.message);
    });
  }
  else appHd.alert('no serviceWorker')
};