/**
* Created by David FAIVRE-MAÇON
* https://www.davidfm.me
*/

const ERRORS_KEY = 'myAccountErrors';

import './myaccount.html';
Template.myAccount.onCreated(function () {

  Session.set(ERRORS_KEY, {});
  Session.set('myAccountSaved', false);
});

Template.myAccount.helpers({

  errorMessages: function () {
    return _.values(Session.get(ERRORS_KEY));
  },

  errorClass: function (key) {
    return Session.get(ERRORS_KEY)[key] && 'error';
  },

  myemail: function () {
    //TODO return without social account?
    return dfm.getUserEmail(Meteor.userId());
  },

  saved: function () {
    return Session.get('myAccountSaved');
  },

  hasService(service) {
    console.log(appHd.getUserServices(Meteor.userId()));
    return appHd.getUserServices(Meteor.userId()).indexOf(service) > -1

  }
});

Template.myAccount.events({

  'keyup, change': function (event, template) {
    Session.set('myAccountSaved', false);
  },

  'submit': function (event, template) {
    event.preventDefault();
    const email = template.$('[name=email]').val();
    const password = template.$('[name=password]').val();
    const confirm = template.$('[name=confirm]').val();

    const errors = {};
    if (!email) errors.email = 'Email required';
    if (confirm !== password) errors.confirm = 'Please confirm your password';

    Session.set(ERRORS_KEY, errors);
    if (_.keys(errors).length) return;

    Meteor.users.update(
      { _id: Meteor.user()._id }, {
      $set: { emails: [{ address: email, verified: false }] }
    },
      function (err) {
        if (!err) Session.set('myAccountSaved', true);
        else Session.set(ERRORS_KEY, { 'onsave': err.message });
      });
    if (password) {
      Meteor.callAsync('setPassword', password)
        .then(() => {
          Router.go('dfmSigninAtForm');
        })
        .catch((err) => {
          Session.set(ERRORS_KEY, { 'onsave': err.message });
        });
    }

  }
});