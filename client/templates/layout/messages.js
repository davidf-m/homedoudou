import './messages.html';

Template.messages.helpers({

  getMessages() {
    const messages = Messages.find({ userId: Meteor.userId(), close: { $ne: true } });
    return messages.count() ? { messages: messages } : false;
  },
});

Template.messages.events({

  'click .js-close': function () {
    console.log(this);
    Meteor.callAsync('messages.close', this._id)
  },
});