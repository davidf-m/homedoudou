import './layoutConfiguration.html';

Template.configurationLayout.onRendered(function () {

  setTimeout(function () {
    $('[data-bs-toggle="tooltip"]').tooltip();

  }, 500);
});

Template.configurationLayout.onDestroyed(function () {
  $('[data-bs-toggle="tooltip"]').tooltip('dispose');
});

Template.configurationLayout.events({

  'click .js-logout': function () {
    Meteor.logout();
  },

  'click [data-dfm-toggle]'(e) {

    const target = $(e.currentTarget).data('dfm-target');
    $(target).toggleClass('show')
  }
});