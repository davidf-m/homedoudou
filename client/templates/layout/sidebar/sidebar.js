import DfmConnections from 'meteor/davidfm:core/api/accounts/lib/collections.js';
import './sidebar.html';

Template.sideBar.onRendered(function () {

  if (document.body.clientWidth < 768)
    $('#wrapper').removeClass('sidebar-on');
  else
    $('.navbar-collapse').collapse('show');
});

Template.sideBar.helpers({

  isInDashboard() {
    const curRoute = Router.current().route.getName();
    if (curRoute === 'dashboard' || curRoute === 'dataKey' || curRoute === 'hardware')
      return 'active';
    else
      return '';
  },

  hardwareMine() {
    // les matériels de l'utilisateur connecté
    if (!Meteor.userId()) return false;
    return Hardwares.find({ owner_id: Meteor.userId() }, { sort: { order: 1 } })
  },

  hardwareShared() {
    // les matériels partagés à l'utilisateur connecté
    if (!Meteor.userId()) return false;
    const h = Hardwares.find({ owner_id: { $ne: Meteor.userId() }, shared: { $elemMatch: { "email": dfm.getUserEmail(Meteor.userId()) } } }, { sort: { order: 1 } });
    return h.count() ? h : false;
  },

  hardwarePublic() {
    const allPublicKeys = Keys.find({ public: true }).fetch();
    let hard_ids = [];
    _.each(allPublicKeys, function (publicKey) {
      hard_ids.push(publicKey.hard_id);
    });

    let filter = {
      $or: [{ _id: { $in: hard_ids } }]
    };

    if (Meteor.userId()) {
      const u = Meteor.users.findOne(Meteor.userId());
      if (u) {
        filter.owner_id = { $ne: Meteor.userId() };
        filter.shared = { $not: { $elemMatch: { "userId": Meteor.userId() } } };
      }
    }

    return Hardwares.find(filter);
  },

  hardware() {
    const h = Hardwares.find({}, { sort: { order: 1 } });
    console.log(h.fetch());
    return h;
  },

  cles() {
    return _.sortBy(this.cles, 'order');
  },

  nbConnected() {
    return DfmConnections.find().count()
  },
});

Template.sideBar.events({

  'click #side-menu a.menuqui': function (e) {
    $('#side-sidebar a.menuqui').not(e.currentTarget).next().slideUp(500);
  },

});

Template.menuHardwareItem.onRendered(function () {
  $('[data-bs-toggle="tooltip"]').tooltip();
});

Template.menuHardwareItem.helpers({

  activeMenuQui: function (routeName) {
    const curRoute = Router.current().params['hard_id'];
    return curRoute === routeName ? 'active btn-selected' : '';
  },

  menuCle: function (hard_id) {
    return Keys.find({ hard_id: hard_id, show: true }, { sort: { order: 1 } });
  },

  getHowLongHour() {
    return this.howLongHour || 24
  },

  activeMenuDataKey: function (hard_id, cle) {
    const curRouteArr = Iron.Location.get().path.split('/');
    if (curRouteArr[2] === hard_id && curRouteArr[3] === cle)
      return 'active';
    return '';
  },

});
