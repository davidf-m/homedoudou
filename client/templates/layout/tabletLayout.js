/**
* Created by David FAIVRE-MAÇON  (29 September 2022)
* https://www.davidfm.me
*/

import '../../../imports/ui/front/dashbord/rooms/rooms';
import './tabletLayout.html';

Template.tabletLayout.onCreated(function () {
});

Template.tabletLayout.onRendered(function () {
  setTimeout(function () {
    $('[data-bs-toggle="tooltip"]').tooltip();
  }, 500);
});

Template.tabletLayout.onDestroyed(function () {
  $('[data-bs-toggle="tooltip"]').tooltip('dispose');
});

Template.tabletLayout.helpers({

  notOnHome() {
    return Router.current().route.getName() !== 'home';
  },

  connected: function () {
    //TODO make global helper
    if (Session.get(SHOW_CONNECTION_ISSUE_KEY)) {
      return Meteor.status().connected;
    }
    else {
      return true;
    }
  },

  exception: function () {
    return Session.get('exception');
  },

  tabletMode() {
    return appHd.isUserSettingsActive('tabletMode_' + appHd.hardwareId) ? 'tablet-mode' : '';
  }
});

Template.tabletLayout.events({

  'click .js-logout': function () {
    Meteor.logout(function (err) {
      if (err) console.log(err);
    });
  },

  'click .js-close-exception': function () {
    Session.set('exception', null);
  },
});
