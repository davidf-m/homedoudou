/**
* Created by David FAIVRE-MAÇON
* https://www.davidfm.me
*/

import "./menu-user.html";
import "./loading.html";
import "./footer.html";
import "./frontLayout.html";
import '../../../imports/ui/front/dashbord/rooms/rooms';

Template.mainLayout.onCreated(function () {
});

Template.mainLayout.onRendered(function () {
  setTimeout(function () {
    $('[data-bs-toggle="tooltip"]').tooltip();
  }, 500);

  this.subscribe('dfmTranslationsPrimary');

  $("#sidebar")
    .on('show.bs.collapse', function (e) {
      $('#wrapper').addClass('sidebar-on');
    })
    .on('hide.bs.collapse', function (e) {
      $('#wrapper').removeClass('sidebar-on');
    })
});

Template.mainLayout.onDestroyed(function () {
  $('[data-bs-toggle="tooltip"]').tooltip('dispose');
});

Template.mainLayout.helpers({

  notOnHome() {
    return Router.current().route.getName() !== 'home';
  },

  connected: function () {
    //TODO make global helper
    if (Session.get(SHOW_CONNECTION_ISSUE_KEY)) {
      return Meteor.status().connected;
    }
    else {
      return true;
    }
  },

  exception: function () {
    return Session.get('exception');
  },

  tabletMode() {
    return appHd.isUserSettingsActive('tabletMode_' + appHd.hardwareId) ? 'tablet-mode' : '';
  }
});

Template.mainLayout.events({

  'click .js-logout': function () {
    Meteor.logout();
  },

  'click .js-mode-edition-on': function () {
    Session.set('isModeEdition', true);
  },

  'click .js-mode-edition-off': function () {
    Session.set('isModeEdition', false);
  },

  'click .js-close-exception': function () {
    Session.set('exception', null);
  },

  'click #sidebar': function (e) {
    if (document.body.clientWidth < 768) {
      if ($(e.currentTarget).hasClass("show")) {
        $('.navbar-collapse').collapse('hide');
        $('#wrapper').removeClass('sidebar-on');
        $('.navbar-fixed-top .navbar-collapse').removeClass('in');
      }
    }
  }
});
