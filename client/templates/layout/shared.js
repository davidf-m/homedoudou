Template.adminOnly.events({

  'click .js-hide-only-admin-things': function (e) {
    e.preventDefault();
    Session.set('hideOnlyAdminThings', true);
  },
});

Template.isDDPConnected.onRendered(function () {
  $('[data-bs-toggle="tooltip"]').tooltip();
});

Template.aliasName.helpers({

  ctx() {
    return this.ctx || this;
  },

  link() {
    return this.link || Router.path('hardware', { hard_id: (this.ctx?._id || this._id) })
  }
});