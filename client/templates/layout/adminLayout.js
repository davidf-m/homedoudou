import './adminLayout.html';

Template.adminLayout.onRendered(function () {

  if (document.body.clientWidth < 768) {
    $('#wrapper').removeClass('sidebar-on');
    $('#sidebar').removeClass('in');
  }

  setTimeout(function () {
    $('[data-bs-toggle="tooltip"]').tooltip();
  }, 500);
});

Template.adminLayout.onDestroyed(function () {
  $('[data-bs-toggle="tooltip"]').tooltip('dispose');
});

Template.adminLayout.helpers({
  userName: function () {
    const un = Meteor.user().username;
    if (un && un != "") {
      return un;
    }
    const email = Meteor.user().emails[0].address;
    return email.substring(0, email.indexOf('@'));
  },

  activeMenuSplit: function (segmentName, segmentNumber) {
    const curRouteArr = Iron.Location.get().path.split('/');
    return curRouteArr[segmentNumber] === segmentName ? 'active' : '';
  },

  activeMenuAllUrlClass: function (routeName) {
    const curRoute = Router.current().route;
    return curRoute.originalPath === routeName ? 'active' : '';
  },

  exception: function () {
    return Session.get('exception');
  }
});

Template.adminLayout.events({

  'click .js-logout': function () {
    Meteor.logout();
  },

  'click .js-close-exception': function () {
    Session.set('exception', null);
  },
});
