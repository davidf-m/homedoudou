/**
* Created by David FAIVRE-MAÇON
* https://www.davidfm.me
*/

import '../lib/collections/main.js';

import './templates/layout/shared.html';
import './templates/layout/breadcrumb.html';
import './templates/layout/messages.js';
import './templates/layout/sidebar/sidebar.js';
import './templates/layout/adminLayout.js';
import './templates/layout/webmasterLayout.js';
import './templates/layout/layoutConfiguration.js';
import './templates/layout/tabletLayout.js';
import './templates/layout/frontLayout.js';
import './lib/startup.js';
import './lib/helpers/helpers.js';
import './lib/graphLaunch.js';
import './lib/ohsnap.js';
import './lib/push-notification.js';
import 'mdbootstrap/scss/mdb-lite-free.scss';

import '../lib/helpers/main.js';
import '../imports/ui/stylesheets/main.less';

import '../imports/client/routes.js'

Tracker.autorun(() => {
  Meteor.subscribe('dfmTranslationsBylng', dfm.languages.getCurrent());
});