/**
* Created by David FAIVRE-MAÇON
* https://www.davidfm.me
*/

import dfmCron from 'meteor/davidfm:core/api/cron/server/class-cron.js';

if (typeof (appHd) === "undefined") appHd = {};
if (typeof (appHd.scenario) === "undefined") appHd.scenario = {};

class HD_Scenario {
  constructor() {
    this.name = 'yo';
  }

  actionLaunch(id, options) {
    const s = Scenarios.findOne(id);
    if (!s) return { err: true, msg: 'scenario ' + id + ' not found' };
    if (s) {
      if (s.scenarioType !== 'action') return { err: true, msg: 'scenarioType action only' };
      // type getLastValue
      if (s.type === 'getLastValue' && s.cleId) {
        const c = Keys.findOne(s.cleId);
        let text = " " + c.lastData?.value;
        if (c.type === "temp") text = '... Il fait ' + (c.lastData?.value * 1) + ' degrés...';
        const karotz = Hardwares.findOne(s.karotzId);
        if (karotz) {
          const command = '/cgi-bin/tts?voice=1&text=' + encodeURI(text) + '&nocache=0';
          appHd.karotz.command(karotz, command);
          return { data: c.lastData?.value };
        }
        else return { err: true, msg: 'no karotz found' }
      }
      // type doKarotz
      else if (s.type === 'doKarotz' && s.command) {
        const karotz = Hardwares.findOne(s.karotzId);
        if (karotz) {
          appHd.karotz.command(karotz, s.command);
          return { msg: "done" };
        }
        else return { err: true, msg: 'no karotz found' }
      }
      // type relaiON
      else if (s.type === 'relaiON') {
        const hard = Hardwares.findOne(s.hardId);
        if (hard) {
          appHd.arduinoDoudou.command(hard, { mode: "action", cle: s.broche, valeur: "0" });
          return { msg: "done" };
        }
        else return { err: true, msg: 'no karotz found' }
      }
      // type relaiOFF
      else if (s.type === 'relaiOFF') {
        const hard = Hardwares.findOne(s.hardId);
        if (hard) {
          appHd.arduinoDoudou.command(hard, { mode: "action", cle: s.broche, valeur: "1" });
          return { msg: "done" };
        }
        else return { err: true, msg: 'no karotz found' }
      }
      // type "mhz433"
      else if (s.type === 'mhz433') {
        const hard = Hardwares.findOne(s.hardId);
        if (hard) {
          const Mhz433One = Mhz433.findOne(s.mhz433_id);
          if (Mhz433One) {
            return appHd.talkToArduino(hard._id, { mode: "433E", cle: Mhz433One.broche, valeur: Mhz433One.signal });
            // {"msg": "signal envoyé", "type": 'mhz433', "scenario_id": s.mhz433_id, "hard_id": s.hardId, "alias": s.alias};
          }
          else return { err: true, msg: 'no mhz433 found' }
        }
        else return { err: true, msg: 'hardware not found' }
      }
      // type "serie"
      else if (s.type === 'serie') {
        const hard = Hardwares.findOne(s.hardId);
        if (hard) {
          return appHd.talkToArduino(hard._id, { mode: s.module, cle: s.key, valeur: s.value });
          //FIXME retur done or not?
          return { msg: "done" };
        }
        else return { err: true, msg: 'hardware not found' }
      }
      // type "socketSend"
      else if (s.type === 'socketSend') {
        const hard = Hardwares.findOne(s.hardId);
        if (hard) {
          return appHd.talkToArduino(hard._id, { mode: s.mode, cle: s.key, valeur: s.value });
          //FIXME retur done or not?
          return { msg: "done" };
        }
        else return { err: true, msg: 'hardware not found' }
      }
      // type "SMS FREE"
      else if (s.type === 'sms-free') {
        const u = Meteor.users.findOne(s.createdBy);
        if (u) {
          let msg = "Action SMS FREE";
          if (s.msg) {
            msg = s.msg;
            if (options && options.msg) msg += options.msg;
          }
          Meteor.call('sendNotificationWeb', u._id, msg);
          return { err: false, msg: 'Notification envoyée' };
        }
        else
          return { err: true, msg: 'Error : User not found' }
      }
      // type "NOTIF MAIL ONCE"
      else if (s.type === 'notif-mail-once') {
        const u = Meteor.users.findOne(s.createdBy);
        if (u) {
          Meteor.call('sendNotificationByMailOnce', s, options);
          return { err: false, msg: 'Notification mail once envoyée' };
        }
        else
          return { err: true, msg: 'Error : User not found' }
      }
      // type "HTTP REQUEST"
      else if (s.type === 'http-request') {
        const u = Meteor.users.findOne(s.createdBy);
        if (u) {
          try {
            Meteor.call('hmdHttpGet', s.url);
          } catch (e) {
            console.log(e);
            return { err: true, msg: e }
          }
          return { msg: "done" };
        }
        else
          return { err: true, msg: 'user not found' }
      }
      // type "HTTP REQUEST"
      else if (s.type === 'scenarioStatus') {
        const u = Meteor.users.findOne(s.createdBy);
        if (u) {
          try {
            appHd.scenario.status(s.scenarioId, s.status);
            return { msg: "scenario.status done" };
          } catch (e) {
            console.log(e);
            return { err: true, msg: e }
          }
        }
        else
          return { err: true, msg: 'user not found' }
      }
      else return { err: true, msg: 'unknown action type' }
    }
    return { err: true, msg: 'unknown error' }
  }

  isTriggering(data) {
    // je check si un event faisant parti d'un scenario actif exist.
    const scenariosEvent = Scenarios.find({ scenarioType: 'event', eventCleId: data.cleId }).fetch().reduce(function (filtered, scenario) {

      if (scenario.input === 'strictly-positive' && data.valeur > 0) filtered.push(scenario._id);
      else if (scenario.input === 'null' && Number(data.valeur) === 0) filtered.push(scenario._id);
      else if (scenario.input === 'different') {
        const keyId = appHd.getKeyId(data.hard_id, data.cle);
        const last = DataByKey[keyId].find({}, { sort: { date: -1 }, limit: 2 }).fetch();
        if (!last.length) filtered.push(scenario._id);
        else if (last.length === 1) filtered.push(scenario._id);
        else if (last.length && last[0].valeur !== data.valeur) filtered.push(scenario._id);
      }
      else if (scenario.input === 'superior-to' && parseFloat(data.valeur) > parseFloat(scenario.value)) filtered.push(scenario._id);
      else if (scenario.input === 'inferior-to' && parseFloat(data.valeur) < parseFloat(scenario.value)) filtered.push(scenario._id);
      else if (scenario.input === 'equal-to' && ((!isNaN(data.valeur) && parseFloat(data.valeur) === parseFloat(scenario.value)) || (data.valeur === scenario.value))) filtered.push(scenario._id);
      else if (scenario.input === 'all-data-on' && parseFloat(data.valeur) === 1) {
        //vérifie que toutes les autres données soit à 1
        let check = 1;
        for (let i = 0; i < scenario.eventCleId.length; i++) {
          const cleId = scenario.eventCleId[i];
          if (cleId !== data.cleId) {
            const k = Keys.findOne(cleId);
            if (k.lastData?.value !== '1') check = 0;
          }
        }
        if (check) filtered.push(scenario._id);
        // FIXME : si une des autres données est à 0, je dois aussi le savoir
      }
      else if (scenario.input === 'all-data-off' && data.valeur === '0') {
        //vérifie que toutes les autres données soit à 0
        let check = true;
        for (let i = 0; i < scenario.eventCleId.length; i++) {
          const cleId = scenario.eventCleId[i];
          if (cleId !== data.cleId) {
            const k = Keys.findOne(cleId);
            if (k.lastData?.value !== '0') check = false;
          }
        }
        if (check) filtered.push(scenario._id);
      }
      else if (scenario.input === 'not-all-data-on' && data.valeur === '0') {
        filtered.push(scenario._id);
      }
      else if (scenario.input === 'not-all-data-off' && data.valeur === '1') {
        filtered.push(scenario._id);
      }
      return filtered;
    }, []);

    // Si j'ai trouvé un event correspondant à la cle, je regarde si un scenario existe pour cet event
    if (scenariosEvent.length) {
      const time = new Date().toLocaleString('fr-FR', { hour: "numeric", minute: "numeric", "hour12": false });
      return Scenarios.find({ scenarioType: 'scenario', input: { $in: scenariosEvent }, $and: [{ $or: [{ fromDate: { $exists: false } }, { fromDate: "" }, { fromDate: { $lte: time } }] }, { $or: [{ toDate: { $exists: false } }, { toDate: "" }, { toDate: { $gt: time } }] }], disabled: { $ne: true } }).fetch();
    }
    else return false;
  }

  launchActionsOfScenariosOutput(scenarioId) {
    const that = this;
    const scenario = Scenarios.findOne(scenarioId);
    if (scenario && scenario.output && scenario.output.length) {
      scenario.output.forEach(function (output) {
        const action = Scenarios.findOne(output);
        if (action) {
          that.actionLaunch(action._id, scenario.outputOptions);
        }
      });

    }
  }

  status(scenarioId, status) {
    if (!appHd.hasRightOnScenario(scenarioId)) return dfm.logError('no rights on this scenario');

    const scenario = Scenarios.findOne(scenarioId);
    if (scenario.type === "cron") {
      if (status) {
        // dfmCron.startTask(scenarioId);
      }
      else {
        // dfmCron.stopTask(scenarioId);
      }
    }
    console.log(scenario, status);
    return Scenarios.update(scenarioId, { $set: { disabled: !status } });
  }
}

appHd.scenario = new HD_Scenario();