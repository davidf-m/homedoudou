// NAMESPACE
if (typeof (appHd) === "undefined") appHd = {};

appHd.dataAddArchive = function (obj, options) {
  let HardOne, hard_id, key_id, session_id;

  // obj en http
  // {
  //   connexionType: 'http',
  //   hard_id: pHard_id,
  //   key: pCle,
  //   value: pValeur,
  //   index: pIndex,
  //   that: this
  // }

  // obj en session
  // {
  //   connexionType: 'ddp',
  //   session_id: session_id,
  //   key: key,
  //   value: value,
  //   index: index,
  // }

  if (obj.connexionType === 'http') {
    HardOne = Hardwares.findOne({ _id: obj.hard_id });
    if (!HardOne) return sendNok({
      connexionType: obj.connexionType,
      hard_id: obj.hard_id,
      that: obj.that,
      text: "id missing"
    });
    hard_id = HardOne._id;
  }
  else if (obj.connexionType === 'ddp') {
    HardOne = Hardwares.findOne({ ddpId: obj.session_id });
    if (!HardOne) return sendNok({
      connexionType: obj.connexionType,
      session_id: obj.session_id,
      text: "unknown_hard"
    });
    hard_id = HardOne._id;
    session_id = obj.session_id;
  }
  else return appHd.logsError("unknown_communication");

  const data = {
    hard_id: hard_id,
    cle: obj.key,
    valeur: obj.value,
    date: Date.now()
  };

  // valeurs minimum et maximum si numérique
  let min = null;
  let max = null;
  const pvn = parseFloat(obj.value);
  if (!isNaN(pvn)) {
    min = pvn;
    max = pvn;
  }

  // BROCHE_14_RFlink_OK_
  // FIXME changer le nom de la cle dans l'arduino en "RFLinkBack"
  // {"m":"archive","k":"BROCHE_14_RFlink_OK_","v":"10;Maclean;f41f;01;ON","i":"0"}
  // retour de commande 433
  if (obj.key === "BROCHE_14_RFlink_OK_") {
    const splited = obj.value.split(';');
    const key = splited[1] + '_' + splited[2] + '_' + splited[3];
    const objSplited = {
      connexionType: obj.connexionType,
      key: key,
      value: splited[4],
      index: obj.index,
    };
    if (obj.connexionType === 'http') {
      objSplited.hard_id = hard_id;
      objSplited.that = obj.that;
    }
    else if (obj.connexionType === 'ddp') {
      objSplited.session_id = session_id;
    }
    else return;

    appHd.dataAddArchive(objSplited, { back: false, isSplitted: true });
    return sendOk({
      connexionType: obj.connexionType,
      hard_id: hard_id,
      that: obj.that,
      session_id: session_id,
      text: obj.key + ' -- ' + obj.value,
      log: { enabled: true, comment: "RFLINK splited" }
    });
  }

  // je regarde si cette clé existe
  let KeyOne = Keys.findOne({ hard_id: hard_id, name: data.cle });

  // Si la cle est de type RFLINK, je la decoupe
  if ((!options || !options.isSplitted) && KeyOne && KeyOne.device && KeyOne.device.type_id === 86) {
    // /add?id=---&mode=archive&cle=BROCHE_14&valeur=20;91;Digitech;ID=0131;TEMP=00a4;BAT=OK;&index=1
    // http://www.rflink.nl/blog2/protref
    // 20;2D;UPM/Esic;ID=0001;TEMP=00cf;HUM=16;BAT=OK;
    // 20;02;Name;ID=9999;LABEL=data;

    if (!obj.value || typeof obj.value !== 'string') {
      //console.log(obj.value, typeof obj.value);
      return sendNok({
        connexionType: obj.connexionType,
        hard_id: hard_id,
        that: obj.that,
        session_id: session_id,
        text: "obj.value !== 'string'"
      });
    }

    const splited = obj.value.split(';');
    // si le split n'est pas bon...
    if (splited.length < 2) {
      if (obj.index && HardOne.devices && HardOne.devices[obj.index]) {
        const device = HardOne.devices[obj.index];
        //console.log(device);
        device.index = obj.index;
        Keys.update(KeyOne._id, { $set: { device: device } });
      }

      return sendNok({
        connexionType: obj.connexionType,
        hard_id: hard_id,
        that: obj.that,
        session_id: session_id,
        text: 'splited.length < 2 : ' + obj.value,
        log: { enabled: true, text: "ERROR", comment: "RFLINK : splited.length < 2" }
      });
    }

    //FIXME ci dessous n'est pas vrai
    // tous les messages RFLINK sont cencé être formaté de la même manière, splited[3] étant l'id, [ '20', '91', 'Digitech', 'ID=1234', 'TEMP=00a6', 'BAT=OK', '' ]

    let text = "";
    for (let i = 0; i < splited.length; i++) {
      const item = splited[i];
      if (!item) continue;
      const name = item.split('=')[0];
      let value = item.split('=')[1];
      // console.log(name, value);i
      if (name === 'ID') {
        data.cle += "_" + value;
      }
      else if (value) {
        const key = data.cle + "_" + name;
        text += name + "=" + value + " ";
        // FIXME replace by !== SWITCH ?
        if (name === "TEMP" || name === "HUM" || name === "BAT") {
          if (name === "TEMP") value = parseInt(value, 16) / 10;
          let KeyOneTemp = Keys.findOne({ hard_id: hard_id, name: key });
          if (!KeyOneTemp) KeyOneTemp = Keys.insert({ hard_id: hard_id, name: key, owner_id: HardOne.owner_id });
        }

        const objSplited = {
          connexionType: obj.connexionType,
          key: key,
          value: value,
          index: obj.index,
        };
        if (obj.connexionType === 'http') {
          objSplited.hard_id = hard_id;
          objSplited.that = obj.that;
        }
        else if (obj.connexionType === 'ddp') {
          objSplited.session_id = session_id;
        }
        else return;

        // minTime default
        const keySplited = Keys.findOne({ hard_id: HardOne._id, name: key, minTime: { $exists: false } });
        if (keySplited) Keys.update(keySplited._id, { $set: { minTime: 570000 } });

        appHd.dataAddArchive(objSplited, { back: false, isSplitted: true });
      }
    }
    return sendOk({
      connexionType: obj.connexionType,
      hard_id: hard_id,
      that: obj.that,
      session_id: session_id,
      text: obj.key + ' -- ' + obj.value,
      log: { enabled: true, comment: "RFLINK splited" }
    });
  }

  let broche_id = false;
  const keySplited = data.cle.split("_");
  if (keySplited[0].toUpperCase() === "BROCHE") broche_id = keySplited[1];

  // If key doesn't exist, i create it
  if (!KeyOne) {
    // if is new Key
    // FIXME value is not added
    const keyCount = Keys.find({ hard_id: hard_id }).count(); // FIXME pas count mais le plus grand order
    const newKey = {
      hard_id: hard_id,
      broche_id: broche_id,
      name: data.cle,
      style: "stock",
      order: keyCount,
      min: min,
      max: max,
      owner_id: HardOne.owner_id,
      lastData: { date: new Date, value: data.valeur },
    };
    if (data.cle === "sequence_") newKey.hidden = true;
    key_id = Keys.insert(newKey);
    KeyOne = Keys.findOne(key_id);
    DataByKey[key_id] = new Mongo.Collection('DataByKey_' + key_id);
    // DataByKey_30[key_id] = new Mongo.Collection('DataByKey_30_' + key_id);
    // DataByKey_365[key_id] = new Mongo.Collection('DataByKey_365_' + key_id);

    appHd.logs({
      // ip: ip,
      hard_id: hard_id,
      logs: ["new key : " + data.cle],
      tags: ['ddp', 'key', 'newKey'],
    });
  }
  else {
    // Si la cle existe deja

    let set = { lastDate: new Date, lastData: { date: new Date, value: data.valeur }, beforeLast: KeyOne.lastData };

    // si la valeur est numérique
    if (!isNaN(pvn)) {
      if (pvn < KeyOne.min) set.min = pvn;
      if (pvn > KeyOne.max) set.max = pvn;
    }

    if (obj.index && HardOne.devices && HardOne.devices[obj.index]) {
      const device = HardOne.devices[obj.index];
      set.device = {
        index: obj.index,
        broche: device.broche,
        type_id: device.type_id,
        name: device.name
      };
    }

    // si la cle commence par BROCHE et a un name ex: BROCHE_17_MOTDETAT
    if (keySplited[0].toUpperCase() === "BROCHE" && keySplited[2]) set.keyName = keySplited[2];

    // update Keys
    Keys.update(KeyOne._id, { $set: set });
    key_id = KeyOne._id;
  }

  if (!DataByKey[key_id]) {
    appHd.logsError({
      hard_id: hard_id,
      log: 'Collection don\'t exist DataByKey[' + key_id + '] ' + JSON.stringify(KeyOne),
      tags: [obj.connexionType]
    });
    DataByKey[key_id] = new Mongo.Collection('DataByKey_' + key_id);
    // DataByKey_30[key_id] = new Mongo.Collection('DataByKey_30_' + key_id);
    // DataByKey_365[key_id] = new Mongo.Collection('DataByKey_365_' + key_id);
  }

  // //

  const hardOwner = Meteor.users.findOne(HardOne.owner_id);
  if (!hardOwner) return appHd.logsError('hardOwner missing');

  // notifications

  if (KeyOne.notified) appHd.sendNotifs(hardOwner._id, appHd.aliasname(KeyOne) + " : " + obj.value);

  // scenarios

  const logs = {
    //ip: ip,
    hard_id: hard_id,
    logs: [],
    tags: [obj.connexionType, 'triggered', 'scenario'],
    enabled: false
  }

  /*
    je regarde si un event avec cette cleId existe et si un scenario libre utilisant cet event existe
    alors je fais l'action correspondate

    ensuite

    je regarde si un scenario chauffage existe avec cette cleId
    alors je fais la logique de chauffage
 */

  // quand une cle en mode archive envoie une valeur, je dois verifier si un scenario libre est lié a cette cle et si l'evenement est déclencheur.
  data.cleId = key_id;
  // console.log(data);
  const scenarios = appHd.scenario.isTriggering(data);
  // console.log(scenarios);
  if (scenarios && scenarios.length) {
    logs.tags.push('free');
    logs.logs.push({ log: JSON.stringify(scenarios) });
    for (let i = 0; i < scenarios.length; i++) {
      appHd.scenario.launchActionsOfScenariosOutput(scenarios[i]._id);
    }
    logs.enabled = true;
  }

  // scenario chauffage
  const scenariosHeater = Scenarios.find({ scenarioType: 'scenario', sensorId: data.cleId, disabled: { $ne: true } }).fetch();
  if (scenariosHeater.length) {
    scenariosHeater.forEach(function (scenario) {
      // y a t-il une plage de fonctionnement horaire pour une temperature précise
      let customRangeTriggered = false;
      // Temperature sur plage horaire
      if (scenario.rangeDateUTC?.length) {
        const time = new Date().toLocaleString('fr-FR', { hour: "numeric", minute: "numeric", "hour12": false });
        scenario.rangeDateUTC.forEach((customRange) => {
          if ((customRange.toDate > customRange.fromDate && time > customRange.fromDate && time < customRange.toDate) || (customRange.fromDate > customRange.toDate && (time > customRange.fromDate || time < customRange.toDate))) {
            customRangeTriggered = true;
            if (parseFloat(data.valeur) < (customRange.temperature - scenario.delta)) appHd.scenario.actionLaunch(scenario.heaterOn);
            else if (parseFloat(data.valeur) > (customRange.temperature + scenario.delta)) appHd.scenario.actionLaunch(scenario.heaterOff);
          }
        });
      }
      // Temperature par defaut
      if (!customRangeTriggered) {
        if (parseFloat(data.valeur) < (scenario.temp - scenario.delta)) appHd.scenario.actionLaunch(scenario.heaterOn);
        else if (parseFloat(data.valeur) > (scenario.temp + scenario.delta)) appHd.scenario.actionLaunch(scenario.heaterOff);
      }

      /*
      // To remove : ancien fonctionement de plage horaires, aucune personalisation de temperature
      // y a t-il une plage de fonctionnement horaire
      if(scenario.rangeDateUTC?.length) {
        const time = new Date().toLocaleString('fr-FR', {hour: "numeric", minute: "numeric", "hour12": false});
        scenario.rangeDateUTC.forEach((dates) => {
          if((dates.toDate > dates.fromDate && time > dates.fromDate && time < dates.toDate) || (dates.fromDate > dates.toDate && (time > dates.fromDate || time < dates.toDate)))
            if(parseFloat(data.valeur) < (scenario.temp - scenario.delta)) appHd.scenario.actionLaunch(scenario.heaterOn);
        });
      }
      else if(parseFloat(data.valeur) < (scenario.temp - scenario.delta)) appHd.scenario.actionLaunch(scenario.heaterOn);
      if(parseFloat(data.valeur) > (scenario.temp + scenario.delta)) appHd.scenario.actionLaunch(scenario.heaterOff);
       */
    });
    logs.logs.push({ log: JSON.stringify(scenariosHeater) });
    logs.tags.push('heater');
    logs.enabled = true;
  }

  // sequence_ FIN je remets le 433 en mode ecoute si besoin
  if (obj.key === "sequence_" && obj.value && obj.value.toLowerCase() === "fin") {
    Hardwares.update(HardOne._id, { $set: { listenSince: new Date() } });

    // Mode ecoute 433
    if (obj.connexionType === 'ddp' && HardOne.listening433) {
      const i = appHd.arrayObjectIndexOf(HardOne.devices, 86, 'type_id');
      if (i > -1) {
        const broche = HardOne.devices[i].broche;
        const time = 10 * 60 * 1000; // 10 minutes
        // console.log('Mode ecoute 433', broche, time);
        appHd.ddpSend(session_id, { mode: "433R", cle: broche, valeur: time });
        logs.logs.push({ log: "Ecoute 433 ON" });
      }
    }
  }

  appHd.logs(logs);

  // Ajout Données

  if (data.cle.toUpperCase() === "JSON") {
    // const json = JSON.parse(decodeURI(obj.value));
    // console.log(json);
    return sendOk({
      connexionType: obj.connexionType,
      hard_id: hard_id,
      that: obj.that,
      session_id: session_id,
      text: "ok json test"
    });
  }
  else if (data.cle === "directionVENT") {
    // Si l'ajout est de type "directionVENT" ne pas ajouter la valeur si la dernière est la même.
    const lastData = DataByKey[key_id].findOne({}, { sort: { 'date': -1 }, limit: 1 });
    if (!lastData || lastData !== obj.value) addArchive(data);
    else
      return sendNok({
        connexionType: obj.connexionType,
        hard_id: hard_id,
        that: obj.that,
        session_id: session_id,
        text: "ok ARCHIVE directionVENT but not save cause same value"
      });
  }
  else {
    // Save data
    // FIXME si la dernière donnée est >= à 10 minutes, seulement les t° et etc, pas les actions...

    if (KeyOne.minTime) {
      const lastData = DataByKey[key_id].findOne({}, { sort: { date: -1 } });
      if (lastData && (new Date() - lastData.date) < KeyOne.minTime) {

        // Dernières donnée même inférieur à KeyOne.minTime
        Keys.update(KeyOne._id, {
          $set: {
            lastData: { date: new Date(), value: data.valeur },
            beforeLast: { date: KeyOne.lastData?.date, value: KeyOne.lastData?.value }
          }
        });

        return sendOk({
          connexionType: obj.connexionType,
          hard_id: hard_id,
          that: obj.that,
          session_id: session_id,
          text: "ok ARCHIVE not saved < " + KeyOne.minTime + " sec",
          log: { enabled: true }
        });
      }
    }

    if (KeyOne.volatile) DataByKey[key_id].remove({});
    addArchive(data);
    if (!options || options.back !== false)
      return sendOk({
        connexionType: obj.connexionType,
        hard_id: hard_id,
        that: obj.that,
        session_id: session_id,
        text: "ok ARCHIVE save"
      });
    else {
      appHd.logs({
        hard_id: hard_id,
        log: JSON.stringify(data),
        tags: ['archive', obj.connexionType]
      });
    }
  }

};

function addArchive(data) {
  DataByKey[data.cleId].insert(data);
}

function sendOk(obj) {
  if (obj.connexionType === 'http') {
    if (obj.log) obj.log.hard_id = obj.hard_id;
    return appHd.respOk(obj.that, obj.text, obj.log);
  }
  else if (obj.connexionType === 'ddp') {
    return appHd.ddpSendOk(obj.session_id, obj.text, obj.log);
  }
}

function sendNok(obj) {
  if (obj.connexionType === 'http') {
    if (obj.that) {
      if (obj.log) obj.log.hard_id = obj.hard_id;
      return appHd.respNok(obj.that, obj.text, obj.log);
    }
  }
  else if (obj.connexionType === 'ddp') {
    return appHd.ddpSendError(obj.session_id, obj.text, obj.log);
  }
}