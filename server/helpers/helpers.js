// NAMESPACE
import { HTTP } from "meteor/http";

const timeout = 20000;

if (typeof (appHd) === "undefined") appHd = {};

appHd.throwError = function (obj) {
  if (!obj.tags) obj.tags = ['throwError'];
  else obj.tags.push('throwError');
  appHd.logsError({ log: obj.text, tags: obj.tags });
  throw new Meteor.Error(500, obj.text, "throwError (server)");
};

appHd.talkToArduino = function (hard_id, obj) {
  // appellé par url, pas de connexion ex http://localhost:3000/api/scenario/action?id=xXDPixCB6hpWQdRjH
  // if(!Meteor.userId()) appHd.throwError("not-authorized", {log: 'talkToArduino ' + JSON.stringify(hard) + JSON.stringify(obj)});
  // //FIXME better security

  //console.log("appHd.talkToArduino", hard_id, obj);
  if (!obj.mode) appHd.throwError({ text: 'missing params, talkToArduino ' + JSON.stringify(hard_id) + JSON.stringify(obj) });

  const HardOne = Hardwares.findOne(hard_id);
  if (!HardOne) return appHd.throwError({ text: 'talkToArduino HardOne not found' });

  // Si l'arduino est en ecoute répétée, il n'ecoute pas, d'abord, on arrette l'écoute.
  let mustListeningAfterTalk = false;
  if (HardOne.listening433 && obj.mode !== "433R") {
    mustListeningAfterTalk = true;
    const i = appHd.arrayObjectIndexOf(HardOne.devices, 86, 'type_id');
    const broche = HardOne.devices[i].broche;
    appHd.talkToArduino(HardOne._id, { mode: "433R", cle: broche, valeur: "STOP" });
  }

  let result;
  const sessionId = appHd.isDdpConnected(HardOne._id);
  if (sessionId) { // DDP
    result = appHd.ddpSend(sessionId, { mode: obj.mode, cle: obj.cle, valeur: obj.valeur });
    if (mustListeningAfterTalk) appHd.setArduinoListening433(HardOne._id, true);
    return result;
  }
  else { // HTTP
    const command = "/?mode=" + obj.mode + "&cle=" + obj.cle + "&valeur=" + obj.valeur;
    const url = "http://" + HardOne.ip + ":" + HardOne.port + command;
    try {
      result = HTTP.get(url, { timeout: timeout });
    } catch (error) {
      appHd.logsError({ log: 'talkToArduino ' + JSON.stringify(error) });
      // appHd.messages.error('ip : ' + HardOne.ip + ' port: ' + HardOne.port, 'Impossible de se connecter à l\'arduino', error);
      return { err: true, error }
    }
    if (mustListeningAfterTalk)
      appHd.setArduinoListening433(HardOne._id, true);
    return result;
  }
};

appHd.setArduinoListening433 = function (hard_id, state) {
  // FIXME BUG?
  // {"m":"bug","k":"BROCHE_0","v":"MAUVAISE_broche_selectionnee:RFLINK","i":"999","hard_id":"WpkkR6757FhfBoaDK","createdAt":"2019-10-25T17:13:02.243Z"}	ddp		37.165.168.99
  // {"m":"retour","broche":"0","etat":"port serie NON VALIDE"}

  const HardOne = Hardwares.findOne(hard_id);
  const i = appHd.arrayObjectIndexOf(HardOne.devices, 86, 'type_id');
  const spindle = HardOne.devices[i].broche; // spindle = broche

  const time = 600000 - (new Date() - HardOne.listenSince); // "433", 10 minutes moins le temps passé
  const log = {
    hard_id: hard_id,
    log: "broche : " + spindle + " time : " + time,
    tag: ['debug']
  };
  appHd.logs(log);
  Hardwares.update(hard_id, { $set: { listening433: state } });
  if (state) {
    Meteor.setTimeout(function () {
      appHd.talkToArduino(hard_id, { mode: "433R", cle: spindle, valeur: time });
    }, 2000);
    return true
  }
  else
    return appHd.talkToArduino(hard_id, { mode: "433R", cle: spindle, valeur: "STOP" });
};