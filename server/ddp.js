import { UserStatus } from 'meteor/mizzao:user-status';

// NAMESPACE
if (typeof (appHd) === "undefined") appHd = {};

appHd.getSessionDdp = function (sessionId) {
  const sockets = Meteor.server.stream_server.open_sockets;
  const session = appHd.arrayObjectIndexOf(sockets, sessionId, function (e, i) {
    if (e._meteorSession)
      return e._meteorSession.id;
  });
  return sockets[session];
};

appHd.ddpSend = function (sessionId, obj) {
  // ex obj = '{"msg":"toto"}' ou '{"mode":"EEPROM", "cle": "CONF_OK"}'
  const Session = appHd.getSessionDdp(sessionId);
  if (Session) {
    Session.send(JSON.stringify(obj));
    return { msg: "sending in ddp", ddpId: sessionId };
  }
  else {
    appHd.logsError({
      log: "session not found : " + sessionId + " " + JSON.stringify(obj),
      tags: ['ddp']
    });
    return { error: "session not found" };
  }

};

appHd.ddpSendOk = function (sessionId, text, log) {
  if (log && log.enabled) {
    const hardOne = Hardwares.findOne({ ddpId: sessionId });
    appHd.logs({
      hard_id: hardOne ? hardOne._id : '',
      log: "SessionId : " + sessionId + " ---> " + text,
      tags: ['ddp', 'send']
    });
  }
  // FIXME gerer dans l'arduino
  appHd.ddpSend(sessionId, { msg: "log", reason: text });
};

appHd.ddpSendError = function (sessionId, text) {
  const hard = Hardwares.findOne({ ddpId: sessionId });
  appHd.logsError({
    hard_id: hard ? hard._id : '',
    log: "SessionId : " + sessionId + " " + text,
    tags: ['ddp', 'send']
  });

  appHd.ddpSend(sessionId, { msg: "error", reason: text });
};

/****************** INIT *********************/
appHd.ddpInit = function (session_id, params) {
  const key = params.k;
  const value = params.v;

  const Session = appHd.getSessionDdp(session_id);
  if (!Session) return appHd.logsError('error session')
  const ip = Session.headers['x-forwarded-for'];
  if (key.toLowerCase() === "demande" && value.toLowerCase() === "config") {
    console.log('new client ip ddp init: ' + ip);
    console.log("ddp 59 : faire ça en http ou pas...");
  }
  if (key.toLowerCase() === 'local_ip' && value) {
    // UserStatus.connections.update(session_id, { $set: { local_ip: value } });
  }
  if (key.toLowerCase() === 'version' && value) {
    // UserStatus.connections.update(session_id, { $set: { version: value } });
  }
  if (key.toLowerCase() === 'name' && value) {
    // UserStatus.connections.update(session_id, { $set: { name: value } });
  }
};

/****************** BOOT *********************/
appHd.ddpBoot = function (session_id, params) {
  const key = params.k;
  const value = params.v;
  const hardOne = Hardwares.findOne({ ddpId: session_id });
  const hard_id = hardOne ? hardOne._id : false;
  if (!hardOne) {
    // arduino vierge ?
    if (key === 'local_ip' && value) {
      // UserStatus.connections.update(session_id, { $set: { local_ip: value } });
    }
    if (key === 'version' && value) {
      // UserStatus.connections.update(session_id, { $set: { version: value } });
    }
    if (key === 'name' && value) {
      // UserStatus.connections.update(session_id, { $set: { name: value } });
    }
  }

  if (key.toLowerCase() === "demande") {
    if (value.toLowerCase() === "okko") {
      if (!hardOne) return appHd.ddpSendError(session_id, "unknown_hard");
      //TODO verifier la conf
      return appHd.ddpSend(session_id, { mode: "eeprom", cle: "conf_ok" });
    }
    // on demande si l'arduino doit faire quelques choses boot, par exemple on remets les broches de l'arduino à jour.
    if (value.toLowerCase() === "ordre") {
      // quelles broches doit on remmettre à jour?
      if (hardOne.devices?.length) {
        hardOne.devices.forEach((device) => {
          if (device.type_id === 3) {
            const broche = device.broche;
            const keyOne = Keys.findOne({ hard_id: hardOne._id, name: "BROCHE_" + broche });
            if (!keyOne) return appHd.ddpSendError(session_id, "unknown_key");
            else if (keyOne.initOnBoot) {
              const lastData = keyOne.lastData?.value;
              if (lastData !== undefined) {
                // log("broche maj")
                // log(hardOne._id, broche, lastData)
                appHd.arduinoDoudou.command(hardOne, { mode: "action", cle: broche, valeur: lastData });
              }
            }
          }
        })
      }
    }
  }
  else if (key.toLowerCase() === "debut") {
    // efface les anciens bugs de l'arduino qui boot
    if (hard_id) {
      // On set le dernier boot pour cet hardware à maintenant
      Hardwares.update(hard_id, { $set: { lastBoot: new Date() } });

      // On efface les bugs
      Bugs.remove({ hard_id: hard_id });
      // On archive l'ancien boot et on en crée un nouveau
      Boots.update({ hard_id: hard_id }, { $set: { archived: true } }, { multi: true });
      Boots.insert({ hard_id: hard_id, boot: [], date: Date.now() });

      // si config free envoyer un sms "arduino boot"
      const u = Meteor.users.findOne(hardOne.owner_id);
      if (u?.settings?.notifHardBoot) appHd.sendNotifs(u._id, appHd.aliasname(hardOne) + " boot");
    }
    else appHd.ddpSendError(session_id, "unknown_hard");
  }
  else {
    if (key.toLowerCase() === "version") Hardwares.update(hard_id, { $set: { version: value } });
    if (key.toLowerCase() === "nom") Hardwares.update(hard_id, { $set: { name: value } });

    const bootLineSaved = Boots.update({ hard_id: hard_id, "archived": { $ne: true } }, {
      $push: {
        boot: {
          "cle": key,
          "valeur": value,
        }
      }
    });
    if (!bootLineSaved) appHd.ddpSendError(session_id, "nok! BOOT not saved");
  }
};

/****************** ETAT *********************/
appHd.ddpEtat = function (session_id, params) {
  const key = params.k;
  const value = params.v;
  const hard = Hardwares.findOne({ ddpId: session_id });
  const hard_id = hard ? hard._id : false;

  if (key.toLowerCase() === "debut") {
    // FIXME obligation de l'arduino avant la suite
    Etats.remove({ hard_id: hard_id });
    Etats.insert({
      hard_id: hard_id,
      state: [],
      createdAt: new Date()
    });
  }
  else if (key.toLowerCase() !== "fin") {
    const stateLineSaved = Etats.update({ hard_id: hard_id }, {
      $push: {
        state: {
          "k": key,
          "v": value,
        }
      }
    });
    if (!stateLineSaved) appHd.ddpSendError(session_id, "nok! Etat not saved");
  }
};

/****************** BUG *********************/
appHd.ddpBug = function (session_id, params) {
  const hard = Hardwares.findOne({ ddpId: session_id });
  params.hard_id = hard ? hard._id : "";
  params.createdAt = new Date();
  Bugs.insert(params);
};

/****************** SMS *********************/
appHd.ddpSms = function (session_id, params) {
  const hard = Hardwares.findOne({ ddpId: session_id });
  if (hard) {
    const user = Meteor.users.findOne(hard.owner_id);
    if (user) {
      const uf = appHd.isFreeConfig(user);
      if (uf) {
        const key = params.k;
        const value = params.v;
        const message = appHd.aliasname(hard) + " dit : \r\n Cle : " + key + "\r\n Valeur : " + value;
        const that = this;
        Meteor.call('sendFreeSMS', uf.login, uf.key, message, function (err) {
          if (err) appHd.ddpSendError(that, "err sending sms");
        });
      }
      else appHd.ddpSendError(this, "err sending sms, user " + user._id + " has no free config");
    }
    else appHd.ddpSendError(this, "err sending sms, user not found");
  }
  else appHd.ddpSendError(this, "unknown_hard");
};

/****************** ARCHIVE *********************/
appHd.ddpArchive = function (session_id, params) {
  let key = params.k;
  let value = params.v;
  const index = params.i;

  appHd.dataAddArchive({
    connexionType: 'ddp',
    session_id: session_id,
    key: key,
    value: value,
    index: index,
  });
};

