/**
* Created by David FAIVRE-MAÇON
* https://www.davidfm.me
*/

import { Meteor } from 'meteor/meteor';
import dfmCron from 'meteor/davidfm:core/api/cron/server/class-cron.js';

dfmCron.functions["scenarioLaunch"] = ({ scenarioId }) => {
  const s = Scenarios.findOne(scenarioId);
  s.output.forEach((actionId) => {
    appHd.scenario.actionLaunch(actionId)
  })
};

Meteor.methods({

  'scenario.add': function (data) {
    data.scenarioType = 'scenario';
    data.createdAt = new Date();
    data.createdBy = Meteor.userId();

    const scenarioId = Scenarios.insert(data);
    if (data.type === "cron") {
      dfmCron.addCron({
        name: data.name,
        fn: {
          name: 'scenarioLaunch',
          attributes: { scenarioId: scenarioId }
        },
        options: { time: data.cronTime },
        meta: {
          description: 'Scenario ' + data.name + '',
        }
      });
      // dfmCron.add({
      //   sourceId: scenarioId,
      //   cronTime: data.cronTime,
      //   jobsId: data.output,
      //   name: data.name
      // });
    }
  },

  'scenarios.update': function (scenarioId, data) {
    data.modifiedAt = new Date();
    data.modifiedBy = Meteor.userId();

    const scenario = Scenarios.findOne(scenarioId);
    Scenarios.update(scenarioId, { $set: data });
    if (data.type === "cron") {
      dfmCron.removeCron(scenario.name);
      dfmCron.addCron({
        name: data.name,
        fn: {
          name: 'scenarioLaunch',
          attributes: { scenarioId: scenarioId }
        },
        options: { time: data.cronTime },
        meta: {
          description: 'Scenario ' + data.name + '',
        }
      });
    }

  },

  async 'scenarios.summerMode.toggle'(scenarioId) {

    const scenario = await Scenarios.findOneAsync(scenarioId);
    await Scenarios.updateAsync(scenarioId, { $set: { summerMode: !scenario.summerMode } });
  },

  'scenarios.updateRanks': function (updates) {
    updates.forEach((update) => {
      Scenarios.update(update.scenarioId, { $set: { orderScenarios: update.orderScenarios } });
    });
  },

  'scenarioDuplicate': function (id) {
    const scenarioOne = Scenarios.findOne(id);
    if (scenarioOne) {
      delete scenarioOne['_id'];
      scenarioOne.name += " duplicated";
      scenarioOne.createdAt = new Date();
      scenarioOne.createdBy = Meteor.userId();
      Scenarios.insert(scenarioOne);
    }
    else appHd.throwError({ text: 'scenarioDuplicate ' + JSON.stringify(scenarioOne) });
  },

  'scenario.remove': function (scenarioId) {
    const s = Scenarios.findOne(scenarioId);
    if (!Meteor.userId() || Meteor.userId() !== s.createdBy) return appHd.logsError("scenario.remove, not rights !")

    // Si le scenario est de type cron, supprimer le cron
    const scenario = Scenarios.findOne(scenarioId);
    if (scenario.type === "cron") dfmCron.removeCron(scenario.name);

    Scenarios.remove(scenarioId);
  },

  'scenario.status': function (scenarioId, status) {
    return appHd.scenario.status(scenarioId, status);
  },

  'scenario.event.add': function (data) {
    data.scenarioType = 'event';
    data.createdAt = new Date();
    data.createdBy = Meteor.userId();
    Scenarios.insert(data);
  },

  'scenario.event.edit': function (id, data) {
    data.modifiedAt = new Date();
    data.modifiedBy = Meteor.userId();
    Scenarios.update(id, { $set: data });
  },

  'scenarioEventDuplicate': function (id) {
    const scenarioOne = Scenarios.findOne(id);
    if (scenarioOne) {
      delete scenarioOne['_id'];
      scenarioOne.name += " duplicated";
      scenarioOne.createdAt = new Date();
      scenarioOne.createdBy = Meteor.userId();
      Scenarios.insert(scenarioOne);
    }
    else appHd.throwError({ text: 'scenarioEventDuplicate ' + JSON.stringify(scenarioOne) });
  },

  'scenario.event.remove': function (id) {
    const s = Scenarios.findOne(id);
    if (Meteor.userId() && Meteor.userId() === s.createdBy) {
      //FIXME a finir, peut etre un array?
      Scenarios.remove({ input: id });
      Scenarios.remove(id);
    }
  },

  'scenario.action.add': function (data) {
    data.scenarioType = 'action';
    data.createdAt = new Date();
    data.createdBy = Meteor.userId();
    Scenarios.insert(data);
  },

  'scenario.action.edit': function (id, data) {
    data.modifiedAt = new Date();
    data.modifiedBy = Meteor.userId();
    Scenarios.update(id, { $set: data });
  },

  'scenarioActionDuplicate': function (id) {
    const scenarioOne = Scenarios.findOne(id);
    if (scenarioOne) {
      delete scenarioOne['_id'];
      scenarioOne.name += " duplicated";
      scenarioOne.createdAt = new Date();
      scenarioOne.createdBy = Meteor.userId();
      Scenarios.insert(scenarioOne);
    }
    else appHd.throwError({ text: 'scenarioActionDuplicate ' + JSON.stringify(scenarioOne) });
  },

  'scenario.action.remove': function (id) {
    const s = Scenarios.findOne(id);
    if (Meteor.userId() && Meteor.userId() === s.createdBy) {
      Scenarios.remove({ output: id });
      Scenarios.remove(id);
    }
  },

  'scenarioActionLaunch': function (id) {
    return appHd.scenario.actionLaunch(id);
  },

  'scenarios.push.room'(scenarioId, roomId) {
    // je dois au moins être connecté
    if (!Meteor.userId()) throw new Meteor.Error("not-authorized");

    const scenario = Scenarios.findOne(scenarioId);
    console.log(scenario);
    console.log(roomId);
    if (!scenario) throw new Meteor.Error("scenario not exist");

    // const hard = Hardwares.findOne(key.hard_id);
    // // je dois être webmaster ou l'appareil doit m'appartenir pour modifier cette clé
    // if(hard.owner_id !== Meteor.userId() && !dfm.isWebmaster()) throw new Meteor.Error("not-authorized");

    Scenarios.update(scenarioId, { $push: { rooms: roomId } })

  }
});
