import DfmConnections from 'meteor/davidfm:core/api/accounts/lib/collections.js';

Meteor.methods({

  'ddpFromHmd': function (session_id, obj) {
    // ON ENVOIE UN MESSAGE A L'ARDUINO

    //FIXME sécurité, vérifier le proprio
    return appHd.ddpSend(session_id, obj);
  },

  'ddpToHmd': function (params) {
    // FIXME remove ddpId on deconnexion ?
    // ON REÇOIT UN MESSAGE DE L'ARDUINO EN DDP
    const ip = this.connection.clientAddress || this.connection.httpHeaders['x-forwarded-for'];
    if (!params) throw new Meteor.Error("no params");
    const session_id = this.connection.id;
    const mode = params.m;
    const key = params.k;
    const value = params.v;
    let hard_id = '';
    const tags = ['ddp', mode];
    let comment = "";

    const hard = Hardwares.findOne({ ddpId: this.connection.id });
    if (hard) {
      Hardwares.update(hard._id, { $set: { ip: ip, lastSeen: new Date() } });
      hard_id = hard._id;
    }

    if (mode) {
      switch (mode) {
        case 'close':
          const Session = appHd.getSessionDdp(session_id);
          if (Session) return Session.close();
          break;
        case 'log':
          const log = {
            ip: ip,
            hard_id: hard_id,
            log: value,
            tags: ['ddp']
          };
          appHd.logs(log);
          break;
        case 'init':
          appHd.ddpInit(session_id, params);
          break;
        case 'boot':
          appHd.ddpBoot(session_id, params);
          break;
        case 'etat':
          appHd.ddpEtat(session_id, params);
          break;
        case 'bug':
          appHd.ddpBug(session_id, params);
          break;
        case 'sms':
          appHd.ddpSms(session_id, params);
          break;
        case 'connexion':
          if (key === 'user_id' && value) {
            // arduino vierge
            DfmConnections.update({ id: session_id }, { $set: { arduino_user_id: value } })
            // UserStatus.connections.update(session_id, { $set: { arduino_user_id: value } });
          }
          if (key === 'hard_id' && value) {
            if (value === "01234567890123456") return appHd.ddpInit(session_id, params); // arduino vierge en attente de config
            DfmConnections.update({ id: session_id }, { $unset: { arduino_user_id: 1 } })
            // UserStatus.connections.update(session_id, { $unset: { arduino_user_id: 1 } });
            // Doit être le premier echange je dois avoir avec l'arduino pour associer la session et le matériel.
            const update = Hardwares.update(value, { $set: { ip: ip, lastSeen: new Date(), ddpId: session_id } });
            if (update) hard_id = value;
            else return appHd.ddpSendError(session_id, "unknown_hard");
          }
          break;
        case 'archive':
          appHd.ddpArchive(session_id, params);
          break;
        default:
        // code block
      }
    }
    else {
      tags.push('error');
      comment += " mode manquant";
    }

    const logs = [JSON.stringify(params)];
    logs.push(comment)
    const log = {
      ip: ip,
      hard_id: hard_id,
      logs: logs,
      tags: tags
    };
    appHd.logs(log);
  },
});