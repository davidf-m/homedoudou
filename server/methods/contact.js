Meteor.methods({

  newFormContact(contact) {
    contact.created = new Date();
    Contact.insert(contact);

    const users = Roles.getUsersInRole('webmaster').fetch();
    _.each(users, function (user) {
      Email.send({
        from: user.emails[0].address,
        to: user.emails[0].address,
        subject: 'message contact',
        html: JSON.stringify(contact)
      });
    });
  }

});