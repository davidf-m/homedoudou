import { HTTP } from "meteor/http";
import * as cheerio from 'cheerio/lib/cheerio.js';

Meteor.methods({

  'scrapers.insert'(form) {
    form.userId = Meteor.userId();
    form.lastDateDone = new Date();
    Scrapers.insert(form);
  },

  'scrapers.update'(id, form) {
    Scrapers.update(id, { $set: form });
  },

  scrapersRemove: function (id) {
    Scrapers.remove(id);
  },

  scrapersGet: function (id) {
    const scrap = Scrapers.findOne(id);
    if (scrap && scrap.url && scrap.element) {
      HTTP.get(scrap.url, function (error, result) {
        if (error) {
          console.log('http get FAILED! router.js 590');
          console.log(error);
          throw new Error('http get FAILED! router.js 590: ' + error)
        }
        else {
          if (result.statusCode === 200) {
            const content = result.content;

            let value = '';
            if (scrap.type === 'json') {
              const data = JSON.parse(content);
              const keys = scrap.element.split('.');
              value = data;
              for (let i = 0; i < keys.length; i++) {
                const key = keys[i];
                if (value && key)
                  value = value[key];
              }
            }
            else if (scrap.type === 'html') {
              const $ = cheerio.load(content);
              const search = $(scrap.element);
              value = search.html();
            }

            if (value !== undefined && scrap.hardId && scrap.key) {
              let domain = 'http://hard.homedoudou.fr/';
              if (dfm.isLocalhost()) domain = "http://localhost:3000/"
              Meteor.call('hmdHttpGet', domain + "add?id=" + scrap.hardId + "&mode=archive&cle=" + scrap.key + "&valeur=" + value, function (err, result) {
                if (err) {
                  console.log(err);
                  throw new Meteor.Error("500", "hmdHttpGet");
                }
              });
            }
            else {
              console.log("error in : value && scrap.hardId && scrap.key");
              throw new Meteor.Error("500", "error in : value && scrap.hardId && scrap.key");
            }
          }
        }
      });
    }
  }

});


