Meteor.methods({

  'buttons.insert': function (form) {
    if(!form.createdBy) form.createdBy = Meteor.userId();
    form.order = Buttons.find({createdBy: form.createdBy, rooms: form.rooms[0]}).count() + 1;
    return Buttons.insert(form);
  },

  'buttons.update': function (id, form) {
    return Buttons.update(id, {$set: form});
  },

  'buttons.remove': function (buttonId) {
    const buttonOne = Buttons.findOne(buttonId)
    if(buttonOne.createdBy !== Meteor.userId())
      throw new Meteor.Error("not-authorized");
    else
      return Buttons.remove(buttonOne._id);
  }
});
