import { HTTP } from "meteor/http";

Meteor.methods({

  hmdHttpGet: function (url) {
    // peut etre appellé par url, pas de connexion ex http://localhost:3000/api/scenario/action?id=xXDPixCB6hpWQdRjH
    // id fait la sécurité dans ce cas là
    //FIXME better security

    try {
      return HTTP.get(url);
    } catch (error) {
      throw new Meteor.Error(error.response.statusCode, error.response.content);
    }
  },

  talkToArduino: function (hard_id, obj) {
    return appHd.talkToArduino(hard_id, obj);
  }

});