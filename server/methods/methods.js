/**
* Created by David FAIVRE-MAÇON  (12 May 2024)
* https://www.davidfm.me
*/

const { Parser } = require('json2csv');
const fs = require('fs');
const path = require('path');

import { json2csv } from 'json-2-csv';

Meteor.methods({

  bootRemove(id) {
    if (Roles.userIsInRole(Meteor.user(), ['webmaster']))
      Boots.remove(id);
  },

  exportExcel(hard_id, keyName) {
    const keyId = appHd.getKeyId(hard_id, keyName);
    const collection = DataByKey[keyId].find({}, { fields: { cle: 1, valeur: 1, date: 1 } }).fetch();
    const heading = true; // Optional, defaults to true
    const delimiter = ";"; // Optional, defaults to ",";
    return exportcsv.exportToCSV(collection, heading, delimiter);
  },

  async exportCollectionToCSV(hard_id, keyName) {
    const keyId = appHd.getKeyId(hard_id, keyName);
    const data = DataByKey[keyId].find({}, { fields: { cle: 1, valeur: 1, date: 1 } }).fetch();




    // const fields = ['cle', 'valeur', 'date']; // Remplacez par les noms des champs de votre collection
    try {
      const csv = json2csv(data, {
        delimiter: {
          wrap: '"', // Double quote (") character
          field: ';', // Comma field delimiter
          eol: '\n' // New line character
        },
        keys: [{
          "field": "_id", // required
          "title": "id", // optional
          "wildcardMatch": false, // optional - default: false
        }, {
          "field": "cle", // required
          "title": "cle", // optional
          "wildcardMatch": false, // optional - default: false
        }, {
          "field": "valeur", // required
          "title": "valeur", // optional
          "wildcardMatch": false, // optional - default: false
        }, {
          "field": "date", // required
          "title": "date", // optional
          "wildcardMatch": false, // optional - default: false
        }]
      })

      // Écrire le CSV dans un fichier temporaire
      const tempFilePath = path.join(process.env.PWD, 'temp.csv');
      fs.writeFileSync(tempFilePath, csv);

      // Lire le fichier temporaire et le renvoyer au client
      const fileContent = fs.readFileSync(tempFilePath, 'utf-8');
      fs.unlinkSync(tempFilePath); // Supprimer le fichier temporaire après l'avoir lu

      return fileContent;
    } catch (err) {
      console.error(err);
      throw new Meteor.Error('export-failed', 'Échec de l\'export CSV');
    }







    ///
    const fields = ['cle', 'valeur', 'date']; // Remplacez par les noms des champs de votre collection

    try {
      const json2csvParser = new Parser({ fields });
      const csv = json2csvParser.parse(data);

      // Écrire le CSV dans un fichier temporaire
      const tempFilePath = path.join(process.env.PWD, 'temp.csv');
      fs.writeFileSync(tempFilePath, csv);

      // Lire le fichier temporaire et le renvoyer au client
      const fileContent = fs.readFileSync(tempFilePath, 'utf-8');
      fs.unlinkSync(tempFilePath); // Supprimer le fichier temporaire après l'avoir lu

      return fileContent;
    } catch (err) {
      console.error(err);
      throw new Meteor.Error('export-failed', 'Échec de l\'export CSV');
    }
  },

  bugsDeleteByHardId(hard_id) {
    // je dois être connecté
    if (!this.userId) throw new Meteor.Error("not-authorized");
    Bugs.remove({ hard_id: hard_id });
  },

  orderConfigQui(quiId, newOrder) {
    // je dois être connecté
    if (!Meteor.userId()) throw new Meteor.Error("not-authorized");
    Hardwares.update({ _id: quiId }, { $set: { order: newOrder } });
  },

  orderConfigCle(hard_id, cle, newOrder) {
    // je dois être connecté
    if (!Meteor.userId()) throw new Meteor.Error("not-authorized");
    Keys.update(
      { hard_id: hard_id, name: cle },
      { $set: { order: newOrder } }
    );
  },

  getAbsoluteLastData(options) {
    // options : {onlyMine : boolean(default:true)}
    options = options || {};
    if (options.onlyMine === undefined) options.onlyMine = true;
    //FIXME les public aussi? qui ne m'appartienne pas?

    if (options.onlyMine) {
      // je dois être connecté
      if (!Meteor.userId()) throw new Meteor.Error("not-authorized");

      const hard_ids = Hardwares.find({ owner_id: Meteor.userId() }).map(function (hard) {
        return hard._id;
      });
      const lastKey = Keys.findOne({ hard_id: { $in: hard_ids } }, { sort: { lastDate: -1 } });
      if (!lastKey) return false;
      const lastHard = Hardwares.findOne(lastKey.hard_id);
      return { lastHard: lastHard, lastKey: lastKey };
    }
    else {
      // FIXME only mine(if connected) and public
      //Keys.findOne({hard_id: {$in: hard_ids}}, {sort: {lastDate: -1}});
    }
  },

  forumReponseUpdate(post_id, body) {
    ForumReponse.update(post_id, { $set: { body: body } });
  },

  getMyIP() {
    // console.log(this);
    if (this.connection.httpHeaders && this.connection.httpHeaders['x-forwarded-for'])
      return this.connection.httpHeaders['x-forwarded-for'];
    else
      return this.connection.clientAddress;
    // return Meteor.user().status.lastLogin.ipAddr
  },

  'add-hard-by-ip'(ip, port) {
    console.log(ip, port);
    const date = Math.round(new Date().getTime() / 1000);
    console.log(date);

    const hard = Hardwares.findOne(hard_id);

    if (hard) {
      if (hard.owner_id === Meteor.userId()) {
        alert('Cet appareil vous est déja attribué'); // FIXME : notification
      }
      else if (hard.owner_id !== undefined) {
        alert('Cet appareil est déja attribué'); // FIXME : notification
      }
      else if (hard.owner === undefined) {
        Hardwares.update({ _id: hard._id }, { $set: { owner_id: Meteor.userId() } });
        alert('Appareil enregistré!'); // FIXME : modal
      }
    }

    //HTTP.get("http://" + ip + ":" + port + "/etat?cle=LIRE_ID&valeur=15&mode=eeprom", function (error, result) {
    //  if (error) {
    //    return false;
    //  } else {
    //    return result;
    //  }
    //});

  }

});
