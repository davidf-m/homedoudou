Meteor.methods({

  'rooms.insert': function (name) {
    const uId = this.userId;
    if (!uId) return;

    Rooms.insert({
      name: name,
      userId: uId
    });
  },

  async 'rooms.update'(id, obj) {
    //FIXME security
    const uId = this.userId;
    if (!uId) return;

    Rooms.updateAsync({ _id: id, userId: uId }, { $set: obj });
  },

  'rooms.remove': function (id) {
    const uId = this.userId;
    if (!uId) return;

    Rooms.remove(id);
  },

  'rooms.shared': function (id, shared = []) {
    //FIXME security
    const uId = this.userId;
    if (!uId) return;

    Rooms.update({ _id: id }, { $set: { shared: shared } });
  },

  'rooms.set': function (keyId, roomIds) {
    const uId = this.userId;
    if (!uId) return;

    Keys.update(keyId, { $set: { rooms: roomIds } });
  },

  'rooms.scenario.set': function (id, roomIds) {
    const uId = this.userId;
    if (!uId) return;

    Scenarios.update(id, { $set: { rooms: roomIds } });
  },

  'rooms.show': function (room) {
    const uId = this.userId;
    if (!uId) return;

    Rooms.update({ _id: room._id }, { $set: { show: !room.show } });
  }

});
