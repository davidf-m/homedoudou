Meteor.methods({

  'Mhz433.insert': function (form) {
    //FIXME security, check if hard is mine?
    if (!this.userId) return;

    return Mhz433.insert(form);
  },

  async 'Mhz433.update'(id, set) {
    //FIXME security
    const uId = this.userId;
    if (!uId) return;

    return Mhz433.updateAsync(id, { $set: set });
  },

  'Mhz433.remove': function (id) {
    //FIXME security
    const uId = this.userId;
    if (!uId) return;

    return Mhz433.remove(id);
  },

});