Meteor.methods({

  async 'setPassword'(password) {
    if (!this.userId) throw new Meteor.Error("not-logged-in", "Must be logged in");

    return Accounts.setPasswordAsync(this.userId, password);
  },

  userSettingsUpdate: function (key, value) {
    if (!this.userId) throw new Meteor.Error("not-logged-in", "Must be logged in");
    // update variable

    const update = { $set: {} };
    update.$set['settings.' + key] = value;
    return Meteor.users.update(this.userId, update);
  },

  userUpdate: function (id, form) {
    if (!dfm.isWebmaster()) throw new Meteor.Error("not-logged-in", "Must be logged in");
    // update variable

    return Meteor.users.update(id, { $set: form });
  },
});
