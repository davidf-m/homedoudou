Meteor.methods({

  'lastLog': function (id) {
    const last = Logs.findOne({ hard_id: id }, { sort: { date: -1 } });
    return last ? last.date : null;
  },

  'logsRemove': function (hard_id) {
    if (!dfm.isAdmin()) return dfm.throwError("not-logged-in", "Must be logged in");

    if (hard_id === "all")
      Logs.rawCollection().drop();
    else
      Logs.remove({ hard_id: hard_id });
  },

  async 'logs.insert'(obj) {
    /*
    {
      ip: ip,
      hard_id: string,
      logs: [string],
      ? tags:[string]
      ? type: 'error'
    }
    */

    if (!obj) return;
    if (!obj.hard_id) obj.hard_id = "undefined";
    if (!obj.logs) obj.logs = [];

    if (obj.log) {
      obj.logs.push({ log: obj.log });
      delete obj['log'];
    }

    if (!obj.date) obj.date = new Date();

    obj.logs.forEach((log, i) => {
      if (typeof log === 'string') {
        obj.logs[i] = { log: log };
      }
    })

    if (obj.tags)
      obj.tags.forEach((tag) => {
        Configs.upsertAsync({ sourceId: "tag", value: tag }, { $set: { sourceId: "tag", value: tag } });
      })
    return Logs.insertAsync(obj);
  },

});
