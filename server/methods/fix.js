Meteor.methods({

  webmasterActionsFix(json) {
    if(!dfm.isWebmaster()) throw new Meteor.Error("not-authorized");

    // console.log(json);
    JSON.parse(json).forEach(function (action) {
      // console.log(action._id);

      if(!Scenarios.findOne(action._id)) {
        Scenarios.insert(action)
        console.log("action inserted " + action.name)
      }
      else Scenarios.update(action._id, action)

    })
  },

});
