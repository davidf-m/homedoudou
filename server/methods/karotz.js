import fetch from 'node-fetch';

Meteor.methods({

  'karotz.cmd.update.insert': function (obj) {
    HMD_Karotz.update({ type: 'cmdUpdate' }, { $set: { "deleted": new Date() } }, { multi: true });
    obj.cmdKarotz.forEach(function (value, idx) {
      if (value) HMD_Karotz.insert({ type: 'cmdUpdate', value: value, idx: idx })
    });
    return true;
  },

  'karotz.radios.update.insert': function (obj) {
    HMD_Karotz.remove({ type: 'radios' });
    obj.radiosKarotzName.forEach(function (name, idx) {
      const url = obj.radiosKarotzUrl[idx];
      if (name || url) {
        HMD_Karotz.insert({ type: 'radios', name: name, url: url, idx: idx })
      }
    });
    return true;
  },

  async 'karotz-command'(karotz, command) {
    if (!karotz || karotz.type !== 'karotz')
      return false;

    const url = "http://" + karotz.ip + ":" + karotz.port + command;
    const response = await fetch(url, {
      method: 'GET', // *GET, POST, PUT, DELETE, etc.
      insecureHTTPParser: true
    });
    const result = await response.text(); // parses JSON response into native JavaScript objects
    return result;

  }

});
