Meteor.methods({

  'adping.update.ip': function (id, index, ip) {
    const uId = this.userId;
    if(!uId) return;
    const hard = Hardwares.findOne(id);
    hard.devices[index].ip = ip;
    delete hard._id;
    Hardwares.update(id, {$set: hard});
  },

  'adping.update.disableNotifications': function (id, index, disableNotifications) {
    const uId = this.userId;
    if(!uId) return;
    const hard = Hardwares.findOne(id);
    hard.devices[index].disableNotifications = disableNotifications;
    delete hard._id;
    Hardwares.update(id, {$set: hard});
  }

});