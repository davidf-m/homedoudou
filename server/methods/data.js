Meteor.methods({

  dataEdit(data, form) {
    if(!Meteor.userId()) throw new Meteor.Error("not-authorized");
    const hard = Hardwares.findOne(data.hard_id);
    if(!hard || (hard.owner_id !== Meteor.userId() && !dfm.isWebmaster())) throw new Meteor.Error("not-authorized");
    DataByKey[data.cleId].update(data._id, {$set: form});
  },

  async webmasterDbsList() {
    //if(!dfm.isWebmaster()) throw new Meteor.Error("not-authorized");

    const db = MongoInternals.defaultRemoteCollectionDriver().mongo.db;
    const docs = await db.listCollections().toArray().then(async docs => {
      return await docs.map(async doc => {
        let hard = undefined;
        let key = undefined;
        if(doc.name.indexOf('DataByKey_') > -1) {
          const keyId = doc.name.slice(10);
          const keyOne = Keys.findOne(keyId);
          if(keyOne) {
            key = keyOne;
            hard = Hardwares.findOne(keyOne.hard_id);
          }
        }
        return {name: doc.name, key, hard, count: await db.collection(doc.name).count()}
      });
    });
    return Promise.all(docs);
  },

  webmasterDbRemove(dbName) {
    if(!dfm.isWebmaster()) throw new Meteor.Error("not-authorized");

    return new Mongo.Collection(dbName).rawCollection().drop();
  },

  webmasterDbRemoveOrphanDataByKey() {
    if(!dfm.isWebmaster()) throw new Meteor.Error("not-authorized");

    const db = MongoInternals.defaultRemoteCollectionDriver().mongo.db;
    db.listCollections().toArray().then(docs => {
      docs.map(doc => {
        if(doc.name.indexOf('DataByKey') > -1) {
          const key = Keys.findOne(doc.name.slice(10))
          if(!key) db.collection(doc.name).drop();
        }
      });
    });

    console.log('webmasterDbRemoveOrphanDataByKey');
  },

  webmasterDbsGetKey(key_id) {
    if(!dfm.isWebmaster()) throw new Meteor.Error("not-authorized");

    return Keys.findOne(key_id)
  },

  webmasterDbsGetHard(hard_id) {
    if(!dfm.isWebmaster()) throw new Meteor.Error("not-authorized");

    return Hardwares.findOne(hard_id)
  },

  webmasterDbMoveDataKey(keyId, form) {
    DataByKey[keyId].find().fetch().forEach((data) => {
      const newData = {
        cle: form.cle,
        cleId: form.cleId,
        date: data.date,
        hard_id: form.hard_id,
        valeur: data.valeur,
      }
      DataByKey[form.cleId].insert(newData);
    });
    // DataByKey[keyId].update({}, {$set: form}, {multi: true});
  }
});

