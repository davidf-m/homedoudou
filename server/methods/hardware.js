/**
 * Created by David on 18/05/2015.
 */

import { HTTP } from "meteor/http";

const timeout = 20000;

Meteor.methods({

  orphelinsSameIp: function () {
    const ip = Meteor.user().status.lastLogin.ipAddr;
    return Hardwares.find({ ip: ip }).fetch();
  },

  hardwareClaimDDP: function (sessionObj, hard_id) {
    // Version SOCKET
    if (!hard_id) {
      // New Hardware
      hard_id = Hardwares.insert({
        type: "arduinodoudou",
        owner_id: Meteor.userId()
      });
      appHd.ddpSend(sessionObj._id, { mode: "EEPROM", cle: "RAZ_BOOT", valeur: "" });
      appHd.ddpSend(sessionObj._id, { mode: "EEPROM", cle: "MAJ_ID", valeur: hard_id });
      appHd.ddpSend(sessionObj._id, { mode: "EEPROM", cle: "FIN", valeur: "" });
    }
    else {
      // Old Hardware
      const HardOne = Hardwares.findOne(hard_id);
      appHd.ddpSend(sessionObj._id, { mode: "EEPROM", cle: "RAZ_BOOT", valeur: "" });
      appHd.ddpSend(sessionObj._id, { mode: "EEPROM", cle: "MAJ_ID", valeur: hard_id });
      if (HardOne.devices?.length) {
        HardOne.devices.forEach(function (device) {
          if (device.child) {
            appHd.ddpSend(sessionObj._id, { mode: "EEPROM", cle: "MAJ_DEVICE", broche: "100", type: device.broche });
            appHd.ddpSend(sessionObj._id, { mode: "EEPROM", cle: "MAJ_DEVICE", broche: "100", type: device.type_id });
          }
          else {
            appHd.ddpSend(sessionObj._id, { mode: "EEPROM", cle: "MAJ_DEVICE", broche: device.broche, type: device.type_id });
          }
          if (device.type_id === 3 || device.type_id === 10) {
            Keys.update({ hard_id: HardOne._id, name: "BROCHE_" + device.broche }, {
              $set: {
                hard_id: HardOne._id,
                name: "BROCHE_" + device.broche,
                device: device
              }
            }, { upsert: true });
          }
        });
      }
      appHd.ddpSend(sessionObj._id, { mode: "EEPROM", cle: "FIN", valeur: "" });
    }

    return hard_id;
  },

  hardwareClaim: function (type, ip, port) {
    // Version HTTP
    const hard_id = Hardwares.insert({
      type: type,
      ip: ip,
      port: port,
      owner_id: Meteor.userId()
    });
    const result = {
      type: type,
      hard_id: hard_id
    };

    if (type === 'arduinodoudou') {
      try {
        result.dialogue = HTTP.get("http://" + ip + ":" + port + "/etat?cle=MAJ_ID&valeur=" + hard_id + "&mode=eeprom", { timeout: timeout });
        if (result.dialogue && result.dialogue.statusCode === 200) {
          if (result.dialogue.data.maj_id === hard_id) {
            try {
              HTTP.get("http://" + ip + ":" + port + "/etat?cle=fin&valeur=fin&mode=eeprom", { timeout: timeout });
            } catch (error) {
              appHd.messages.error('ip : ' + ip + ' port: ' + port, 'Impossible de se connecter à l\'arduino', error);
            }
          }
          else
            appHd.messages.error('maj_id : ' + hard_id, 'Mauvais retour identifiant');
        }
        else
          appHd.messages.error('status code : ' + result.dialogue.statusCode, 'Status code de connexion different de 200');
      } catch (error) {
        appHd.messages.error('ip : ' + ip + ' port: ' + port, 'Impossible de se connecter à l\'arduino', error);
      }
    }
    return result
  },

  hardwareRaz: function (hard_id) {
    appHd.logs({
      hard_id: hard_id || '',
      log: "hardware Remise à zéro",
      tags: ['reboot']
    });
    appHd.talkToArduino(hard_id, { mode: "eeprom", cle: "RAZ_BOOT", valeur: "" });
    return appHd.talkToArduino(hard_id, { mode: "eeprom", cle: "ARDUINO_REBOOT", valeur: "" });
  },

  hardwareReset: function (hard_id) {
    appHd.logs({
      hard_id: hard_id || '',
      log: "hardware Reboot",
      tags: ['reboot']
    });
    return appHd.talkToArduino(hard_id, { mode: "eeprom", cle: "ARDUINO_REBOOT", valeur: "" });
  },

  hardwareDevicesAdd: function (hard, maj_id, devices) {
    const sessionId = appHd.isDdpConnected(hard._id);
    if (sessionId) {
      console.log('maj ddp');
      appHd.ddpSend(sessionId, { mode: "EEPROM", cle: "RAZ_BOOT", valeur: "" }); // reinitialise les variables vives, pas l'eeprom.
      if (maj_id) appHd.ddpSend(sessionId, { mode: "EEPROM", cle: "MAJ_ID", valeur: hard._id });
      devices.forEach(function (device) {
        if (device.child) {
          appHd.ddpSend(sessionId, { mode: "EEPROM", cle: "MAJ_DEVICE", broche: "100", type: device.broche });
          appHd.ddpSend(sessionId, { mode: "EEPROM", cle: "MAJ_DEVICE", broche: "100", type: device.type_id });
        }
        else {
          appHd.ddpSend(sessionId, { mode: "EEPROM", cle: "MAJ_DEVICE", broche: device.broche, type: device.type_id });
        }
        if (device.type_id === 3 || device.type_id === 10) {
          // Si le device est de type relai il faut ajouter key ici.
          Keys.update({ hard_id: hard._id, name: "BROCHE_" + device.broche }, {
            $set: {
              hard_id: hard._id,
              name: "BROCHE_" + device.broche,
              device: device
            }
          }, { upsert: true });
        }
      });
      appHd.ddpSend(sessionId, { mode: "EEPROM", cle: "FIN", valeur: "" });

      return { ok: 1 };
    }
    else {
      console.log('maj http');
      try {
        const result = HTTP.get("http://" + hard.ip + ":" + hard.port + "/etat?cle=RAZ_BOOT&valeur=rien&mode=EEPROM", { timeout: timeout });

        if (result.data.raz_boot === "fait") {
          if (maj_id) {
            const return_maj_id = HTTP.get("http://" + hard.ip + ":" + hard.port + "/etat?cle=MAJ_ID&valeur=" + hard._id + "&mode=EEPROM", { timeout: timeout });
            console.log('127 return_maj_id', return_maj_id);
            if (return_maj_id.data.v !== hard._id)
              return { ok: 'nok maj id' };
          }

          const confOk = { ok: 1, devices: devices };
          console.log('start maj devices');
          devices.forEach(function (device) {
            console.log(device);
            if (device.child) {
              HTTP.get("http://" + hard.ip + ":" + hard.port + "/etat?mode=EEPROM&cle=MAJ_DEVICE&broche=100&type=" + device.broche, { timeout: timeout });
              HTTP.get("http://" + hard.ip + ":" + hard.port + "/etat?mode=EEPROM&cle=MAJ_DEVICE&broche=100&type=" + device.type_id, { timeout: timeout });
              // FIXME analyser le retour synchro
            }
            else {
              const resultD = HTTP.get("http://" + hard.ip + ":" + hard.port + "/etat?mode=EEPROM&cle=MAJ_DEVICE&broche=" + device.broche + "&type=" + device.type_id, { timeout: timeout });
              if (resultD.data.broche * 1 !== device.broche * 1 || resultD.data.type * 1 !== device.type_id * 1)
                confOk.ok = {
                  message: 'nok conf device ',
                  resultD: resultD,
                };
              else
                confOk.ok = 1;

              confOk[resultD.data.type] = device.type_id;
              confOk[resultD.data.broche] = device.broche;
              console.log('resultD.data.type: ', resultD.data.type);
            }
            if (device.type_id === 3 || device.type_id === 10) {
              Keys.update({ hard_id: hard._id, name: "BROCHE_" + device.broche }, {
                $set: {
                  hard_id: hard._id,
                  name: "BROCHE_" + device.broche,
                  device: device
                }
              }, { upsert: true });
            }
          });

          console.log('confok: ', confOk.ok);
          if (confOk.ok === 1) {
            const resultF = HTTP.get("http://" + hard.ip + ":" + hard.port + "/etat?cle=FIN&valeur=yo&mode=EEPROM", { timeout: timeout });
            if (resultF.data.server_valide === "conf_fini") {
              //FIXME Mettre l'arduino validé
              return confOk
            }
          }
          else {
            return confOk;
          }
        }
      } catch (e) {
        return e;
      }
    }
  },

  hardwareBrocheCommentaireAdd: function ({ hardId, broche, comment }) {
    const hardOne = Hardwares.findOne(hardId);
    const devices = hardOne.devices;
    devices.forEach(function (device) {
      if (device.broche === broche) {
        device.comment = comment;
      }
    });
    // log(devices)
    Hardwares.update(hardId, { $set: { devices: devices } });
    log('hardwareBrocheCommentaireAdd', hardOne);

  },

  askEtat: function (hard_id) {
    return appHd.talkToArduino(hard_id, { mode: "etat", cle: "tout", valeur: "lire" });
  },

  lireId: function (hard_id) {
    return appHd.talkToArduino(hard_id, { mode: "eeprom", cle: "lire_id", valeur: "" });
  },

  deviceDelete: function (device_id) {
    Device.remove(device_id);
  },

  arduinoCommand: function (hard_id, m, k, v) {
    return appHd.talkToArduino(hard_id, { mode: m, cle: k, valeur: v });
  },

  setArduinoListening433: function (hard_id, state) {
    return appHd.setArduinoListening433(hard_id, state);

  }
});
