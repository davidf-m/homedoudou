Meteor.methods({

  'keys.update'(keyId, form) {
    // je dois au moins être connecté
    if (!Meteor.userId()) throw new Meteor.Error("not-authorized");

    const key = Keys.findOne(keyId);
    if (!key) throw new Meteor.Error("key not exist");
    if (form._id) delete form._id;

    const hard = Hardwares.findOne(key.hard_id);
    // je dois être webmaster ou l'appareil doit m'appartenir pour modifier cette clé
    if (hard.owner_id !== Meteor.userId() && !dfm.isWebmaster()) throw new Meteor.Error("not-authorized");

    form.updateAt = { date: new Date(), by: Meteor.userId() };
    Keys.update(key._id, { $set: form });
  },

  keyDeleteById(keyId) {
    // l'utilisateur doit pouvoir effacer une cle et toutes ces données associés

    //FIXME
    // vérifier si les données ou les clef ou l'appareil ne sont pas utilisées, mentionnées  ailleurs, scénarios, etc...
    // boot, etats, bugs, group, scénarios, notifs?

    // je dois au moins être connecté
    if (!Meteor.userId()) throw new Meteor.Error("not-authorized");

    const key = Keys.findOne(keyId);
    if (!key) throw new Meteor.Error("this key don't exist");
    const hard = Hardwares.findOne({ _id: key.hard_id });
    if (hard) {

      // je dois être webmaster ou l'appareil doit m'appartenir pour effacer cette clé
      if (hard.owner_id !== Meteor.userId() && !dfm.isWebmaster()) throw new Meteor.Error("not-authorized");

      // j'efface les données associées à la clé
      DataByKey[keyId].rawCollection().drop();

      // j'efface la clé
      Keys.remove({ _id: keyId });
    }
    else throw new Meteor.Error("this hardware don't exist");
  },

  keySwitchShow(keyId) {
    // je dois au moins être connecté
    if (!Meteor.userId()) throw new Meteor.Error("not-authorized");

    const key = Keys.findOne(keyId);
    const hard = Hardwares.findOne({ _id: key.hard_id });

    // je dois être webmaster ou l'appareil doit m'appartenir pour modifier cette clé
    if (hard.owner_id !== Meteor.userId() && !dfm.isWebmaster()) throw new Meteor.Error("not-authorized");

    Keys.update({ _id: key._id }, { $set: { show: !key.show } });
  },

  keySwitchPublic(keyId) {
    // je dois au moins être connecté
    if (!Meteor.userId()) throw new Meteor.Error("not-authorized");

    const key = Keys.findOne(keyId);
    const hard = Hardwares.findOne({ _id: key.hard_id });

    // je dois être webmaster ou l'appareil doit m'appartenir pour modifier cette clé
    if (hard.owner_id !== Meteor.userId() && !dfm.isWebmaster()) throw new Meteor.Error("not-authorized");

    Keys.update({ _id: key._id }, { $set: { public: !key.public } });
  },

  keySwitchNotified(keyId) {
    // je dois au moins être connecté
    if (!Meteor.userId()) throw new Meteor.Error("not-authorized");

    const key = Keys.findOne(keyId);
    const hard = Hardwares.findOne({ _id: key.hard_id });

    // je dois être webmaster ou l'appareil doit m'appartenir pour modifier cette clé
    if (hard.owner_id !== Meteor.userId() && !dfm.isWebmaster()) throw new Meteor.Error("not-authorized");

    Keys.update({ _id: key._id }, { $set: { notified: !key.notified } });
  },

  keyUpdateHowLongHour(data) {
    if (!data.howLongHour) return;
    const keyOne = Keys.findOne({ hard_id: data.hard_id, name: data.name });
    if (keyOne && keyOne.howLongHour !== data.howLongHour)
      Keys.update(keyOne._id, { $set: { howLongHour: data.howLongHour } });
  },

  keyPushRoom(keyId, roomId) {
    // je dois au moins être connecté
    if (!Meteor.userId()) throw new Meteor.Error("not-authorized");

    const key = Keys.findOne(keyId);
    if (!key) throw new Meteor.Error("key not exist");

    const hard = Hardwares.findOne(key.hard_id);
    // je dois être webmaster ou l'appareil doit m'appartenir pour modifier cette clé
    if (hard.owner_id !== Meteor.userId() && !dfm.isWebmaster()) throw new Meteor.Error("not-authorized");
    if (!key.rooms) Keys.update(keyId, { $set: { rooms: [] } })
    Keys.update(keyId, { $push: { rooms: roomId } })
  },

  keyPullRoom(keyId, roomId) {
    // je dois au moins être connecté
    if (!Meteor.userId()) throw new Meteor.Error("not-authorized");

    const key = Keys.findOne(keyId);
    if (!key) throw new Meteor.Error("key not exist");

    const hard = Hardwares.findOne(key.hard_id);
    // je dois être webmaster ou l'appareil doit m'appartenir pour modifier cette clé
    if (hard.owner_id !== Meteor.userId() && !dfm.isWebmaster()) throw new Meteor.Error("not-authorized");
    Keys.update(keyId, { $pull: { rooms: roomId } })
  },

  cumulativeKeyInsertRoom(form) {
    // je dois au moins être connecté
    if (!Meteor.userId()) throw new Meteor.Error("not-authorized");
    CumulKeys.insert({ roomsId: form.roomsId, ownerId: Meteor.userId(), lines: form.lines, created: { at: new Date(), by: Meteor.userId() } });
  },

  cumulativeKeyRemoveRoom(id) {
    // je dois au moins être connecté
    if (!Meteor.userId()) throw new Meteor.Error("not-authorized");
    CumulKeys.remove(id);
  },
});