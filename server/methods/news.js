Meteor.methods({

  'news-insert': function (news) {
    if(!dfm.isWebmaster()) return;

    news.created = new Date();
    news.flash = false;
    News.insert(news);
  },

  'news-update': function (id, set) {
    if(!dfm.isWebmaster()) return;

    News.update(id, {$set: set});
  },

  'news-delete': function (id) {
    if(!dfm.isWebmaster()) return;

    News.remove(id);
  }

});
