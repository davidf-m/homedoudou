Meteor.methods({

  hardUpdate(form) {
    // je dois être connecté
    if(!Meteor.userId()) throw new Meteor.Error("not-authorized");

    const hard_id = form._id;
    delete form._id;

    const hard = Hardwares.findOne(hard_id);
    // je dois être webmaster ou l'appareil doit m'appartenir pour le modifier
    if(hard.owner_id !== Meteor.userId() && !dfm.isWebmaster()) throw new Meteor.Error("not-authorized");

    // update name in eeprom of arduinodoudou
    //FIXME remonter les erreurs
    if(hard.type === 'arduinodoudou' && hard.name !== form.name) {
      appHd.arduinoDoudou.command(hard, {mode: "eeprom", cle: "maj_arduino", valeur: form.name});
    }

    if(hard.type === 'arduinodoudou' && hard.emails !== form.emails) {
      const emails = form.emails.replace(/\s/g, '').split(',');
      console.log(emails);
      if(emails[0]) appHd.arduinoDoudou.command(hard, {mode: "eeprom", cle: "email_adresse1", valeur: emails[0]});
      if(emails[1]) appHd.arduinoDoudou.command(hard, {mode: "eeprom", cle: "email_adresse2", valeur: emails[1]});
      if(emails[2]) appHd.arduinoDoudou.command(hard, {mode: "eeprom", cle: "email_adresse3", valeur: emails[2]});
      appHd.arduinoDoudou.command(hard, {mode: "eeprom", cle: "nb_adresse_utilise", valeur:emails.length});
    }
    return Hardwares.update(hard_id, {$set: form})
  },

  hardDeleteById(id) {
    // l'utilisateur doit pouvoir effacer un appareil et toutes ces données associés

    //FIXME
    // vérifier si les données ou les clef ou l'appareil ne sont pas utilisées, mentionnées  ailleurs, scénarios, etc...
    // scénarios, notifs?

    //FIXME logguer les erreurs

    // je dois être connecté
    if(!Meteor.userId()) throw new Meteor.Error("not-authorized");

    // je cherche l'appareil
    const hardOne = Hardwares.findOne({_id: id});

    // je dois être webmaster ou l'appareil doit m'appartenir pour le supprimer
    if(hardOne.owner_id !== Meteor.userId() && !dfm.isWebmaster()) throw new Meteor.Error("not-authorized");

    // Si l'appareil existe
    if(hardOne) {

      //// Keys
      // je cherche toutes les cles attachées à ce matériel
      const keys = Keys.find({hard_id: hardOne._id}).fetch();
      // j'efface toutes les données de chaque clé.
      for(let i = 0; i < keys.length; i++) {
        const key = keys[i];
        DataByKey[key._id].remove();
      }
      // j'efface toutes les clés.
      Keys.remove({hard_id: hardOne._id});

      // Boots
      Boots.remove({hard_id: hardOne._id});
      BootTemp.remove({hard_id: hardOne._id});

      // Etats
      Etats.remove({hard_id: hardOne._id})
      EtatTemp.remove({hard_id: hardOne._id})

      // Bugs
      Bugs.remove({hard_id: hardOne._id})

      // j'efface le matériel.
      Hardwares.remove({_id: hardOne._id});
    }
    else throw new Meteor.Error("this hardware don't exist");
  }
});