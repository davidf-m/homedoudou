Meteor.methods({

  'nabaztag-command': function (nabaztag, command) {
    if(!nabaztag || nabaztag.type !== 'nabaztag-pi')
      return false;

    if(typeof command === 'object') command = JSON.stringify(command);
    command = {msg: "command", command: command};

    return appHd.ddpSend(nabaztag.ddpId, command);
  }

});
