Meteor.methods({

  'data_remove': function (cleId, dataId) {
    // FIXME securité
    // if (Roles.userIsInRole(Meteor.user(), ['webmaster']))

    //FIXME replacing by DataByKey
    DataByKey[cleId].remove(dataId);
  },

  'dataCount': function (hard_id, key_name) {
    if(!hard_id && !key_name) return 'error';

    const keyId = appHd.getKeyId(hard_id, key_name);
    if(key_name)
      return DataByKey[keyId].find().count();
    else {
      let count = 0;
      for(let i = 0; i < keyId.length; i++) {
        count += DataByKey[keyId[i]].find().count();
      }
      return count;
    }
  },

});
