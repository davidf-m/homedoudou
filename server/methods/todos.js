Meteor.methods({

  insert: function(listid, text) {
    Todos.insert({
    listId: listid,
    text: text,
    checked: false,
    createdAt: new Date(),
    rank: Todos.find({listId: listid}).count() + 1
    });
    Lists.update(listid, {$inc: {incompleteCount: 1}});
  },
  
  updateTodoRank: function(todoid, newRank) {
    Todos.update({_id: todoid}, {$set: {rank: newRank}});
  },
  
  removeTodo: function(id) {
    var t = Todos.findOne({_id: id});
    if (!t.checked)
      Lists.update(t.listId, {$inc: {incompleteCount: -1}});
    Todos.remove(id);
  },

  updateTodoDone: function(todoId, done) {
    var t = Todos.findOne({_id: todoId});
    Todos.update(todoId, {$set: {checked: done}});
    Lists.update(t.listId, {$inc: {incompleteCount: done ? -1 : 1}});
  },

  updateTodo: function(todoId, text) {
    Todos.update(todoId, {$set: {text: text}});
  }

});


