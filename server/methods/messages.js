Meteor.methods({

  messagesInsert(obj) {
    Messages.insert(obj);
  },

  async 'messages.close'(id) {
    Messages.updateAsync(id, { $set: { close: true } })
  },

  messagesDelete(id) {
    if (dfm.isAdmin())
      Messages.remove(id)
  },
});