/**
 * Created by David FAIVRE-MAÇON on 24 january 2020
 * https://www.davidfm.me
 */

Meteor.methods({

  'configs.insert'(data) {
    if(!dfm.isWebmaster()) return;
    Configs.insert(data);
  },

  'configs.update'(id, data) {
    if(!dfm.isWebmaster()) return;
    Configs.update(id, {$set: data});
  },

  'configs.remove'(id) {
    if(!dfm.isWebmaster()) return;
    Configs.remove(id);
  },

  getTime(){
    return new Date();
  }

});