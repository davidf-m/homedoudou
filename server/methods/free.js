Meteor.methods({

  freeConfigUpdate: function (login, key) {
    Meteor.users.update(Meteor.userId(), {$set: {"freeConfig.login": login, "freeConfig.key": key}});
  },

  freeConfigUpdateKeyValue: function (key, value) {
    const set = {};
    set["freeConfig." + key] = value;
    Meteor.users.update(Meteor.userId(), {$set: set});
  }


});