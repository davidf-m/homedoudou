// users
import DfmConnections from 'meteor/davidfm:core/api/accounts/lib/collections.js';

Meteor.publish("adminUsers", function () {
  let u = null;
  if (this.userId) u = Meteor.users.findOne(this.userId);
  if (u && Roles.userIsInRole(u, ['admin', 'webmaster'])) return Meteor.users.find({});
  else return [];
});

Meteor.publish('', function () {
  return Meteor.users.find({ _id: this.userId });
});

Meteor.publish("dfmConnections", function () {
  //FIXME fields filter
  return DfmConnections.find({}, { limit: dfm.usersLimitPublish });
});

Meteor.publish('', function () {
  if (dfm.isWebmaster())
    return NotificationsSubs.find();
  else
    return NotificationsSubs.find({ "user._id": this.userId });
});

Meteor.publish("adminHardwares", function () {
  if (!dfm.isAdmin()) return this.ready();
  return Hardwares.find();
});

Meteor.publish("adminKeys", function () {
  if (!dfm.isAdmin()) return this.ready();
  return Keys.find();
});

Meteor.publish("actionsHardware", function () {

  const User = Meteor.users.findOne(this.userId);
  let sharedWith;
  if (User)
    sharedWith = User.emails[0].address;

  return Hardwares.find({
    "$or": [
      { owner_id: this.userId },
      { shared: { $elemMatch: { "email": sharedWith } } }
    ]
  });
});

Meteor.publish('hardwareMine', function () {
  /* Hardware à publier
   * Mes Hardware
   */

  if (!this.userId) return [];
  return Hardwares.find({ owner_id: this.userId });
});

Meteor.publish('hardwareshared', function () {
  /* Hardware à publier
   * Hardware shared
   */

  if (!this.userId) return [];
  return Hardwares.find({ shared: { $elemMatch: { "email": dfm.getUserEmail(this.userId) } } });
});

Meteor.publish('publicHardwares', function () {
  /* Hardware à publier (exclusif):
   * Hardware dont au moins une clé est publique
   * Hardware qui n'est pas le mien ou partagé avec moi, déja publié...
   */

  const allPublicKeys = Keys.find({ public: true }).fetch();
  let hard_ids = [];
  _.each(allPublicKeys, function (ClePublic) {
    hard_ids.push(ClePublic.hard_id);
  });

  let filter = { _id: { $in: hard_ids } };
  if (this.userId) {
    filter.owner_id = { $ne: this.userId };
    filter.shared = { $not: { $elemMatch: { "email": dfm.getUserEmail(this.userId) } } };
  }

  // FIXME securité ddp?
  return Hardwares.find(filter, { fields: { hard_id: 1, type: 1, alias: 1, name: 1, order: 1, show: 1, unit: 1, shared: 1, ddp: 1, lastSeen: 1 } });
});

Meteor.publish('keysMine', function () {
  /* Cle à publier
   * Clés dont je suis owner
   */

  const HardsMine = Hardwares.find({ owner_id: Meteor.userId() }).fetch();
  let hard_ids = [];
  _.each(HardsMine, function (Hard) {
    hard_ids.push(Hard._id);
  });
  return Keys.find({ hard_id: { $in: hard_ids } });
});

Meteor.publish('publicKeys', function () {
  /* Cle à publier
   * Clés showed
   * Clés est publique
   */

  return Keys.find({ public: true, show: true });
});

Meteor.publish('configsqui', function () {
  /* Qui à publier:
   * - Qui Owner
   * - Qui shared
   */

  if (!this.userId) return [];
  return Hardwares.find({
    "$or": [
      { owner_id: this.userId },
      {
        shared: {
          $elemMatch: {
            "$and": [
              { "email": dfm.getUserEmail(this.userId) }
            ]
          }
        }
      }
    ]
  });
});

Meteor.publish('keys', function () {
  /* Cle à publier
   * Clés dont Qui Owner
   * Qui shared admin
   */

  if (!this.userId) return [];
  let allQuiOwner = Hardwares.find({
    "$or": [
      { owner_id: this.userId },
      {
        shared: {
          $elemMatch: {
            "$or": [
              { "email": dfm.getUserEmail(this.userId) }
            ]
          }
        }
      }
    ]
  }).fetch();
  let hard_ids = [];
  _.each(allQuiOwner, function (quiOwner) {
    hard_ids.push(quiOwner._id);
  });
  return Keys.find({ hard_id: { $in: hard_ids } });
});

Meteor.publish('donnees', function (hard_id, keyName, howLongHour) {
  const keyId = appHd.getKeyId(hard_id, keyName);
  if (howLongHour === "debug")
    return DataByKey[keyId].find({});
  else {
    const now = Date.now();
    const dateAfter = now - (howLongHour * 60 * 60 * 1000);
    return DataByKey[keyId].find({ date: { $gt: dateAfter } });
  }
});

Meteor.publish('dataRange', function (hard_id, keyName, dateFrom, dateTo) {
  const keyId = appHd.getKeyId(hard_id, keyName);
  const now = Date.now();
  if (!dateFrom) dateFrom = now;
  if (!dateTo) dateTo = now - (24 * 60 * 60 * 1000);

  dateFrom *= 1;
  dateTo *= 1;

  return DataByKey[keyId].find({ date: { $gt: dateFrom, $lt: dateTo } });
});

Meteor.publish('boottemp', function () {
  return Boottemp.find();
});

Meteor.publish('configurationBootsServer', function () {
  return Boots.find({ hard_id: "server" });
});

Meteor.publish('configurationBootsNotArchived', function () {
  return [Boots.find({ archived: { $exists: false } }), Device.find()];
});

Meteor.publish('configurationBootsHard', function (hard_id) {
  return [Boots.find({ hard_id: hard_id }), Device.find()];
});

Meteor.publish('etats', function () {
  return Etats.find();
});

Meteor.publish('bugs', function () {
  return Bugs.find({});
});

Meteor.publish('adminLogs', function (query, options) {
  if (!dfm.isAdmin()) return this.ready();
  return Logs.find(query, options);
});


Meteor.publish('todos', function (listId) {
  return Todos.find({ listId: listId });
});

Meteor.publish("forumquestion", function () {
  return ForumQuestion.find();
});

Meteor.publish("forumreponse", function () {
  return ForumReponse.find();
});

Meteor.publish("roles", function () {
  return Meteor.roles.find({});
});

Meteor.publish("contact", function () {
  return Contact.find();
});

Meteor.publish("news", function () {
  return News.find();
});

Meteor.publish("device", function () {
  return Device.find();
});

Meteor.publish("myScrapers", function () {
  return Scrapers.find({ userId: Meteor.userId() });
});

Meteor.publish("usersConnections", function () {
  //FIXME fields filter
  return DfmConnections.find();
});

Meteor.publish('usersPublic', function () {
  let u = null;
  if (this.userId) u = Meteor.users.findOne(this.userId);
  if (u && Roles.userIsInRole(u, ['webmaster'])) return Meteor.users.find({});
  else return Meteor.users.find({},
    {
      fields: {
        'username': 1,
        'status.idle': 1,
        'status.online': 1
      }
    });
});

Meteor.publish("mhz433", function (hard_id) {
  return Mhz433.find({ hard_id: hard_id });
});

publishComposite('myRooms', function () {
  if (!this.userId) return [];
  return {
    find() {
      return Rooms.find({ $or: [{ userId: this.userId }, { shared: this.userId }] });
    },
    children(parentRoom) {
      return [
        {
          find(room) {
            return Keys.find({ rooms: parentRoom._id });
          },
          children(parentKey) {
            return [{
              find(key) {
                return Hardwares.find({ _id: parentKey._id });
              },
            }];
          },
        },
        {
          find(room) {
            return Buttons.find({ rooms: parentRoom._id });
          },
          children(button) {
            if (button.on && button.off)
              return [{
                find() {
                  return Scenarios.find({ scenarioType: "scenario", type: "heater", heaterOn: button.on, heaterOff: button.off })
                },
              }];
          },
        },
        {
          find(room) {
            return Scenarios.find({ scenarioType: "scenario", rooms: parentRoom._id });
          },
        }
      ];
    },
  };
});

Meteor.publish('adminMessages', function () {
  if (!dfm.isAdmin()) return [];
  return Messages.find();
});

// Autopublish, attention ne pas envoyé aux arduinos, nabaztag connectés en ddp !!!

Meteor.publish(null, function () {
  if (dfm.cannotSubscribe(this)) return [];
  return Buttons.find({
    createdBy: this.userId
  });
});

Meteor.publish(null, function () {
  if (dfm.cannotSubscribe(this)) return [];
  return Scenarios.find({ createdBy: Meteor.userId() });
});

Meteor.publish("webmasterActionsFix", function () {
  if (!dfm.isWebmaster()) return [];
  return Scenarios.find({ scenarioType: "action" })
});

Meteor.publish(null, function () {
  // FIXME faire une function
  if (dfm.cannotSubscribe(this)) return [];
  return Configs.find();
});

Meteor.publish(null, function () {
  if (dfm.cannotSubscribe(this)) return [];
  return Messages.find({
    userId: this.userId,
    close: { $ne: true }
  });
});

Meteor.publish(null, function () {
  return CumulKeys.find({
    ownerId: this.userId
  });
});