// use of notifications fcm (Firebase Cloud Messaging)
const admin = require("firebase-admin");
const ms = Meteor.settings;
if(ms && ms.firebase) {
  const serviceAccount = ms.firebase;
  admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: "https://homedoudou-bgrhol.firebaseio.com"
  });

}
else  console.log('settings firebase missing');


Meteor.methods({

  'firebase-sendMessage': function (userToken, message) {
    // console.log(message);

    message = {
      "notification": {
        "title": message,
        "body": message,
        "click_action": "https://www.homedoudou.fr",
        "icon": "https://www.homedoudou.fr/favicon.ico"
      },
      "to": userToken
    };

    const options = {
      headers: {
        "Content-Type": "application/json",
        "Authorization": "key=AAAA9CP5Jpw:APA91bFfvxWpi3yC1rbPQpR5vhrQu53Z3qAQv5lokPoWpXtykl4VG6_ab5XE7IvMgeU51kVQS0gL_abqM1MI0WCoMq2Jy69UCB7-pKMIoZFoLnICMhYt4aSXAb78JQYYxrWwCCtuXudn"
      },
      data: message
    };

    HTTP.post("https://fcm.googleapis.com/fcm/send", options, function (err, result) {
      if(err)
        console.log(err);
      else {
        NotifsHistory.insert({
          message: message,
          result: result
        });
      }
    })
  },

  'firebase-setUserToken': function (token) {
    return Meteor.users.update(Meteor.userId(), {
      $set: {
        "notifs.firebase.token": token,
        "notifs.firebase.enabled": true
      }
    });
  }


});
