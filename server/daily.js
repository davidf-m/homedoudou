import dfmCron from 'meteor/davidfm:core/api/cron/server/class-cron.js';

if (typeof (appHd) === "undefined") appHd = {};

dfmCron.functions["appDaily"] = () => {
  appHd.logsInfo({
    hard_id: "server",
    log: 'appDaily',
  });

  appHd.sendDailyMailToAdmin('Envoi journalier');
  logsClean();
};

const logsClean = function () {
  const dateBefore = Date.now() - (3 * 30 * 24 * 60 * 60 * 1000); //  3 mois
  Logs.remove({ date: { $lt: dateBefore } });
}

appHd.sendDailyMailToAdmin = function (subject) {
  const users = Roles.getUsersInRole('admin').fetch();
  const Hards = Hardwares.find({}, { sort: { owner_id: 1 } }).fetch();

  let lines = "", hardName = "", style = "", confCleName = "";

  let ownerIdBack = null;
  _.each(Hards, function (hard) {
    if (hard.lastSeen) {
      const now = Date.now();
      const since = Math.round(now / 1000 - hard.lastSeen / 1000);
      if (since > 604800) style = null;
      else if (since > 86400) style = 'background-color:#f44336; color:#fff;';
      else if (since > 1800) style = 'background-color:#ff5722; color:#fff';
      else style = 'background-color:#009688; color:#fff';

      let donnees = "";

      const boots = Boots.findOne({ hard_id: hard._id }, { sort: { date: -1 } });
      if (boots) {
        donnees = donnees + " last boot : " + dfm.formatDate({ date: boots.date }) + "</i><br>";
        _.each(boots.boot, function (boot) {
          // confCleName = boot.name;
          // if(confCle.alias) confCleName = confCle.alias;
          if (boot.cle && boot.cle === "date_et_version_arduino")
            donnees = donnees + boot.cle + " : " + boot.valeur + "<br>";
        });
      }

      if (style) {
        if (hard.owner_id !== ownerIdBack)
          lines += '<tr><td style="padding:20px;">' + dfm.getUsername(hard.owner_id) + '</td><td></td></tr>';

        hardName = hard.alias ? hard.alias : '<b>' + 'Inconnu (' + hard._id + ')</b>: '
        lines += '<tr><td style="padding:0px;' + style + '"><a href="https://www.homedoudou.fr/dashboard/' + hard._id + '" style="display:block;padding:10px;color:white;text-decoration:none">' + hardName + '</a></td><td>' + donnees + '</td></tr>';

        ownerIdBack = hard.owner_id;
      }
    }

  });

  const template = subject + '<br><br><table cellpadding="10">' + lines + '</table><br><br><i>Ceci est un mail automatique.</i>';

  _.each(users, function (user) {
    try {
      Email.send({
        from: user.emails[0].address,
        to: user.emails[0].address,
        subject: subject,
        html: template
      });
    } catch (e) {
      console.log(e);
    }
  });
  console.log("sendDailyMailToAdmin to " + users.length + " users, END");
};