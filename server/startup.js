/**
* Created by David FAIVRE-MAÇON
* https://www.davidfm.me
*/

import dfmCron from 'meteor/davidfm:core/api/cron/server/class-cron.js';

Meteor.startup(function () {

  DfmImpersonate.admins = ["webmaster", "admin"]; //FIXME admin can impersonate webmaster?

  Boots.insert({ hard_id: "server", date: new Date() });
  appHd.logsInfo({
    hard_id: "server",
    log: 'Serveur Boot',
    tags: ['server', 'boot']
  });

  const ms = Meteor.settings;

  if (ms.smtp && ms.smtp.username && ms.smtp.password && ms.smtp.server && ms.smtp.port) {

    // Envoie de mail au démarrage de l'application et resumé journalier.
    if (!dfm.isLocalhost()) {

      try {
        appHd.sendDailyMailToAdmin('Le server demarre');
      } catch (e) {
        return e;
      }

      dfmCron.addCron({
        name: 'daily',
        fn: {
          name: 'appDaily',
          attributes: undefined
        },
        options: { time: '0 0 0 * * *' },
        meta: {
          description: 'Envoi journalier',
        }
      });
    }
  }
  else appHd.logsError({
    hard_id: "server",
    log: 'mail settings not configured'
  }, { console: true });

  if (ms.services) {
    if (ms.services.google) {
      ServiceConfiguration.configurations.update(
        { service: "google" }, {
        $set: {
          clientId: ms.services.google.clientId,
          client_email: ms.services.google.client_email,
          secret: ms.services.google.secret,
        },
      },
        { upsert: true },
      );
    }

    if (ms.services.facebook) {
      ServiceConfiguration.configurations.update(
        { service: "facebook" }, {
        $set: {
          appId: ms.services.facebook.appId,
          secret: ms.services.facebook.secret,
        },
      },
        { upsert: true },
      );
    }
  }
});