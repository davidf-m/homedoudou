Meteor.startup(function () {

  // verifie toutes les 5 minutes si une tache est a effectuer
  // Scraper
  const delay = 5 * 60 * 1000;
  Meteor.setInterval(function () {
    const tasks = Scrapers.find().fetch();
    tasks.forEach(function (task) {
      let lastDateDone = task.lastDateDone;
      if(lastDateDone) {
        const CurrentDate = new Date();
        const diff = (CurrentDate.getTime() - lastDateDone.getTime()) / 1000;
        if(diff > task.repeat) {
          Meteor.call('scrapersGet', task._id, function (err, result) {
            if(err) return console.log(err);
            else {
              Scrapers.update(task._id, {$set: {lastDateDone: new Date()}})
            }
          });
        }
      }
    });
  }, delay);

});