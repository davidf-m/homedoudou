HMD_Karotz = new Mongo.Collection('hmd_karotz');

if(Meteor.isClient) {
  Meteor.subscribe('karotz.all');
}

if(Meteor.isServer) {
  Meteor.publish('karotz.all', function () {
    return HMD_Karotz.find();
  });
}