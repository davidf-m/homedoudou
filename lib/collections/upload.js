import {FilesCollection} from 'meteor/ostrio:files';

const HMD_Files = new FilesCollection({
  collectionName: 'hmd_medias',
  allowClientCode: false, // Disallow remove files from Client
  storagePath: Meteor.settings?.obFiles?.storagePath || '/opt/hmd_medias',
  onBeforeUpload(file) {
    // Allow upload files under 10MB, and only in png/jpg/jpeg formats
    if(file.size <= 10485760) {
      return true;
    }
    else {
      return 'Please upload image, with size equal or less than 10MB';
    }
  }
});

if(Meteor.isClient) {
  Meteor.subscribe('files.images.all');
}

if(Meteor.isServer) {
  Meteor.publish('files.images.all', function () {
    return HMD_Files.find().cursor;
  });
}

export default HMD_Files; // To be imported in other files

Meteor.methods({

  'HMD_Files.remove': function (id) {
    if(dfm.isAdmin())
      return HMD_Files.remove(id);
  }

});