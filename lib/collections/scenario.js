Scenarios = new Mongo.Collection('hd_scenarios');

Scenarios.allow({
    insert(userId, doc) {
        // The user must be logged in and the document must be owned by the user.
        return userId && doc.createdBy === userId;
    },

    update(userId, doc, fields, modifier) {
        // Can only change your own documents.
        return doc.createdBy === userId;
    },

    remove(userId, doc) {
        // Can only remove your own documents.
        return doc.createdBy === userId;
    },

    fetch: ['createdBy']
});