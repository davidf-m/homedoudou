Configs = new Mongo.Collection('hmd_configs');
Messages = new Mongo.Collection('hmd_messages');

Rooms = new Meteor.Collection('rooms');
Buttons = new Mongo.Collection('hmd_buttons');
CumulKeys = new Mongo.Collection('hmd_cumulative_keys');

Live = new Mongo.Collection('live');

BootTemp = new Mongo.Collection('boottemp');
Boots = new Mongo.Collection('boots');

EtatTemp = new Mongo.Collection('etattemp');
Etats = new Mongo.Collection('etats');

EepromTemp = new Mongo.Collection('eepromtemp');
Eeprom = new Mongo.Collection('eeeprom');

ForumQuestion = new Meteor.Collection('forumquestion');
ForumReponse = new Meteor.Collection('forumreponse');

Bugs = new Mongo.Collection('bugs');

Logs = new Mongo.Collection('hmd_logs');

Todos = new Mongo.Collection('todos');

Hardwares = new Mongo.Collection('hardwares');
Hardwares.allow({
  update: function (userId, docs, fields, modifier) {
    /* // FIXME: user and doc checks ,
     return true to allow update */
    return !!Meteor.user();
  }
});

Keys = new Mongo.Collection('configCle');

/*
 * Contact
 */

Contact = new Mongo.Collection("contact");

/*
 * Device : sondes, relais...
 */

Device = new Mongo.Collection("device");
Device.allow({
  insert: function (userId, docs, fields, modifier) {
    /* // FIXME: user and doc checks ,
     return true to allow update */
    return dfm.isAdmin();
  },
  update: function (userId, docs, fields, modifier) {
    /* // FIXME: user and doc checks ,
     return true to allow update */
    return dfm.isAdmin();
  },
});

News = new Meteor.Collection('news');
Mhz433 = new Meteor.Collection('mhz_433');

DataByKey = [];
// DataByKey_30 = [];
// DataByKey_365 = [];
DataByKey['withoutConfig'] = new Mongo.Collection('DataByKey_withoutConfig');
// DataByKey_30['withoutConfig'] = new Mongo.Collection('DataByKey_30_withoutConfig');
// DataByKey_365['withoutConfig'] = new Mongo.Collection('DataByKey_365_withoutConfig');

if (Meteor.isClient) {
  Tracker.autorun(() => {
    //si une nouvelle Key est crée, je crée ses collections DataByKey correspondantes
    const c = Keys.find().fetch();
    for (let i = 0; i < c.length; i++) {
      const cElement = c[i];
      if (!DataByKey[cElement._id]) {
        DataByKey[cElement._id] = new Mongo.Collection('DataByKey_' + cElement._id);
        // DataByKey_30[cElement._id] = new Mongo.Collection('DataByKey_30_' + cElement._id);
        // DataByKey_365[cElement._id] = new Mongo.Collection('DataByKey_365_' + cElement._id);
      }
    }
  });
}
else {
  const c = Keys.find().fetch();
  for (let i = 0; i < c.length; i++) {
    const cElement = c[i];
    DataByKey[cElement._id] = new Mongo.Collection('DataByKey_' + cElement._id);
    // DataByKey_30[cElement._id] = new Mongo.Collection('DataByKey_30_' + cElement._id);
    // DataByKey_365[cElement._id] = new Mongo.Collection('DataByKey_365_' + cElement._id);
  }
}

NotifsHistory = new Meteor.Collection('notifs_history');
Scrapers = new Meteor.Collection('hmd_scrapers');

// Push notifications subscriptions
NotificationsSubs = new Mongo.Collection('hmd_notifications_subs');