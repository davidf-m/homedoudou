// NAMESPACE
if(typeof (appHd) === "undefined") appHd = {};

appHd.isDdpConnected = function (hard_id) {
  const HardOne = Hardwares.findOne(hard_id);
  if(HardOne) {
    const Session = appHd.getSessionDdp(HardOne.ddpId);
    if(Session) return HardOne.ddpId
  }
  return false;
};

