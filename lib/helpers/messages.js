// NAMESPACE
if(typeof (appHd) === "undefined") appHd = {};
appHd.messages = {};

appHd.messages.add = function (message, title, details, type) {
  if(typeof message === 'object') {
    message = JSON.stringify(message)
  }
  if(!title) title = 'Message';
  Meteor.call('messagesInsert', {
    title: title,
    text: message,
    details: details,
    type: type || '',
    style: type || '',
    userId: Meteor.userId(),
    createdAt: new Date()
  })
};

appHd.messages.error = function (message, title, details) {
  if(typeof message === 'object') {
    message = JSON.stringify(message)
  }
  if(!title) title = 'Erreur';
  Meteor.call('messagesInsert', {
    title: title,
    text: message,
    details: details,
    type: 'error',
    style: 'error',
    userId: Meteor.userId(),
    createdAt: new Date()
  })
};