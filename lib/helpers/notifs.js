// NAMESPACE
if(typeof (appHd) === "undefined") appHd = {};

appHd.sendNotifs = function (user_id, message) {
  Meteor.call('sendNotificationWeb', user_id, message);
};