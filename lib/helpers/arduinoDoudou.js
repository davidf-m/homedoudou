if(typeof (appHd) === "undefined") appHd = {};
if(typeof (appHd.arduinoDoudou) === "undefined") appHd.arduinoDoudou = {};

appHd.arduinoDoudou.command = function (hard, obj, callback) {
  Meteor.call('talkToArduino', hard._id, obj, function (err, result) {
    if(err) {
      console.log(err);
      appHd.logs({
        ip: hard.ip,
        hard_id: hard._id,
        log: 'appHd.arduinoDoudou.command : ' + err.code + ' : ' + JSON.stringify(obj),
        tags: ['error']
      });
    }
    else {
      if(Meteor.isClient) {
        console.log('http get SUCCES');
        if(result.statusCode === 200) {
          console.log('Status code = 200!');
          console.log(result.content);
          $('textarea').val(result.content);
        }
        if(typeof callback === "function") callback();
      }
    }
  });
};