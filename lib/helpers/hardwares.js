// NAMESPACE
if (typeof (appHd) === "undefined") appHd = {};

appHd.hardwaresTypes = [
  { text: 'Arduino homedoudou', value: 'arduinodoudou' },
  { text: 'Karotz', value: 'karotz' },
  { text: 'Nabaztag', value: 'nabaztag-pi' },
  { text: 'Homebridge', value: 'homebridge' },
  { text: 'Autres', value: 'other' },
];