if (typeof (appHd) === "undefined") appHd = {};
if (typeof (appHd.karotz) === "undefined") appHd.karotz = {};

appHd.karotz.command = function (karotz, command, callback) {
  Meteor.callAsync('karotz-command', karotz, command).then(result => {
    if (callback) callback(result);
  }).catch(err => {
    log(err)
    if (callback) callback(undefined, err);
  });
};