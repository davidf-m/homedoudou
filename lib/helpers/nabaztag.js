/**
 * Created by DavidFM on 04/06/2020.
 */

if(typeof (appHd) === "undefined") appHd = {};
if(typeof (appHd.nabaztag) === "undefined") appHd.nabaztag = {};

appHd.nabaztag.command = function (nabaztag, command, callback) {
  Meteor.call('nabaztag-command', nabaztag, command, function(err, result){
    if(err) {
      console.log(err);
      appHd.logsError({
        ip: nabaztag.ip,
        hard_id: nabaztag._id,
        log: 'appHd.nabaztag.command : ' + err.code + ' : ' + command,
      });
    }
    else {
      if(Meteor.isClient) {
        console.log(result);
        console.log('nabaztag command SUCCES');
        if(typeof callback === "function") callback();
      }
    }
  });
};