/**
 * Created by David FAIVRE-MAÇON
 * https://www.davidfm.me
 */

Router.configure({
  // we use the mainLayout template to define the layout for the entire app
  layoutTemplate: 'mainLayout',

  // the appNotFound template is used for unknown routes and missing lists
  notFoundTemplate: 'appNotFound',

  // show the appLoading template whilst the subscriptions below load their data
  loadingTemplate: 'appLoading',

  progressSpinner: false,
  progressDelay: 200,

  waitOn: function () {
    return [
      Meteor.subscribe('hardwareMine'),
      Meteor.subscribe('publicHardwares'),
      Meteor.subscribe('hardwareshared'),
      Meteor.subscribe('publicKeys'),
      Meteor.subscribe('usersPublic'),
      Meteor.subscribe('myRooms'),
      Meteor.subscribe('configsqui'),
      Meteor.subscribe('keys'),
      Meteor.subscribe('device')
    ];
  },
});

// Define these routes in a file loaded on both client and server
AccountsTemplates.configureRoute('signIn', {
  name: 'dfmSigninAtForm',
  template: 'dfmAuth',
  path: '/signin',
  redirect: function () {
    // console.log(Router.current().route.getName());
    // console.log("---");
    // console.log(dfm.whereTogoAfter("", true));
    // return window.location.href = dfm.whereTogoAfter();
    Router.go(dfm.whereTogoAfter("", true))
  },
});

AccountsTemplates.configureRoute('signUp', {
  name: 'dfmSignupAtForm',
  template: 'dfmSignupAtForm',
  path: '/join',
});

// Router.route('/join');
// Router.route('/signin');
Router.route('/contact', {
  onBeforeAction: function () {
    import '../ui/front/contact.js';
    this.next();
  }
});

// ----------- CONFIGURATION ACCUEIL
Router.route('/configuration', {
  name: 'configuration',
  layoutTemplate: 'configurationLayout',
  onBeforeAction: function () {
    if (appHd.isConnected()) {
      import '../ui/configurations/configuration.js';
      this.next();
    }
    else Router.go('/');
  }
});

Router.route('/configuration/rooms', {
  name: 'configurationRooms',
  layoutTemplate: 'configurationLayout',
  onBeforeAction: function () {
    if (appHd.isConnected()) {
      import '../ui/configurations/rooms/rooms.js';
      this.next();
    }
    else Router.go('/');
  }
});

Router.route('/configuration/rooms/:_id', {
  name: 'configurationRoomEdit',
  layoutTemplate: 'configurationLayout',
  onBeforeAction: function () {
    if (appHd.isConnected()) {
      import '../ui/configurations/rooms/edit/room-edit.js';
      this.next();
    }
    else Router.go('/');
  },
  data() {
    return Rooms.findOne(this.params._id);
  }
});

Router.route('/configuration/rooms/:roomId/button-edit/:buttonId', {
  name: 'configurationRoomButtonEdit',
  layoutTemplate: 'configurationLayout',
  onBeforeAction: function () {
    if (appHd.isConnected()) {
      import '../ui/configurations/rooms/buttons/button-edit.js';
      this.next();
    }
    else Router.go('/');
  },
  data() {
    return {
      room: Rooms.findOne(this.params.roomId),
      button: Buttons.findOne(this.params.buttonId)
    };
  }
});

Router.route('/configuration/hardware', {
  name: 'configurationHardware',
  layoutTemplate: 'configurationLayout',
  onBeforeAction: function () {
    if (appHd.isConnected()) {
      import '../ui/configurations/hardwares.js';
      this.next();
    }
    else Router.go('/');
  }
});

Router.route('/configuration/hardware/add', {
  name: 'configurationHardwareAdd',
  layoutTemplate: 'configurationLayout',
  onBeforeAction: function () {
    if (appHd.isConnected()) {
      import '../ui/configurations/hardware-add.js';
      this.next();
    }
    else Router.go('/');
  }
});

Router.route('/configuration/hardware/:hard_id', {
  name: 'configurationHardwareEdit',
  layoutTemplate: 'configurationLayout',
  data: function () {
    return Hardwares.findOne({ _id: this.params.hard_id });
  },
  onBeforeAction: function () {
    if (appHd.isConnected()) {
      import '../ui/configurations/hardware-edit.js';
      this.next();
    }
    else Router.go('/');
  }
});

Router.route('/configuration/hardware/devices/:hard_id', {
  name: 'configurationHardwareDevices',
  layoutTemplate: 'configurationLayout',
  data: function () {
    return { "hard_id": this.params.hard_id }
  },
  onBeforeAction: function () {
    if (appHd.isConnected()) {
      import '../ui/configurations/hardware-devices.js';
      this.next();
    }
    else Router.go('/');
  }
});

// LOGS HARDWARE

Router.route('/logs/hardware/:hard_id', {
  name: 'configurationHardwareLogs',
  layoutTemplate: 'configurationLayout',
  onBeforeAction: function () {
    if (appHd.isConnected()) {
      import '../ui/configurations/hardware-logs.js';
      this.next();
    }
    else Router.go('/');
  },
  waitOn: function () {
    return [
      Meteor.subscribe('configurationBootsNotArchived'),
      Meteor.subscribe('etats'),
      Meteor.subscribe('bugs')
    ];
  },
  data: function () {
    return { "hard_id": this.params.hard_id }
  },
});

Router.route('/logs/hardware/boot-server/:hard_id/:howLongHour', {
  name: 'configurationBootsServer',
  layoutTemplate: 'configurationLayout',
  onBeforeAction: function () {
    if (appHd.isConnected()) {
      import '../ui/configurations/boots-server.js';
      this.next();
    }
    else Router.go('/');
  },
  waitOn: function () {
    return [
      Meteor.subscribe('configurationBootsServer'),
      Meteor.subscribe('configurationBootsHard', this.params.hard_id)
    ];
  },
  data: function () {
    return Hardwares.findOne(this.params.hard_id);
  }
});

Router.route('/logs/hardware/boots/:hard_id', {
  name: 'configurationBootsArchived',
  layoutTemplate: 'configurationLayout',
  onBeforeAction: function () {
    if (appHd.isConnected()) {
      import '../ui/configurations/boots-archived.js';
      this.next();
    }
    else Router.go('/');
  },
  waitOn: function () {
    return [
      Meteor.subscribe('configurationBootsHard', this.params.hard_id)
    ];
  },
  data() {
    return Hardwares.findOne(this.params.hard_id);
  }
});

//

Router.route('/configuration/hardware/:hard_id/:name', {
  name: 'configurationHardwareKeyEdit',
  layoutTemplate: 'configurationLayout',
  onBeforeAction: function () {
    if (appHd.isConnected()) {
      import '../ui/configurations/hardware-key-edit.js';
      this.next();
    }
    else Router.go('/');
  },
  data: function () {
    return { "hard_id": this.params.hard_id, name: this.params.name }
  },
});

// FIXME deprecied
Router.route('/configuration/etats', {
  name: 'configurationEtats',
  layoutTemplate: 'configurationLayout',
  waitOn: function () {
    return [
      Meteor.subscribe('etats')
    ];
  },
  onBeforeAction: function () {
    if (appHd.isConnected()) this.next();
    else Router.go('/');
  }
});

Router.route('/configuration/bugs', {
  name: 'configurationBugs',
  layoutTemplate: 'configurationLayout',
  onBeforeAction: function () {
    if (appHd.isConnected()) {
      import '../ui/configurations/bugs.js';
      this.next();
    }
    else Router.go('/');
  },
  waitOn: function () {
    return [
      Meteor.subscribe('bugs')
    ];
  }
});

Router.route('/configuration/notifs', {
  name: 'configurationNotifs',
  layoutTemplate: 'configurationLayout',
  onBeforeAction: function () {
    if (appHd.isConnected()) {
      import '../ui/configurations/notifications/notifications.js';
      this.next();
    }
    else Router.go('/');
  },
});

Router.route('/configuration/scrapers', {
  name: 'configurationScrapers',
  layoutTemplate: 'configurationLayout',
  waitOn: function () {
    return [
      Meteor.subscribe('myScrapers')
    ];
  },
  onBeforeAction: function () {
    if (appHd.isConnected()) {
      import '../ui/configurations/scrapers/scrapers.js';
      this.next();
    }
    else Router.go('/');
  }
});

// Scénarios

Router.route('/configuration/scenarios/list', {
  name: 'configurationScenariosList',
  layoutTemplate: 'configurationLayout',
  onBeforeAction: function () {
    if (appHd.isConnected()) {
      import '../ui/configurations/scenarios/list/scenarios-list.js';
      this.next();
    }
    else Router.go('/');
  },
  waitOn: function () {
    return [
      Meteor.subscribe('hardwareMine'),
      Meteor.subscribe('keysMine')
    ];
  },
});

Router.route('/configuration/scenarios/upsert/:scenarioId?', {
  name: 'configurationScenarioUpsert',
  layoutTemplate: 'configurationLayout',
  onBeforeAction: function () {
    if (appHd.isConnected()) {
      import '../ui/configurations/scenarios/list/scenario-upsert.js';
      this.next();
    }
    else Router.go('/');
  },
  waitOn: function () {
    return [
      Meteor.subscribe('hardwareMine'),
      Meteor.subscribe('keysMine')
    ];
  },
  data() {
    return Scenarios.findOne(this.params.scenarioId);
  }
});

Router.route('/configuration/scenarios/events', {
  name: 'configurationScenariosEvents',
  layoutTemplate: 'configurationLayout',
  onBeforeAction: function () {
    if (appHd.isConnected()) {
      import '../ui/configurations/scenarios/scenarios-events.js';
      this.next();
    }
    else Router.go('/');
  },
  waitOn: function () {
    return [
      Meteor.subscribe('hardwareMine'),
      Meteor.subscribe('keysMine')
    ];
  },
});

Router.route('/configuration/scenarios/actions', {
  name: 'configurationScenariosActions',
  layoutTemplate: 'configurationLayout',
  onBeforeAction: function () {
    if (appHd.isConnected()) {
      import '../ui/configurations/scenarios/scenarios-actions.js';
      this.next();
    }
    else Router.go('/');
  },
  waitOn: function () {
    return [
      Meteor.subscribe('hardwareMine'),
      Meteor.subscribe('keysMine')
    ];
  },
});
//

Router.route('/configuration/ping/:hard_id', {
  name: 'arduinoPing',
  layoutTemplate: 'configurationLayout',
  waitOn: function () {
    return [
      Meteor.subscribe('adminHardwares'),
      Meteor.subscribe('adminKeys'),
    ];
  },
  onBeforeAction: function () {
    if (appHd.isConnected()) {
      import '../ui/configurations/ping.js';
      this.next();
    }
    else Router.go('/');
  },
  data: function () {
    return Hardwares.findOne(this.params.hard_id);
  }
});

// ----- Admin

Router.route('/webmaster/configs', {
  name: 'webmasterConfigs',
  layoutTemplate: 'adminLayout',
  onBeforeAction: function () {
    if (dfm.isWebmaster()) {
      import '../ui/admin/webmaster/configs.js';
      this.next();
    }
    else Router.go('/');
  },
});

Router.route('/webmaster/dbs', {
  name: 'webmasterDbs',
  layoutTemplate: 'adminLayout',
  onBeforeAction: function () {
    if (dfm.isWebmaster()) {
      import '../ui/admin/webmaster/dbs.js';
      this.next();
    }
    else Router.go('/');
  },
  data: function () {
    return this.params;
  }
});

Router.route('/webmaster/dbs/:keyId', {
  name: 'webmasterDbsKey',
  layoutTemplate: 'adminLayout',
  onBeforeAction: function () {
    if (dfm.isWebmaster()) {
      import '../ui/admin/webmaster/dbs.js';
      this.next();
    }
    else Router.go('/');
  },
  data: function () {
    return Keys.findOne(this.params.keyId);
  }
});

Router.route('/webmaster/fix', {
  name: 'webmasterFix',
  layoutTemplate: 'adminLayout',
  waitOn: function () {
    return [
      Meteor.subscribe('webmasterActionsFix'),
    ];
  },
  onBeforeAction: function () {
    if (dfm.isWebmaster()) {
      import '../ui/admin/webmaster/fix.js';
      this.next();
    }
    else Router.go('/');
  }
});

Router.route('/admin/hardware/:hard_id', {
  name: 'adminHardware',
  layoutTemplate: 'adminLayout',
  onBeforeAction: function () {
    if (dfm.isAdmin()) {
      import '../ui/admin/admin-hardware.js';
      this.next();
    }
    else Router.go('/');
  },
  waitOn: function () {
    return [
      Meteor.subscribe('adminHardwares'),
      Meteor.subscribe('adminKeys')
    ];
  },
  data: function () {
    return Hardwares.findOne(this.params.hard_id);
  }
});

Router.route('/admin/hardwares', {
  name: 'adminHardwares',
  layoutTemplate: 'adminLayout',
  onBeforeAction: function () {
    if (dfm.isAdmin()) {
      import '../ui/admin/admin-hardwares.js';
      this.next();
    }
    else Router.go('/');
  },
  waitOn: function () {
    return [
      Meteor.subscribe('adminHardwares'),
      Meteor.subscribe('adminKeys'),
      Meteor.subscribe('adminUsers')
    ];
  },
  data: function () {
    return this.params;
  }
});

Router.route('/admin/device', {
  name: 'adminDevice',
  layoutTemplate: 'adminLayout',
  onBeforeAction: function () {
    if (dfm.isAdmin()) {
      import '../ui/admin/admin-devices.js';
      this.next();
    }
    else Router.go('/');
  },
  data: function () {
    return this.params;
  }
});

Router.route('/admin/upload', {
  name: 'adminUpload',
  layoutTemplate: 'adminLayout',
  onBeforeAction: function () {
    if (!dfm.isAdmin()) return Router.go('/');
    else {
      import '../ui/admin/admin-upload.js';
      this.next();
    }
  }
});

Router.route('/admin/karotz', {
  name: 'adminKarotz',
  layoutTemplate: 'adminLayout',
  onBeforeAction: function () {
    if (dfm.isAdmin()) {
      import '../ui/admin/admin-karotz.js';
      this.next();
    }
    else Router.go('/');
  },
  data: function () {
    return this.params;
  }
});

Router.route('/admin/roles', {
  name: 'adminRoles',
  layoutTemplate: 'adminLayout',
  onBeforeAction: function () {
    if (dfm.isAdmin()) {
      import '../ui/admin/admin-roles.js';
      this.next();
    }
    else Router.go('/');
  },
  waitOn: function () {
    return [
      Meteor.subscribe('adminHardwares'),
      Meteor.subscribe('adminKeys'),
      Meteor.subscribe('adminUsers'),
      Meteor.subscribe('contact'),
      Meteor.subscribe('news')
    ];
  },
  data: function () {
    return this.params;
  }
});

Router.route('/admin/contact', {
  name: 'adminContact',
  layoutTemplate: 'adminLayout',
  onBeforeAction: function () {
    if (dfm.isAdmin()) {
      import '../ui/admin/admin-contact.js';
      this.next();
    }
    else Router.go('/');
  },
  waitOn: function () {
    return [
      Meteor.subscribe('contact'),
    ];
  },
  data: function () {
    return this.params;
  }
});

Router.route('/admin/news', {
  name: 'adminNews',
  layoutTemplate: 'adminLayout',
  onBeforeAction: function () {
    if (dfm.isAdmin()) {
      import '../ui/admin/admin-news.js';
      this.next();
    }
    else Router.go('/');
  },
  waitOn: function () {
    return [
      Meteor.subscribe('news')
    ];
  },
  data: function () {
    return this.params;
  }
});

Router.route('/admin/logs/:limit/:hard_id?', {
  name: 'adminLogs',
  layoutTemplate: 'adminLayout',
  onBeforeAction: function () {
    if (dfm.isAdmin()) {
      import '../ui/admin/admin-logs.js';
      this.next();
    }
    else Router.go('/');
  },
  data: function () {
    return this.params;
  }
});

Router.route('/admin/messages', {
  name: 'adminMessages',
  layoutTemplate: 'adminLayout',
  onBeforeAction: function () {
    if (dfm.isAdmin()) {
      import '../ui/admin/admin-messages.js';
      this.next();
    }
    else Router.go('/');
  },
  waitOn: function () {
    return [
      Meteor.subscribe('adminMessages'),
    ];
  }
});

Router.route('/admin/newtodo', {
  name: 'adminDaydTodo',
  layoutTemplate: 'adminLayout',
  onBeforeAction: function () {
    if (dfm.isAdmin()) {
      import '../ui/admin/admin-todo.html';
      this.next();
    }
    else Router.go('/');
  },
});

Router.route('/admin/organizer', {
  name: 'dfmOrganizer',
  layoutTemplate: 'adminLayout',
  onBeforeAction: function () {
    if (dfm.isAdmin()) {
      // import '../ui/admin/admin-todo.html';
      this.next();
    }
    else Router.go('/');
  },
});

Router.route('/admin/ddp', {
  name: 'adminDdp',
  layoutTemplate: 'adminLayout',
  onBeforeAction: function () {
    if (dfm.isAdmin()) {
      import '../ui/admin/ddp.js';
      this.next();
    }
    else Router.go('/');
  },
});

Router.route('/admin/debug/:start?', {
  name: 'adminDebug',
  layoutTemplate: 'adminLayout',
  onBeforeAction: function () {
    if (dfm.isAdmin()) {
      import '../ui/admin/admin-debug.js';
      this.next();
    }
    else Router.go('/');
  },
  waitOn: function () {
    const limit = parseInt(this.params.start) || 5;
    const filter = {};
    const options = { sort: { date: -1 }, limit: limit };

    let exceptHard = Session.get('hardExcept');
    exceptHard = _.map(exceptHard, function (o) {
      return o.hard_id;
    });

    if (exceptHard)
      filter.hard_id = { $nin: exceptHard };

    return [
      Meteor.subscribe('adminHardwares'),
      // Meteor.subscribe('donneesOptions', filter, options)
    ];
  },
  data: function () {
    return this.params;
  }
});

Router.route('/webmaster/claim', {
  name: 'webmasterClaim',
  action: function () {
    Meteor.callAsync('webmaster.claim')
      .then((res) => {
        Router.go('/')
      })
      .catch((err) => {
        console.log(err);
      });
  }
});

Router.route('/dashboard/', {
  name: 'dashboard',
  title: 'Tableau de bord',
  layoutTemplate: function () {
    return appHd.isUserSettingsActive('tabletMode_' + appHd.hardwareId) ? 'tabletLayout' : 'mainLayout';
  },
  onBeforeAction: function () {
    import '../ui/front/dashbord/dashboard.js';
    this.next();
  },
});

Router.route('/dashboard/:hard_id', {
  name: 'hardware',
  title: 'Matériel',
  onBeforeAction: function () {
    import '../ui/front/dashbord/hardware.js';
    this.next();
  },
  data: function () {
    return { "hard_id": this.params.hard_id };
  }
});

Router.route('/dashboard/:hard_id/:keyName/:howLongHour?', {
  name: 'dataKey',
  title: 'Données',
  onBeforeAction: function () {
    import '../ui/front/dashbord/data/data.js';
    this.next();
  },
  data: function () {
    return Keys.findOne({ hard_id: this.params.hard_id, name: this.params.keyName })
  }
});

Router.route('/data-debug/:hard_id/:keyName/:dateFrom?/:dateTo?', {
  name: 'dataKeyDebug',
  title: 'Données Debug',
  onBeforeAction: function () {
    import '../ui/front/dashbord/data/data-debug.js';
    this.next();
  },
  data: function () {
    return Keys.findOne({ hard_id: this.params.hard_id, name: this.params.keyName })
  }
});

Router.route('/cumul-keys/:cumulsKeysId/:howLongHour?', {
  name: 'cumulativeKeys',
  title: 'Données',
  waitOn: function () {
    const params = this.params;
    const cumulKeys = CumulKeys.findOne(this.params.cumulsKeysId);
    console.log(cumulKeys);
    if (!cumulKeys) return;
    const subs = []
    cumulKeys.lines.forEach(function (line, i) {
      const keyOne = Keys.findOne(line.key_id);
      if (keyOne)
        subs.push(Meteor.subscribe('donnees', line.hard_id, keyOne.name, params.howLongHour || "24"))
    });
    return subs;
  },
  onBeforeAction: function () {
    import '../ui/front/dashbord/data/cumul-keys.js';
    this.next();
  },
  data() {
    return this.params;
  }
});

Router.route('/actions/:hard_id?', {
  name: 'actions',
  data: function () {
    return { "hard_id": this.params.hard_id };
  },
  waitOn: function () {
    return Meteor.subscribe('actionsHardware');
  }
});

Router.route('/myaccount', {
  onBeforeAction: function () {
    if (!Meteor.user()) {
      Router.go('dfmSigninAtForm');
    }
    else {
      import '../../client/templates/account/myaccount.js';
      this.next();
    }
  },
  template: 'myAccount'
});

Router.route('/', {
  name: 'home',
  onBeforeAction: function () {
    if (appHd.isUserSettingsActive('hideHome'))
      Router.go('dashboard');
    else {
      import '../ui/front/home.js';
      this.next();
    }
  },
  waitOn: function () {
    return [
      Meteor.subscribe('news'),
    ]
  }
});