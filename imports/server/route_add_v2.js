import { HTTP } from "meteor/http";
import { IncomingMessage } from "./class/incomingMessage.js";
import { ArchiveHTTP } from "./class/archive_http.js";

// Controlleur de la route "/addV2?id=---&mode=---&cle=---&valeur=---&index=---" ATTENTION HTTP !!!

Router.route('/message', {
  where: 'server'
}).get(function () {

  // use of this.request.query cause this.params.query bad interpreted in this case valeur: '20;2B;Maclean;ID=f41f;SWITCH=01;CMD=OFF;'
  const req = this.request;

  const im = new IncomingMessage({
    hardId: req.query.id,
    mode: req.query.mode,
    keyName: req.query.cle,
    value: req.query.valeur,
    index: req.query.index,
    context: this,
    log: ['ddp', req.query.mode]
  })

  // ---------- DEBUT LOGS -------

  const ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress || req.socket.remoteAddress || req.connection.socket.remoteAddress;
  const fullUrl = req.connection.parser.incoming.originalUrl;

  let log = {
    ip: ip,
    hard_id: im.hardId,
    logs: [fullUrl],
    tags: []
  };

  if (im.mode) log.tags.push(im.mode.toLowerCase());

  if (!im.hardId || !im.keyName || !im.value || !im.mode) {
    log.tags.push("error");
    log.comment = "format url non pris en charge, id=?&cle=?&valeur=?&mode";
    appHd.logs(log);
    return appHd.respNok(this, "format url non pris en charge, id=?&cle=?&valeur=?&mode");
  }

  // --------- FIN LOGS -------

  const donnees = {
    hard_id: im.hardId,
    cle: im.keyName,
    valeur: im.value,
    date: Date.now(),
    ip: ip
  };

  // Setup hardware and save last data for dashboard
  if (im.hardId === '01234567890123456')
    return appHd.respOk(this, "Hardware non enregistre !");

  const HardOne = Hardwares.findOne(im.hardId);

  if (HardOne) {
    // if(ip !== "127.0.0.1")
    Hardwares.update(im.hardId, { $set: { ip: ip, lastSeen: new Date() } });
  }
  else
    return appHd.respNok(this, "unknown_hard");

  // les differents modes d'add
  if (im.mode === "INIT") {                                                                              /****************** INIT *********************/
    return appHd.respOk(this, "Mode init... je comprends pas..."); //FIXME Orphelin avec un id en demande d'init?
  }
  else if (im.mode === "EEPROM") {
    if (pCle.toUpperCase() === "DEBUT") {
      Eeprom.remove({ hard_id: pHard_id });
      return appHd.respOk(this, "ok EEPROM DEBUT");
    }
    else if (pCle.toUpperCase() === "FIN") {
      const etats = EepromTemp.find({ hard_id: pHard_id }).fetch();
      Eeprom.insert({ hard_id: pHard_id, etat: etats, date: Date.now() });
      EepromTemp.remove({ hard_id: pHard_id });
      return appHd.respOk(this, "ok EEPROM FIN");
    }
    else if (pCle.toUpperCase() === "COMBIEN_ADRESSE_PINGER") {
      const pingsCount = HardOne.devices.filter(function (d) {
        return d.type_id === 185
      }).length;
      const command = "http://" + HardOne.ip + ":" + HardOne.port + "/etat?cle=nb_ip_pinger&valeur=" + pingsCount + "&mode=EEPROM"
      HTTP.get(command, function (error, result) {
        if (error) {
          console.log('http get FAILED! router.js 590');
        }
        else {
          //console.log(command + ' http get SUCCES');
          // if(result.statusCode === 200) {
          //   console.log('Status code = 200');
          // }
        }
      });
      return appHd.respOk(this, "ok EEPROM FIN");
    }
    else if (pCle.toUpperCase() === "DEMANDEPINGADRESS") {
      const pingAdressArray = HardOne.devices.filter(function (d) {
        return d.type_id === 185
      });
      const ip = pingAdressArray[pValeur - 1].ip.replace(/\./g, ',');
      HTTP.get("http://" + HardOne.ip + ":" + HardOne.port + "/etat?cle=PINGadress" + pValeur + "&valeur=" + ip + "&mode=EEPROM", function (error, result) {
        if (error) {
          console.log('http get FAILED! router.js 608');
        }
        else {
          // console.log('http get SUCCES');
          if (result.statusCode === 200) {
            // console.log('Status code = 200!');
            // console.log(result.content);
            // for(let i = 0; i < pings.length; i++) {
            //   let ping = pings[i];
            //   HTTP.get("http://" + HardOne.ip + ":" + HardOne.port + "/etat?cle=nb_ip_pinger&valeur=" + nbPing + "&mode=EEPROM", function (error, result) {
            //   });
            // }
          }
        }
      });
      return appHd.respOk(this, "ok EEPROM FIN");
    }
    else {
      if (!EepromTemp.insert(donnees)) {
        return appHd.respNok(this, "nok! EEPROM save");
      }
      else {
        return appHd.respOk(this, "ok EEPROM save " + pHard_id + ' / ' + pCle);
      }
    }
  }
  else if (im.mode === "BOOT") {
    /****************** BOOT *********************/

    if (pCle.toUpperCase() === "DEMANDE") {
      if (pValeur.toUpperCase() === "OKKO") {
        //TODO verifier la conf
        HTTP.get("http://" + HardOne.ip + ":" + HardOne.port + "/etat?cle=CONF_OK&valeur=yo&mode=EEPROM", function (error, result) {
          if (error) {
            console.log('http get FAILED! router add.js 131 ouverture de port?');
            return appHd.respNok(this, "Aucune commucation, ouverture du port ok?");
          }
          else {
            // console.log('http get SUCCES');
            if (result.statusCode === 200) {
              // console.log('Status code = 200!');
              // console.log(result.content);
            }
          }
        });

        return appHd.respOk(this, "ok demande okko entendue");
      }
    }
    else if (pCle.toUpperCase() === "DEBUT") {

      Hardwares.update(pHard_id, { $set: { lastBoot: new Date() } });

      // efface les anciens bugs de l'arduino qui boot
      Bugs.remove({ hard_id: pHard_id });

      Boots.update({ hard_id: pHard_id }, { $set: { archived: true } }, { multi: true });

      Boots.insert({ hard_id: pHard_id, boot: [], date: Date.now() });

      // si config free envoyer un sms "arduino boot"
      const u = Meteor.users.findOne(HardOne.owner_id);
      if (u?.settings?.notifHardBoot) appHd.sendNotifs(u._id, appHd.aliasname(HardOne) + " boot");

      return appHd.respOk(this, "ok BOOT DEBUT");
    }
    else {
      if (pCle.toLowerCase() === "date_et_version_arduino" || pCle.toLowerCase() === "version") Hardwares.update(pHard_id, { $set: { version: pValeur } });
      if (pCle.toLowerCase() === "nom_arduino") Hardwares.update(pHard_id, { $set: { name: pValeur } });

      if (Boots.update({ hard_id: pHard_id, "archived": { $ne: true } }, { $push: { boot: donnees } })) return appHd.respOk(this, "ok BOOT save " + pHard_id + '/' + pCle);
      else return appHd.respNok(this, "nok! BOOT save");
    }
  }
  else if (im.mode === "LIVE") {
    /****************** LIVE *********************/
    Live.insert(donnees);
    return appHd.respOk(this, "ok LIVE save");
  }
  else if (im.mode === "BUG") {
    /****************** BUG *********************/
    Bugs.insert(donnees);
    return appHd.respOk(this, "ok BUG save");
  }
  else if (im.mode === "ETAT") {
    /****************** ETAT *********************/
    if (pCle.toUpperCase() === "DEBUT") {
      Etats.remove({ hard_id: pHard_id });
      return appHd.respOk(this, "ok ETAT DEBUT");
    }
    else if (pCle.toUpperCase() === "FIN") {
      const etats = EtatTemp.find({ hard_id: pHard_id }).fetch();
      Etats.insert({ hard_id: pHard_id, state: etats, date: Date.now() });
      EtatTemp.remove({ hard_id: pHard_id });
      return appHd.respOk(this, "ok ETAT FIN");
    }
    else {
      EtatTemp.insert(donnees);
      return appHd.respOk(this, "ok ETAT save " + pHard_id + '/' + pCle);
    }
  }
  else if (im.mode === "433") {
    /****************** 433 *********************/
    // old
    // /add?id=aaaa&mode=433&cle=BROCHE_0_RXlearn433mhz&valeur=1111111000110101110000000001001000010001111011100011
    //
    // /add?id=aaaa&mode=433&cle=RX_RFLINK&valeur=20;3F;Blyss;ID=5c01;SWITCH=D0;CMD=OFF;
    // mode 433Learn
    // la cle tournante renvoie toujours une value differente, tester la longueur de la cle 52digit (13x4) les 16 derniers inutiles.
    // il faut gerer les diffeents protocole blyss etc... sur une page lear 433
    if (pValeur !== "Emission_OK") {
      Mhz433.update({ hard_id: pHard_id, valeur: pValeur }, {
        hard_id: pHard_id,
        key: pCle,
        value: pValeur
      }, { upsert: true });
    }
    return appHd.respOk(this, "ok valeur recue");
  }
  else if (im.mode === "ARCHIVE") {
    /****************** ARCHIVE *********************/
    return new ArchiveHTTP(im).toInsert();
  }
  else if (im.mode === "SMS") {
    /****************** SMS *********************/
    const hard = Hardwares.findOne(pHard_id);
    if (hard) {
      const user = Meteor.users.findOne(hard.owner_id);
      if (user) {
        const uf = appHd.isFreeConfig(user);
        if (uf) {
          const message = appHd.aliasname(HardOne) + " demande : \r\n Cles : " + pCle + "\r\n Valeur : " + pValeur;
          const that = this;
          Meteor.call('sendFreeSMS', uf.login, uf.key, message, function (err) {
            if (err) appHd.respNok(that, "err sending sms");
            else return appHd.respOk(that, "ok SMS send");
          });
        }
        else appHd.respNok(this, "err sending sms, user " + user._id + " has no free config");
      }
      else appHd.respNok(this, "err sending sms, user not found");
    }
    else return appHd.respNok(this, "unknown_hard");
  }
  else {
    log.log = "format url non pris en charge, mode ok?";
    log.tags = ['error'];
    appHd.logs(log);
    return appHd.respNok(this, "format url non pris en charge, mode ok?");
  }
});