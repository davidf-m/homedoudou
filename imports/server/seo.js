/**
* Created by David FAIVRE-MAÇON
* https://www.davidfm.me
*/

import prerenderIO from 'prerender-node';
import { Meteor } from 'meteor/meteor';
import { WebApp } from 'meteor/webapp';

const sp = Meteor.settings.dfm?.prerenderIO;

if (sp) {
    Meteor.startup(() => {
        prerenderIO.set('prerenderServiceUrl', sp.serviceUrl);
        prerenderIO.set('prerenderToken', sp.token);
        prerenderIO.set('protocol', sp.protocol);
        prerenderIO.set('host', sp.host);
        prerenderIO.crawlerUserAgents.push('googlebot');
        prerenderIO.crawlerUserAgents.push('bingbot');
        prerenderIO.crawlerUserAgents.push('yandex');
        WebApp.rawConnectHandlers.use(prerenderIO);
    });
}
else
    dfm.logError('no prerender settings');