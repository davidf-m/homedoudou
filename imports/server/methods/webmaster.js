Meteor.methods({

  async 'webmaster.claim'() {
    if (!this.userId || await Roles.getUsersInRoleAsync('webmaster').fetch().length) return;
    Roles.createRoleAsync("webmaster", { unlessExists: true });
    Roles.addUsersToRolesAsync(this.userId, 'webmaster');
  },
});
