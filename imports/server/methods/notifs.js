const webpush = require('web-push');

const firebaseAppKey = Meteor.settings?.notifications?.push?.firebaseAppKey;
const mailto = Meteor.settings?.notifications?.push?.webpush?.mailto;
const publicKey = Meteor.settings?.notifications?.push?.webpush?.publicKey;
const privateKey = Meteor.settings?.notifications?.push?.webpush?.privateKey;

if (firebaseAppKey && mailto && publicKey && privateKey) {
  webpush.setGCMAPIKey(firebaseAppKey);
  webpush.setVapidDetails(
    mailto,
    publicKey,
    privateKey,
  );
}

const pushOptions = {
  gcmAPIKey: firebaseAppKey,
  TTL: 24 * 60 * 60, // 24h
};

Meteor.methods({

  sendNotificationWeb: function (user_id, message) {
    const u = Meteor.users.findOne(user_id);
    if (!u) return;

    const uf = appHd.isFreeConfig(u);
    if (uf) Meteor.call('sendFreeSMS', uf.login, uf.key, message);
    // else if(u.notifs && u.notifs.firebase && u.notifs.firebase.enabled) {
    //   Meteor.call('firebase-sendMessage', user.notifs.firebase.token, message)
    // }
    else {
      const dest = NotificationsSubs.findOne({ "user._id": u._id });
      if (dest) {
        const payload = { "title": "homedoudou", "body": message, "eventId": "notif" };
        Meteor.call('notificationsPush', dest, payload)
      }
    }
  },

  sendNotificationByMailOnce: function (scenario, options) {
    const u = Meteor.users.findOne(scenario.createdBy);
    if (!u) return;

    let msg = "Action NOTIF MAIL ONCE";
    if (scenario.msg) {
      msg = scenario.msg;
      if (options && options.msg) msg += options.msg;
    }
    const email = dfm.getUserEmail(u);
    if (!scenario.notifiedByMailOnce) {
      Scenarios.update(scenario._id, { $set: { "notifiedByMailOnce": true } });
      dfm.sendMail({ to: [email], from: "", subject: "notif", htmlTemplate: msg });
    }
  },

  sendFreeSMS: function (login, key, message) {
    if (!login || !key) return;
    if (!message) message = 'Hello World !';

    this.unblock();
    try {
      return HTTP.call('GET', 'https://smsapi.free-mobile.fr/sendmsg', {
        params: { user: login, pass: key, msg: message }
      });
    } catch (e) {
      // Got a network error, timeout, or HTTP error in the 400 or 500 range.
      return e;
    }
  },

  async 'notificationsSubsUpdate'(sub) {
    sub = JSON.parse(sub);
    const u = Meteor.user();
    if (u) sub.user = {
      _id: u._id,
      username: u.username
    };
    sub.createdAt = new Date();

    // will insert if does not exists
    return NotificationsSubs.updateAsync({ endpoint: sub.endpoint }, { $set: sub }, { upsert: true });
  },

  // for returned statusCodes
  // https://developers.google.com/web/fundamentals/push-notifications/common-issues-and-reporting-bugs#http_status_codes
  notificationsPush(dest, payload) {
    const self = this;
    if (_.isEmpty(firebaseAppKey)) return appHd.logsError('No Meteor.settings.notifications.push.firebaseAppKey');
    // https://github.com/web-push-libs/web-push/
    webpush.sendNotification(
      _.pick(dest, 'endpoint', 'keys'),
      JSON.stringify(payload),
      pushOptions,
    ).then(Meteor.bindEnvironment(function (rv) {
      if (self && self.connection) console.log('Success ' + rv.statusCode);
      NotificationsSubs.update({ endpoint: dest.endpoint }, { $set: { lastMessage: { date: new Date(), payload: payload, status: rv.statusCode } } });
    })).catch(Meteor.bindEnvironment(function (rv) {
      // removal on any error
      const remove = Meteor.settings?.notifications?.push?.removeOnError;
      // we force the removal if statusCode is 410 ("Gone")
      if ((rv.statusCode === 410 || remove) && dest.endpoint) NotificationsSubs.remove({ endpoint: dest.endpoint });
      else if (dest.endpoint) NotificationsSubs.update({ endpoint: dest.endpoint }, { $set: { lastMessage: { date: new Date(), payload: 'Error', status: rv.statusCode } } });
      appHd.logsError(rv.message + ' ' + rv.statusCode);
    }));
  },

  hmdNotificationsSubsRemove(userId) {
    return NotificationsSubs.remove({ "user._id": userId });
  },
});