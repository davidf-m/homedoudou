import { Archive } from "./archive";

class ArchiveHTTP extends Archive {
  constructor(data) {
    const HardOne = Hardwares.findOne(data.hardId);
    const KeyOne = Keys.findOne({ hard_id: HardOne._id, name: data.keyName });
    const url = data.context.request.connection.parser.incoming.originalUrl
    super({
      hard: HardOne,
      key: KeyOne,
      log: data.log,
      // log: {logs: ['url : ' + url]},
      ...data,
    });
    this.context = data.context;
  }

  toInsert() {
    if (this.keyName === "BROCHE_14_RFlink_OK_") {
      const split = this.value.split(';');
      const keyName = split[1] + '_' + split[2] + '_' + split[3];
      return new ArchiveHTTP({
        hard_id: this.hard._id,
        keyName: keyName,
        value: split[4],
        index: this.index,
        context: this.context,
        isSplit: true
      }).toInsert();
    }
    else if (this.isRFLink()) {
      // Si la cle est de type RFLINK, je la decoupe
      const Split = this.splitRFLink();
      return new ArchiveHTTP({
        hard_id: this.hard._id,
        keyName: Split.keyName,
        value: Split.value,
        index: this.index,
        context: this.context,
        isSplit: true
      }).toInsert();
    }
    else super.toInsert()
  }

  sendOk(text) {
    appHd.respOk(this.context, text);
  }

  sendNok(text) {
    appHd.respNok(this.context, text);
  }
}

export { ArchiveHTTP };