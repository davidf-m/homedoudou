class IncomingMessage {
  constructor({ hardId, mode, keyName, value, context, log } = {}) {
    this.hardId = hardId;
    this.mode = mode ? mode.toUpperCase() : null;
    this.keyName = keyName;
    this.value = value;
    this.context = context;
    this.log = log || [];
  }
}

export { IncomingMessage };