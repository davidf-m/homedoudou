import {Archive} from "./archive";

class ArchiveDDP extends Archive {
  constructor(data) {
    const HardOne = Hardwares.findOne({ddpId: data.session_id});
    const KeyOne = Keys.findOne({hard_id: HardOne._id, name: data.key});
    super({
      hard: HardOne,
      key: KeyOne,
      value: data.value,
      index: data.index
    });
    this.session_id = data.session_id
  }

  sendOk(text) {
    appHd.ddpSendOk(this.session_id, text);
  }

  sendNok(text) {
    appHd.ddpSendError(this.session_id, text);
  }

  toInsert() {
    if(this.keyName === "BROCHE_14_RFlink_OK_") {
      // "BROCHE_14_RFlink_OK_"    retour de commande 433
      // {"m":"archive","k":"BROCHE_14_RFlink_OK_","v":"10;Maclean;f41f;01;ON","i":"0"}
      // FIXME changer le nom de la cle dans l'arduino en "RFLinkBack"
      const split = this.value.split(';');
      const keyName = split[1] + '_' + split[2] + '_' + split[3];

      return new ArchiveHTTP({
        session_id: this.session_id,
        hard_id: this.hard._id,
        keyName: keyName,
        value: split[4],
        isSplit: true
      }).toInsert();
    }
    else if(this.isRFLink()) {
      // Si la cle est de type RFLINK, je la decoupe
      const Split = this.splitRFLink();
      return new ArchiveDDP({
        session_id: this.session_id,
        hard_id: this.hard._id,
        keyName: Split.keyName,
        value: Split.value,
        isSplit: true
      }).toInsert();
    }
    else super.toInsert()
  }
}

export {ArchiveDDP};