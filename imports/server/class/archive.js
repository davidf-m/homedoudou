/**
 * Created by David FAIVRE-MAÇON on 11 june 2021
 * https://www.davidfm.me
 */

class Archive {
  constructor({ hard, key, keyName, value, index, log }) {
    this.hard = hard;
    this.key = key;
    this.keyName = keyName;
    this.value = value;
    this.index = index;
    this.date = Date.now();
    this.broche_id = null;
    this.owner = this.hard?.owner_id ? Meteor.users.findOne(this.hard.owner_id) : null;
    this.log = {
      ...log, ...{
        hard_id: this.hard._id,
        logs: log?.logs || [],
        tags: log?.tags || []
      }
    };
  }

  isRFLink() {
    // /add?id=---&mode=archive&cle=BROCHE_14&valeur=20;91;Digitech;ID=0131;TEMP=00a4;BAT=OK;&index=1
    // http://www.rflink.nl/blog2/protref
    // 20;2D;UPM/Esic;ID=0001;TEMP=00cf;HUM=16;BAT=OK;
    // 20;02;Name;ID=9999;LABEL=data;

    // je regarde si cette clé existe
    if (this.key?.device?.type_id === 86) {
      if (!this.value || typeof this.value !== 'string') {
        this.sendNok("obj.value !== 'string'");
        throw new Meteor.Error("error", "obj.value !== 'string'");
      }
      return true;
    }
    return false;
  }

  splitRFLink() {
    const splited = this.value.split(';');
    // si le split n'est pas bon...
    if (splited.length < 2) {
      // if(this.index && HardOne.devices && HardOne.devices[obj.index]) {
      //   const device = HardOne.devices[obj.index];
      //   //console.log(device);
      //   device.index = obj.index;
      //   Keys.update(KeyOne._id, {$set: {device: device}});
      // }
      throw new Meteor.Error("error", "splited.length < 2");
    }

    //FIXME ci dessous n'est pas vrai
    // tous les messages RFLINK sont cencé être formaté de la même manière, splited[3] étant l'id, [ '20', '91', 'Digitech', 'ID=1234', 'TEMP=00a6', 'BAT=OK', '' ]

    let text = "";
    for (let i = 0; i < splited.length; i++) {
      const item = splited[i];
      if (!item) continue;
      const name = item.split('=')[0];
      let value = item.split('=')[1];
      // console.log(name, value);i
      if (name === 'ID') {
        this.keyName += "_" + value;
      }
      else if (value) {
        const keyName = this.keyName + "_" + name;
        text += name + "=" + value + " ";
        // FIXME replace by !== SWITCH ?
        if (name === "TEMP" || name === "HUM" || name === "BAT") {
          if (name === "TEMP") value = parseInt(value, 16) / 10;
          let KeyOneTemp = Keys.findOne({ hard_id: this.hard._id, name: keyName });
          if (!KeyOneTemp) KeyOneTemp = Keys.insert({ hard_id: this.hard._id, name: keyName, owner_id: this.hard._id.owner_id });
        }

        // minTime default
        const keySplited = Keys.findOne({ hard_id: this.hard._id._id, name: keyName, minTime: { $exists: false } });
        if (keySplited) Keys.update(keySplited._id, { $set: { minTime: 570000 } });

        return {
          keyName: keyName,
          value: value
        }
      }
    }
    return sendOk({
      connexionType: obj.connexionType,
      hard_id: hard_id,
      that: obj.that,
      session_id: session_id,
      text: obj.key + ' -- ' + obj.value,
      log: { enabled: true, comment: "RFLINK splited" }
    });
  }

  toInsert() {
    // console.log(this.key._id)
    // console.log(this.hard._id)
    // console.log(this.key.name)
    // console.log(this.value)
    // console.log(this.date)
    this.updateKey();
    this.notification();
    this.scenario();
    this.dataInsert();
    this.logInsert();
  }

  updateKey() {

    // valeurs minimum et maximum si numérique
    let min = null;
    let max = null;
    const pvn = parseFloat(this.value);
    if (!isNaN(pvn)) {
      min = pvn;
      max = pvn;
    }

    const keyNameSplited = this.keyName.split("_");
    if (keyNameSplited[0].toUpperCase() === "BROCHE") this.broche_id = keyNameSplited[1];

    if (this.key) {
      // FIXME lastDate et lastData.date redondant
      // Dernières donnée
      let set = { lastDate: new Date, lastData: { date: new Date, value: this.value }, beforeLast: this.key.lastData };

      // si la valeur est numérique
      if (!isNaN(pvn)) {
        if (pvn < this.key.min) set.min = pvn;
        if (pvn > this.key.max) set.max = pvn;
      }

      if (this.index && this.hard.devices && this.hard.devices[this.index]) {
        const device = this.hard.devices[this.index];
        set.device = {
          index: this.index,
          broche: device.broche,
          type_id: device.type_id,
          name: device.name
        };
      }

      // si la cle commence par BROCHE et a un name ex: BROCHE_17_MOTDETAT
      if (keyNameSplited[0].toUpperCase() === "BROCHE" && keyNameSplited[2]) set.keyName = keyNameSplited[2];

      // update Keys
      Keys.update(this.key._id, { $set: set });
    }
    else {
      const orderKey = Keys.findOne({ hard_id: this.hard._id }, { sort: { order: 1 } })?.order || 1; // FIXME pas count mais le plus grand order
      const newKey = {
        hard_id: this.hard._id,
        broche_id: this.broche_id,
        name: this.keyName,
        style: "stock",
        order: orderKey,
        min: min,
        max: max,
        owner_id: this.hard.owner_id,
        lastData: { date: new Date, value: this.value },
      };
      if (this.keyName === "sequence_") newKey.hidden = true;
      const key_id = Keys.insert(newKey);
      this.key = Keys.findOne(key_id);
      DataByKey[key_id] = new Mongo.Collection('DataByKey_' + key_id);

      appHd.logs({
        // ip: ip,
        hard_id: this.hard._id,
        logs: ["new key : " + this.keyName],
        tags: ['ddp', 'key', 'newKey'],
      });
    }
  }

  notification() {
    if (this.key.notified && this.owner) {
      appHd.sendNotifs(this.owner._id, appHd.aliasname(this.key) + " : " + this.value);
      this.log.tags.push("notification")
      this.log.logs.push("notification sent")
    }
  }

  scenario() {
    // scenarios

    /*
      je regarde si un event avec cette cleId existe et si un scenario libre utilisant cet event existe
      alors je fais l'action correspondate

      ensuite

      je regarde si un scenario chauffage existe avec cette cleId
      alors je fais la logique de chauffage
   */

    // quand une cle en mode archive envoie une valeur, je dois verifier si un scenario libre est lié a cette cle et si l'evenement est déclencheur.
    const scenarios = appHd.scenario.isTriggering({
      hard: this.hard,
      key: this.key,
      value: this.value
    });
    // console.log(scenarios);
    if (scenarios && scenarios.length) {
      this.log.tags.push('triggered', 'scenario');
      this.log.logs.push({ log: JSON.stringify(scenarios) });

      for (let i = 0; i < scenarios.length; i++) {
        appHd.scenario.launchActionsOfScenariosOutput(scenarios[i]._id);
      }
      this.log.enabled = true;
    }

    // scenario chauffage
    const scenariosHeater = Scenarios.find({ scenarioType: 'scenario', sensorId: this.key._id, disabled: { $ne: true } }).fetch();
    if (scenariosHeater.length) {
      scenariosHeater.forEach(function (scenario) {
        // y a t-il une plage de fonctionnement horaire pour une temperature précise
        let customRangeTriggered = false;
        // Temperature sur plage horaire
        if (scenario.rangeDateUTC?.length) {
          const time = new Date().toLocaleString('fr-FR', { hour: "numeric", minute: "numeric", "hour12": false });
          scenario.rangeDateUTC.forEach((customRange) => {
            if ((customRange.toDate > customRange.fromDate && time > customRange.fromDate && time < customRange.toDate) || (customRange.fromDate > customRange.toDate && (time > customRange.fromDate || time < customRange.toDate))) {
              customRangeTriggered = true;
              if (parseFloat(this.value) < (customRange.temperature - scenario.delta)) appHd.scenario.actionLaunch(scenario.heaterOn);
              else if (parseFloat(this.value) > (customRange.temperature + scenario.delta)) appHd.scenario.actionLaunch(scenario.heaterOff);
            }
          });
        }
        // Temperature par defaut
        if (!customRangeTriggered) {
          if (parseFloat(this.value) < (scenario.temp - scenario.delta)) appHd.scenario.actionLaunch(scenario.heaterOn);
          else if (parseFloat(this.value) > (scenario.temp + scenario.delta)) appHd.scenario.actionLaunch(scenario.heaterOff);
        }

        /*
        // To remove : ancien fonctionement de plage horaires, aucune personalisation de temperature
        // y a t-il une plage de fonctionnement horaire
        if(scenario.rangeDateUTC?.length) {
          const time = new Date().toLocaleString('fr-FR', {hour: "numeric", minute: "numeric", "hour12": false});
          scenario.rangeDateUTC.forEach((dates) => {
            if((dates.toDate > dates.fromDate && time > dates.fromDate && time < dates.toDate) || (dates.fromDate > dates.toDate && (time > dates.fromDate || time < dates.toDate)))
              if(parseFloat(data.valeur) < (scenario.temp - scenario.delta)) appHd.scenario.actionLaunch(scenario.heaterOn);
          });
        }
        else if(parseFloat(data.valeur) < (scenario.temp - scenario.delta)) appHd.scenario.actionLaunch(scenario.heaterOn);
        if(parseFloat(data.valeur) > (scenario.temp + scenario.delta)) appHd.scenario.actionLaunch(scenario.heaterOff);
         */
      });
      this.log.logs.push({ log: JSON.stringify(scenariosHeater) });
      this.log.tags.push('heater');
      this.log.enabled = true;
    }

    // sequence_ FIN je remets le 433 en mode ecoute si besoin
    // FIXME TODO
    // if(this.keyName === "sequence_" && this.value && this.value.toLowerCase() === "fin") {
    //   Hardwares.update(this.hard._id, {$set: {listenSince: new Date()}});
    //
    //   // Mode ecoute 433
    //   if(this.connexionType === 'ddp' && this.hard.listening433) {
    //     const i = appHd.arrayObjectIndexOf(this.hard.devices, 86, 'type_id');
    //     if(i > -1) {
    //       const broche = this.hard.devices[i].broche;
    //       const time = 10 * 60 * 1000; // 10 minutes
    //       // console.log('Mode ecoute 433', broche, time);
    //       appHd.ddpSend(session_id, {mode: "433R", cle: broche, valeur: time});
    //       logs.logs.push({log: "Ecoute 433 ON"});
    //     }
    //   }
    // }
    console.log(this.log);
  }

  dataInsert() {
    if (this.keyName.toUpperCase() === "JSON") {
      // const json = JSON.parse(decodeURI(obj.value));
      // console.log(json);
      return this.sendOk("ok json test");
    }
    else if (this.keyName === "directionVENT") {
      // Si l'ajout est de type "directionVENT" ne pas ajouter la valeur si la dernière est la même.
      const lastData = DataByKey[this.key._id].findOne({}, { sort: { 'date': -1 }, limit: 1 });
      if (lastData && lastData === this.value) return this.sendNok("ok ARCHIVE directionVENT but not save cause same value");
    }
    else {
      // Save data
      // FIXME si la dernière donnée est >= à 10 minutes, seulement les t° et etc, pas les actions...

      if (this.key.minTime) {
        const lastData = DataByKey[this.key._id].findOne({}, { sort: { date: -1 } });
        if (lastData && (new Date() - lastData.date) < this.key.minTime) {
          return sendOk("ok ARCHIVE not saved < " + this.key.minTime + " sec");
        }
      }

      if (this.key.volatile) DataByKey[this.key._id].remove({});
    }

    DataByKey[this.key._id].insert({
      cleId: this.key._id,
      hard_id: this.hard._id,
      cle: this.key.name,
      valeur: this.value,
      date: this.date
    });
    this.log.tags.push("dataInsert");
    this.log.logs.push(JSON.stringify({
      keyId: this.key._id,
      hard_id: this.hard._id,
      cle: this.key.name,
      valeur: this.value,
      date: this.date
    }));
    this.log.logs.push("data saved");

    this.sendOk("ok ARCHIVE save");
  }

  logInsert() {
    appHd.logs(this.log);
  }

  sendOk(text) {
    console.log(text);
  }

  sendNok(text) {
    console.log("error", text);
  }
}

export { Archive };