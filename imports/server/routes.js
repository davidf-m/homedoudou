import { HTTP } from "meteor/http";

Router.route('/claim-admin', {
  where: 'server',
}).get(function () {
  if (!this.userId || Roles.getUsersInRole('webmaster').fetch().length) return;
  Roles.createRole("webmaster", { unlessExists: true });
  Roles.addUsersToRoles(this.userId, 'webmaster');
});

// Controlleur de la route "/add?id=---&mode=---&cle=---&valeur=---&index=---" ATTENTION HTTP !!!

// RECEPTION EXTERNE
Router.route('/add', {
  where: 'server',
}).get(function () {


  // use of this.request.query cause this.params.query bad interpreted in this case valeur: '20;2B;Maclean;ID=f41f;SWITCH=01;CMD=OFF;'
  const req = this.request;
  const pHard_id = req.query.id;
  let pMode = req.query.mode;
  let pCle = req.query.cle;
  let pValeur = req.query.valeur;
  const pIndex = req.query.index;
  if (pMode) pMode = pMode.toUpperCase();

  // ---------- DEBUT LOGS -------

  const ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress || req.socket.remoteAddress || req.connection.socket.remoteAddress;
  const fullUrl = req.connection.parser.incoming.originalUrl;

  let log = {
    ip: ip,
    hard_id: pHard_id,
    logs: [fullUrl],
    tags: []
  };

  if (pMode) log.tags.push(pMode.toLowerCase());

  if (!pHard_id || !pCle || !pValeur || !pMode) {
    log.tags.push("error");
    log.comment = "format url non pris en charge, id=?&cle=?&valeur=?&mode";
    appHd.logs(log);
    return appHd.respNok(this, "format url non pris en charge, id=?&cle=?&valeur=?&mode");
  }
  else appHd.logs(log);

  // --------- FIN LOGS -------

  const donnees = {
    hard_id: pHard_id,
    cle: pCle,
    valeur: pValeur,
    date: Date.now(),
    ip: ip
  };

  // Setup hardware and save last data for dashboard
  if (pHard_id === '01234567890123456')
    return appHd.respOk(this, "Hardware non enregistre !");

  const HardOne = Hardwares.findOne(pHard_id);

  if (HardOne) {
    // if(ip !== "127.0.0.1")
    Hardwares.update(pHard_id, { $set: { ip: ip, lastSeen: new Date() } });
  }
  else
    return appHd.respNok(this, "unknown_hard");

  // les differents modes d'add
  if (pMode === "INIT") {                                                                              /****************** INIT *********************/
    return appHd.respOk(this, "Mode init... je comprends pas..."); //FIXME Orphelin avec un id en demande d'init?
  }
  else if (pMode === "EEPROM") {
    if (pCle.toUpperCase() === "DEBUT") {
      Eeprom.remove({ hard_id: pHard_id });
      return appHd.respOk(this, "ok EEPROM DEBUT");
    }
    else if (pCle.toUpperCase() === "FIN") {
      const etats = EepromTemp.find({ hard_id: pHard_id }).fetch();
      Eeprom.insert({ hard_id: pHard_id, etat: etats, date: Date.now() });
      EepromTemp.remove({ hard_id: pHard_id });
      return appHd.respOk(this, "ok EEPROM FIN");
    }
    else if (pCle.toUpperCase() === "COMBIEN_ADRESSE_PINGER") {
      const pingsCount = HardOne.devices.filter(function (d) {
        return d.type_id === 185
      }).length;
      const command = "http://" + HardOne.ip + ":" + HardOne.port + "/etat?cle=nb_ip_pinger&valeur=" + pingsCount + "&mode=EEPROM"
      HTTP.get(command, function (error, result) {
        if (error) {
          console.log('http get FAILED! router.js 590');
        }
        else {
          //console.log(command + ' http get SUCCES');
          // if(result.statusCode === 200) {
          //   console.log('Status code = 200');
          // }
        }
      });
      return appHd.respOk(this, "ok EEPROM FIN");
    }
    else if (pCle.toUpperCase() === "DEMANDEPINGADRESS") {
      const pingAdressArray = HardOne.devices.filter(function (d) {
        return d.type_id === 185
      });
      const ip = pingAdressArray[pValeur - 1].ip.replace(/\./g, ',');
      HTTP.get("http://" + HardOne.ip + ":" + HardOne.port + "/etat?cle=PINGadress" + pValeur + "&valeur=" + ip + "&mode=EEPROM", function (error, result) {
        if (error) {
          console.log('http get FAILED! router.js 608');
        }
        else {
          // console.log('http get SUCCES');
          if (result.statusCode === 200) {
            // console.log('Status code = 200!');
            // console.log(result.content);
            // for(let i = 0; i < pings.length; i++) {
            //   let ping = pings[i];
            //   HTTP.get("http://" + HardOne.ip + ":" + HardOne.port + "/etat?cle=nb_ip_pinger&valeur=" + nbPing + "&mode=EEPROM", function (error, result) {
            //   });
            // }
          }
        }
      });
      return appHd.respOk(this, "ok EEPROM FIN");
    }
    else {
      if (!EepromTemp.insert(donnees)) {
        return appHd.respNok(this, "nok! EEPROM save");
      }
      else {
        return appHd.respOk(this, "ok EEPROM save " + pHard_id + ' / ' + pCle);
      }
    }
  }
  else if (pMode === "BOOT") {
    /****************** BOOT *********************/

    if (pCle.toUpperCase() === "DEMANDE") {
      if (pValeur.toUpperCase() === "OKKO") {
        //TODO verifier la conf
        HTTP.get("http://" + HardOne.ip + ":" + HardOne.port + "/etat?cle=CONF_OK&valeur=yo&mode=EEPROM", function (error, result) {
          if (error) {
            console.log('http get FAILED! router add.js 131 ouverture de port?');
            return appHd.respNok(this, "Aucune commucation, ouverture du port ok?");
          }
          else {
            // console.log('http get SUCCES');
            if (result.statusCode === 200) {
              // console.log('Status code = 200!');
              // console.log(result.content);
            }
          }
        });

        return appHd.respOk(this, "ok demande okko entendue");
      }
    }
    else if (pCle.toUpperCase() === "DEBUT") {

      Hardwares.update(pHard_id, { $set: { lastBoot: new Date() } });

      // efface les anciens bugs de l'arduino qui boot
      Bugs.remove({ hard_id: pHard_id });

      Boots.update({ hard_id: pHard_id }, { $set: { archived: true } }, { multi: true });

      Boots.insert({ hard_id: pHard_id, boot: [], date: Date.now() });

      // si config free envoyer un sms "arduino boot"
      const u = Meteor.users.findOne(HardOne.owner_id);
      if (u?.settings?.notifHardBoot) appHd.sendNotifs(u._id, appHd.aliasname(HardOne) + " boot");

      return appHd.respOk(this, "ok BOOT DEBUT");
    }
    else {
      if (pCle.toLowerCase() === "date_et_version_arduino" || pCle.toLowerCase() === "version") Hardwares.update(pHard_id, { $set: { version: pValeur } });
      if (pCle.toLowerCase() === "nom_arduino") Hardwares.update(pHard_id, { $set: { name: pValeur } });

      if (Boots.update({ hard_id: pHard_id, "archived": { $ne: true } }, { $push: { boot: donnees } })) return appHd.respOk(this, "ok BOOT save " + pHard_id + '/' + pCle);
      else return appHd.respNok(this, "nok! BOOT save");
    }
  }
  else if (pMode === "LIVE") {
    /****************** LIVE *********************/
    Live.insert(donnees);
    return appHd.respOk(this, "ok LIVE save");
  }
  else if (pMode === "BUG") {
    /****************** BUG *********************/
    Bugs.insert(donnees);
    return appHd.respOk(this, "ok BUG save");
  }
  else if (pMode === "ETAT") {
    /****************** ETAT *********************/
    if (pCle.toUpperCase() === "DEBUT") {
      Etats.remove({ hard_id: pHard_id });
      return appHd.respOk(this, "ok ETAT DEBUT");
    }
    else if (pCle.toUpperCase() === "FIN") {
      const etats = EtatTemp.find({ hard_id: pHard_id }).fetch();
      Etats.insert({ hard_id: pHard_id, state: etats, date: Date.now() });
      EtatTemp.remove({ hard_id: pHard_id });
      return appHd.respOk(this, "ok ETAT FIN");
    }
    else {
      EtatTemp.insert(donnees);
      return appHd.respOk(this, "ok ETAT save " + pHard_id + '/' + pCle);
    }
  }
  else if (pMode === "433") {
    /****************** 433 *********************/
    // old
    // /add?id=aaaa&mode=433&cle=BROCHE_0_RXlearn433mhz&valeur=1111111000110101110000000001001000010001111011100011
    //
    // /add?id=aaaa&mode=433&cle=RX_RFLINK&valeur=20;3F;Blyss;ID=5c01;SWITCH=D0;CMD=OFF;
    // mode 433Learn
    // la cle tournante renvoie toujours une value differente, tester la longueur de la cle 52digit (13x4) les 16 derniers inutiles.
    // il faut gerer les diffeents protocole blyss etc... sur une page lear 433
    if (pValeur !== "Emission_OK") {
      Mhz433.update({ hard_id: pHard_id, valeur: pValeur }, {
        hard_id: pHard_id,
        key: pCle,
        value: pValeur
      }, { upsert: true });
    }
    return appHd.respOk(this, "ok valeur recue");
  }
  else if (pMode === "ARCHIVE") {
    /****************** ARCHIVE *********************/

    return appHd.dataAddArchive({
      connexionType: 'http',
      hard_id: pHard_id,
      key: pCle,
      value: pValeur,
      index: pIndex,
      that: this
    });
  }
  else if (pMode === "SMS") {
    /****************** SMS *********************/
    const hard = Hardwares.findOne(pHard_id);
    if (hard) {
      const user = Meteor.users.findOne(hard.owner_id);
      if (user) {
        const uf = appHd.isFreeConfig(user);
        if (uf) {
          const message = appHd.aliasname(HardOne) + " demande : \r\n Cles : " + pCle + "\r\n Valeur : " + pValeur;
          const that = this;
          Meteor.call('sendFreeSMS', uf.login, uf.key, message, function (err) {
            if (err) appHd.respNok(that, "err sending sms");
            else return appHd.respOk(that, "ok SMS send");
          });
        }
        else appHd.respNok(this, "err sending sms, user " + user._id + " has no free config");
      }
      else appHd.respNok(this, "err sending sms, user not found");
    }
    else return appHd.respNok(this, "unknown_hard");
  }
  else {
    log.log = "format url non pris en charge, mode ok?";
    log.tags = ['error'];
    appHd.logs(log);
    return appHd.respNok(this, "format url non pris en charge, mode ok?");
  }
});

// imperiHome

Router.route('/api/ih/system', { where: 'server' })
  .get(function () {
    const auth = appHd.checkBasicAuth(this);
    if (!auth) return appHd.respNok(this, 'bad login / password');
    else return appHd.respOk(this, '{"id":"homedoudou.fr","apiversion": 0.1}');
  });

Router.route('/api/ih/rooms', { where: 'server' })
  .get(function () {
    const userId = appHd.checkBasicAuth(this);
    if (!userId) return appHd.respNok(this, 'bad login / password');

    let rooms = Rooms.find({ userId: userId }).fetch();
    rooms = rooms.map(function (r) {
      return { id: r._id, name: r.name }
    });

    return appHd.respOk(this, '{"rooms":' + JSON.stringify(rooms) + '}');
  });

Router.route('/api/ih/devices', { where: 'server' }).get(function () {
  const userId = appHd.checkBasicAuth(this);
  if (!userId) return appHd.respNok(this, 'bad login / password');

  let hards = Hardwares.find({ owner_id: userId }).fetch().map(function (h) {
    return h._id;
  });
  let devices = Keys.find({ hard_id: { $in: hards } }).fetch();
  devices = {
    "devices": devices.map(function (r) {
      let data = {
        "id": r._id,
        "name": appHd.aliasname(r),
        "room": r.room
      };
      if (r.type === "temp") {
        data.type = "DevTemperature";
        data.params = [{
          "key": "Value",
          "value": r.lastData?.value,
          "unit": r.unit,
          "graphable": true
        }, {
          "key": "defaultIcon",
          "value": "https://www.homedoudou.fr/img/temperature.jpg"
        }]
      }
      else if (r.type === "hum") {
        data.type = "DevHygrometry";
        data.params = [{
          "key": "Value",
          "value": r.lastData?.value,
          "unit": r.unit,
          "graphable": true
        }, {
          "key": "defaultIcon",
          "value": "https://www.homedoudou.fr/img/humidite.jpg"
        }]
      }
      else {
        data.type = "DevGenericSensor";
        if (r.last) {
          data.params = [{
            "key": "Value",
            "value": r.lastData?.value,
            "unit": r.unit
          }]
        }
      }
      return data
    })
  };

  return appHd.respOkKeep(this, JSON.stringify(devices));
});

Router.route('/api/ih/devices/:deviceId/:paramKey/histo/:startdate/:enddate', { where: 'server' }).get(function () {
  const userId = appHd.checkBasicAuth(this);
  if (!userId) return appHd.respNok(this, 'bad login / password');

  let data = DataByKey[this.params.deviceId].find({
    date: {
      $gt: this.params.startdate * 1,
      $lt: this.params.enddate * 1
    }
  }).fetch().map(function (d) {
    return { date: d.date, value: d.valeur }
  });

  data = {
    "values": data
  };

  return appHd.respOkJson(this, JSON.stringify(data));
});

// API HOMEBRIDGE

Router.route('/api/homebridge/getLast/:hard_id/:keyName', { where: 'server' })
  .get(function (req, res) {
    const keyOne = Keys.findOne({ hard_id: this.params.hard_id, name: this.params.keyName })
    return appHd.respOk(this, keyOne.lastData?.value);
  });

// API HOMEDOUDOU

Router.route('/api/scenario/action', { where: 'server' }).get(function () {

  const ip = this.request.headers['x-forwarded-for'] ||
    this.request.connection.remoteAddress ||
    this.request.socket.remoteAddress ||
    this.request.connection.socket.remoteAddress;

  const query = this.request.query;
  // const data = {
  //   id: this.request.body.id,
  //   result: this.request.body.result,
  //   date: Date.now(),
  //   ip: ip
  // };

  const a = appHd.scenario.actionLaunch(query.id);
  if (!a || a.err) {
    Logs.insert({
      ip: ip,
      log: a?.msg,
      tags: ['scenario', 'action', 'error'],
      date: Date.now()
    });
    return appHd.respNok(this, a ? JSON.stringify(a) : ' no return actionLaunch');
  }
  else {
    Logs.insert({
      ip: ip,
      hard_id: a.hard_id,
      log: JSON.stringify(a),
      tags: ['scenario', 'action'],
      date: Date.now()
    });
    return appHd.respOk(this, JSON.stringify(a));
  }
});

Router.route('/api/scenario', { where: 'server' }).get(function () {

  const ip = this.request.headers['x-forwarded-for'] ||
    this.request.connection.remoteAddress ||
    this.request.socket.remoteAddress ||
    this.request.connection.socket.remoteAddress;

  const query = this.request.query;


  const a = appHd.scenario.actionLaunch(query.id);

  if (!a || a.err) {
    Logs.insert({
      ip: ip,
      log: a?.msg,
      tags: ['scenario', 'action', 'error'],
      date: Date.now()
    });
    return appHd.respNok(this, a ? JSON.stringify(a) : ' no return actionLaunch');
  }
  else {
    Logs.insert({
      ip: ip,
      hard_id: a.hard_id,
      log: JSON.stringify(a),
      tags: ['scenario', 'action'],
      date: Date.now()
    });
    return appHd.respOk(this, JSON.stringify(a));
  }
});

//export Excel
// Router.route('exportExcel', {
//   where: 'server',
//   path: '/exportExcel/:filename',
//   action: function () {
//     const all = Meteor.users.find().fetch();
//     const output = exportCSV(all);
//     this.response.writeHead(200, {
//       'Content-Type': 'application/zip',
//       'Content-Disposition': 'attachment; filename=' + filename
//     });
//     fs.createReadStream(output).pipe(this.response);
//   }
// });

// NAMESPACE
if (typeof (appHd) === "undefined") appHd = {};

// reponse HTTP
appHd.respOk = function (that, text, log) {
  if (log && log.enabled) {
    const hardOne = Hardwares.findOne(log.hard_id);
    appHd.logs({
      hard_id: hardOne ? hardOne._id : '',
      log: text,
      comment: log.comment ? log.comment : "",
      tags: ['http', 'send']
    });
  }
  if (!that || !that.response) {
    console.log('error ', text, log);
    return false
  }
  that.response.writeHead(200, {
    "Content-Type": "text/plain",
    "Connection": "Close",
    "Content-Length": text.length
  });
  that.response.end(text);
  return true;
};

appHd.respNok = function (that, text, log) {
  if (log && log.enabled) {
    const hardOne = Hardwares.findOne(log.hard_id);
    appHd.logs({
      hard_id: hardOne ? hardOne._id : '',
      log: text,
      comment: log.comment ? log.comment : "",
      tags: ['error', 'http', 'send']
    });
  }
  that.response.writeHead(500, {
    "Content-Type": "text/plain",
    "Connection": "Close",
    "Content-Length": text.length
  });
  that.response.end(text);
  return false;
};

appHd.respOkJson = function (that, data) {
  that.response.writeHead(200, {
    "Connection": "Close",
    "Content-Type": "application/json"
  });
  that.response.end(data);
  return true;
};

appHd.respOkKeep = function (that, data) {
  that.response.writeHead(200, {
    "Connection": "Keep-Alive",
    "Content-Type": "application/json"
  });
  that.response.end(data);
  return true;
};

appHd.checkBasicAuth = function (that) {

  if (!that.request.headers.authorization) return false;
  const auth = that.request.headers.authorization.split(' ')[1];
  if (!auth) return false;

  const plain_auth = new Buffer(auth, 'base64').toString().split(':');
  if (!plain_auth) return false;

  const u = Meteor.users.findOne({ "emails.0.address": plain_auth[0] });
  if (!u) return false;

  const result = Accounts._checkPassword(u, plain_auth[1]);
  if (!result.userId) return false;
  else return result.userId;
};