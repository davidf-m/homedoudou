import './admin-messages.html';

Template.adminMessages.onCreated(function () {
});

Template.adminMessages.onRendered(function () {
});

Template.adminMessages.helpers({

  breadcrumbLinks() {
    return appHd.buildBreadcrumb(["dashboard", "adminMessages"]);
  },

  getMessages() {
    const messages = Messages.find({}, {sort: {createdAt: -1}});
    return messages.count() ? {messages: messages} : false;
  },

  userId() {
    return this.userId
  }
});

Template.adminMessages.events({

  'click .js-close': function () {
    Meteor.call('messagesDelete', this._id)
  },
});
