/**
 * Created by David on 21/05/2015.
 */

import './admin-devices.html';

Template.adminDevice.onCreated(function () {
  Session.set("device-edit", "")
});

Template.adminDevice.helpers({

  breadcrumbLinks() {
    return appHd.buildBreadcrumb(["dashboard", "adminDevice"]);
  },

  devices: function () {
    return Device.find({}, { sort: { "type_id": 1 } });
  },

  edit: function (id) {
    return Session.get("device-edit") === id
  }

});

Template.adminDevice.events({

  'click .js-edit': function (e) {
    Session.set("device-edit", this._id)
  },

  'click .js-delete': function (e) {
    const that = this;
    dfm.confirm('Effacer ?', function (result) {
      if (result)
        Meteor.call('deviceDelete', that._id);
    })

  },

  'click .js-edit-valid': function (e) {
    e.preventDefault();

    const newDevice = {};
    newDevice.type_id = $('[name="type_id_edit"]').val() * 1;
    newDevice.title = $('[name="title_edit"]').val();
    newDevice.body = $('[name="body_edit"]').val();

    Device.update(this._id, { $set: newDevice });

    Session.set("device-edit", "");
  },

  'submit': function (e) {
    e.preventDefault();

    const newDevice = {};
    newDevice.type_id = $('[name="type_id"]').val() * 1;
    newDevice.title = $('[name="title"]').val();
    newDevice.body = $('[name="body"]').val();

    Device.insert(newDevice);

    $('.in-reset').val('');
  },

});
