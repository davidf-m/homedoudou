import DfmConnections from 'meteor/davidfm:core/api/accounts/lib/collections.js';
import './ddp.html';

Template.adminDdp.helpers({

  breadcrumbLinks() {
    return appHd.buildBreadcrumb(["dashboard", "adminDdp"]);
  },

  connected() {
    return DfmConnections.find();
  }

});

Template.adminDdp.events({

  'click .js-talk'(e) {
    const msg = $(e.currentTarget).data('talk');
    Meteor.call('ddpFromHmd', this._id, { "msg": msg }, function (err, r) {
      console.log(r);
    });
  }
});

