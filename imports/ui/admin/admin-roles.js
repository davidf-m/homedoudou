import './admin-roles.html';

Template.adminRoles.onCreated(function () {
  this.subscribe('roles');
});

Template.adminRoles.onRendered(function () {
  $(".chosen-roles").chosen({width: "95%"});
});

Template.adminRoles.helpers({

  users: function () {
    return Meteor.users.find({});
  },

  labelClass: function () {
    if (!this.status) return "";
    if (this.status.idle)
      return "badge-warning";
    else if (this.status.online)
      return "badge-primary";
    else
      return "badge-default";
  },

  isVerified: function () {
    if (this.verified)
      return "verifiée";
    else
      return "non verifiée";

  },

  multipleselected: function (roles) {
    if (Roles.userIsInRole(this.parent._id, [roles])) {
      return 'selected';
    }
    return '';
  },

  roles: function () {
    return Roles.getAllRoles();
  },

  inscrit: function () {
    return this.createdAt;
  },

  lastLogin: function () {
    return this.status.lastLogin.date;
  },
});

Template.adminRoles.events({

  'change [name=new-role]': function (e) {
    console.log('r');
    var $val = $(e.target);
    Roles.createRole($val.val());
    $val.val('');
  }

});
