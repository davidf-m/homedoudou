import './admin-hardware.html';

Template.adminHardware.helpers({

  dataCount: function (hard_id, key_name) {
    Meteor.call('dataCount', hard_id, key_name, function (err, res) {
      if(!err)
        Session.set('dataCount' + hard_id + key_name, res)
    });
    return Session.get('dataCount' + hard_id + key_name);
  },

  keys: function () {
    return Keys.find({hard_id: this._id})
  }
});
