import './admin-karotz.html';

Template.adminKarotz.helpers({

  cmdsKarotz() {
    return HMD_Karotz.find({type: 'cmdUpdate', deleted: {"$exists": false}}, {sort: {idx: 1}});
  },

  radiosKarotz() {
    return HMD_Karotz.find({type: 'radios', deleted: {"$exists": false}}, {sort: {idx: 1}});
  },
});

Template.adminKarotz.events({

  'submit #cmd-karotz': function (e, tpl) {
    e.preventDefault();

    const data = tpl.$('#cmd-karotz').serializeJSON();
    Meteor.call('karotz.cmd.update.insert', data, function (err) {
      if(!err) {
        $('.cmd-karotz-new').val('')
      }
      else {
        console.log(err);
      }
    });
  },

  'submit #radios-karotz': function (e, tpl) {
    e.preventDefault();

    const data = tpl.$('#radios-karotz').serializeJSON();
    console.log(data);
    Meteor.call('karotz.radios.update.insert', data, function (err) {
      if(!err) {
        $('.radio-karotz-new').val('')
      }
      else {
        console.log(err);
      }
    });
  },

});