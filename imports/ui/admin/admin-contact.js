import './admin-contact.html';

Template.adminContact.helpers({

  contacts() {
    return Contact.find({}, {sort: {created: -1}});
  },
});
