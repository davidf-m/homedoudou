/**
 * Created by David FAIVRE-MAÇON on 24 january 2020
 * https://www.davidfm.me
 */

import './configs.html';

Template.webmasterConfigs.onRendered(function () {

  Meteor.call('getTime', function (err, now) {
    console.log(now);
    const time = ('0' + now.getHours()).substr(-2) + ":" + ('0' + now.getMinutes()).substr(-2);

    if(!err) $('#time').text(time);
    else alert(err)
  });

  $('.sortable').sortable({
    update: function (event, ui) {
      _.each(event.target.children, function (config, newOrder) {
        const configId = $(config).find('.row')[0].attr('data-sortable-config_id');
        console.log(configId);
        Meteor.call('configs.update', configId, {order: newOrder});
      })
    },
    axis: 'y',
    distance: 5,
    cancel: '.not-dragguable, input'
  });

});

Template.webmasterConfigs.helpers({

  types() {
    return Configs.find({sourceId: "keyType"}, {sort: {order: 1}});
  },

  tags() {
    return Configs.find({sourceId: "tag"}, {sort: {value: 1}});
  }
});

Template.webmasterConfigs.events({

  'submit #key-type-add'(e, tpl) {
    e.preventDefault();

    const data = tpl.$(e.currentTarget).serializeJSON();
    data.sourceId = "keyType";
    Meteor.call('configs.insert', data, function (err) {
      if(!err) tpl.$(e.currentTarget)[0].reset();
    })
  },

  'submit .key-type-update'(e, tpl) {
    e.preventDefault();

    const data = tpl.$(e.currentTarget).serializeJSON();

    console.log(data);
    Meteor.call('configs.update', this._id, data)
  },

  'click .js-update-save'(e, tpl) {
    tpl.$(e.currentTarget).submit();
  },

  'click .js-remove'() {
    Meteor.call('configs.remove', this._id)
  },
});