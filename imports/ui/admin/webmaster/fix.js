import './fix.html';

Template.webmasterFix.helpers({

  actions() {
    return Scenarios.find({scenarioType: "action"})
  },

});

Template.webmasterFix.events({

  'submit': function (e, tpl) {
    e.preventDefault();

    const form = tpl.$('form').serializeJSON();
    Meteor.call('webmasterActionsFix', form.actions, function (err, r) {
      if (err) console.log(err);
      else tpl.$('form')[0].reset();
    });
  }

});