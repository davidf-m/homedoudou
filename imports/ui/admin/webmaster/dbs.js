import './dbs.html';

let dbsList = new ReactiveVar();
Template.webmasterDbs.onCreated(function () {
  Meteor.call('webmasterDbsList', function (err, dbs) {
    dbs.sort((a, b) => a.name.localeCompare(b.name));
    dbsList.set(dbs);
  });
});

Template.webmasterDbs.helpers({

  dbs() {
    console.log(dbsList.get());
    return dbsList.get();
  },
});

Template.webmasterDbs.events({

  'click .js-delete-DataByKey-orphan': function (e) {
    const that = this;
    dfm.confirm('sure?', function (r) {
      if (r) {
        console.log(that);
        Meteor.call('webmasterDbRemoveOrphanDataByKey', function (err, r) {
          if (!err) alert('refresh f5 !')
        });
      }
      else console.log(err)
    })
  },

  'click .js-delete-db': function (e) {
    e.preventDefault();

    const that = this;
    dfm.confirm('sure?', function (r) {
      if (r) {
        console.log(that);
        Meteor.call('webmasterDbRemove', that.name, function (err, r) {
          if (!err)
            Meteor.call('webmasterDbsList', function (err, dbs) {
              dbs.sort((a, b) => a.name.localeCompare(b.name));
              dbsList.set(dbs);
            });
        });
      }
      else console.log(err)
    })
  }

});

Template.webmasterDbsKey.events({

  'submit': function (e) {
    e.preventDefault();

    console.log(this);
    const form = $(e.currentTarget).serializeJSON();
    console.log(form);
    Meteor.call('webmasterDbMoveDataKey', this._id, form, (err, res) => {
      if (err) return dfm.alert(JSON.stringify(err))
      else dfm.toast('finish')
    })
  },


});