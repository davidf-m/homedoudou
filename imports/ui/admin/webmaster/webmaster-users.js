import './webmaster-users.html';

Template.webmasterUsers.onCreated(function () {
  this.subscribe('webmasterUsers');
});

Template.webmasterUsers.onRendered(function () {
  this.subscribe("roles", {
    onReady: function () {
      $(".chosen-roles").chosen({width: "95%"});
    },
    onError: function () {
      console.log("onError", arguments);
    }
  });
});

Template.webmasterUsers.helpers({

  users: function () {
    return Meteor.users.find({}, {sort: {"status.lastLogin.date": -1}});
  },

  labelClass: function () {
    if (!this.status) return "";
    if (this.status.idle)
      return "badge-warning";
    else if (this.status.online)
      return "badge-primary";
    return "badge-default";
  },

  isVerified: function () {
    if (this.verified)
      return "verifiée";
    return "non verifiée";
  },

  roleSelected: function (role) {
    if (Roles.userIsInRole(this.parent._id, [role]))
      return 'selected';
    return '';
  },

  roleDisabled: function (role) {
    if (!Roles.userIsInRole(Meteor.userId(), ['webmaster', 'admin']))
      return 'disabled';
    if (!Roles.userIsInRole(Meteor.userId(), ['webmaster']) && role == 'webmaster')
      return 'disabled';
    if (!Roles.userIsInRole(Meteor.userId(), ['webmaster']) && role == 'admin')
      return 'disabled';
    return '';
  },

  roles: function () {
    return Roles.getAllRoles();
  },

  inscrit: function () {
    return this.createdAt;
  },

  lastLogin: function () {
    if (!this.status || !this.status.lastLogin) return false;
    return this.status.lastLogin.date;
  },
});

Template.webmasterUsers.events({

  'change select': function (e) {
    var user = this;
    var roles = $(e.target).val();

    _.each($(e.target.children), function (chaqueObj) {
      var chaque = chaqueObj.text;
      if (!_.some(roles, function (role) {
          return (role == chaque);
        }))
        Roles.removeUsersFromRoles(user._id, chaque, Roles.GLOBAL_GROUP)
    });


    _.each(roles, function (role, i) {
      Roles.addUsersToRoles(user._id, role, Roles.GLOBAL_GROUP);
    });
  }

});
