import './admin-debug.html';

Template.adminDebug.helpers({

  configCle: function () {
    return Keys.find({}, {sort: {hard_id: 1}});
  },

  except: function () {
    return Session.get('hardExcept');
  },

  hardName: function () {
    const hard = Hardwares.findOne(this.hard_id);
    return hard ? hard.alias : '----'
  }
});

Template.adminDebug.events({

  'click .js-hard': function () {
    const filters = Session.get('hardExcept') || [];
    const hard_id = this.hard_id;
    const i = appHd.arrayObjectIndexOf(filters, hard_id, 'hard_id');
    if(i > -1)
      filters.splice(i, 1);
    else
      filters.push({hard_id: hard_id});
    Session.set('hardExcept', filters);
  },

  'click .js-delete-hard': function () {
    Meteor.call('hardDeleteById', this.hard_id)
  }
});
