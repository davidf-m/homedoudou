/**
* Created by David FAIVRE-MAÇON
* https://www.davidfm.me
*/

import DfmConnections from 'meteor/davidfm:core/api/accounts/lib/collections.js';
import './admin-hardwares.html';

const orderName = 'adminHardwares';
Template.adminHardwares.onCreated(function () {
  this.subscribe('configurationBootsServer');
});

Template.adminHardwares.helpers({

  breadcrumbLinks() {
    return appHd.buildBreadcrumb(["dashboard", "adminHardwares"]);
  },

  hardwares() {
    const sort = {};
    sort[Session.get("sortBy_" + orderName)] = Session.get("sortOrder_" + orderName);
    return Hardwares.find({}, { sort });
  },

  users() {
    return Meteor.users.find({});
  },

  owner() {
    const owner = Meteor.users.findOne({ _id: this.owner_id });
    if (owner)
      return owner.username;
    return "Orphelin";
  },

  ownerSelected(user_id) {
    if (user_id === this.parent.owner_id)
      return 'selected';
    return '';
  },

  getConnection() {
    console.log(this)
    return DfmConnections.findOne({ id: this.ddpId })
  },

  serverBoot() {
    //configurationBootsServer
    return Boots.findOne({ hard_id: "server" }, { sort: { date: -1 } });
  }
});

Template.adminHardwares.events({

  'click .js-toggle'(e, tpl) {
    $(e.currentTarget).parent().parent().find('.toggle-fhide').toggle();
  },

  'click .js-lire-id'(e) {

    const hard = this;
    Meteor.call('lireId', hard._id, function (err, back) {
      if (err) {
        $(e.target).text('error');
        console.log(err);
      }
      else {
        // seulement en retour synchro http
        if (back && back.data)
          $(e.target).text('id: ' + back.data.lire_id);
        else appHd.messages.add('voir log si ddp', 'lireId', back)
      }
    });

  },

  'click .js-remove': function () {
    const that = this;
    dfm.confirm('Supprimer ?', function (result) {
      if (result) {
        Meteor.call('hardDeleteById', that._id);
      }
    });
  },

  'click .js-raz': function (e) {
    console.log('raz');

    const hard = this;
    dfm.confirm('Remettre à zéro?', function (result) {
      if (result) {
        Meteor.call('hardwareRaz', hard._id, function (err, back) {
          if (err) {
            throw new Meteor.Error("500", "Error raz: " + err);
          }
          else {
            appHd.alert("Arduino remis à zéro");
          }
        });
      }
    });
  },

  'click .js-reset': function (e, tpl) {
    const hard = this;

    // TODO mettre des toasts au lieu de tout ce bazard
    let name = "John Doe";
    if (hard.alias) name = hard.alias;
    dfm.confirm('Redemarrer ' + name + ' ?', function (result) {
      const $btn = $(e.currentTarget);
      if (result) {
        $btn.text('En cours');
        log(hard._id)
        Meteor.call('hardwareReset', hard._id, function (err, back) {
          if (err) {
            $btn.text('Erreur');
            dfm.toastError(err)
          }
          else {
            log(back)
            if (back.err) {
              console.log(back);
              $btn.text('Erreur');
              dfm.toastError("erreur, voir la console")
            }
            else {
              $btn.removeClass('btn-warning');
              $btn.addClass('btn-success');
              $btn.text('Ok entendue');
            }
          }
        })
      }
      else
        dfm.toast('cancel')
    });
  },

  'change [name=owner]': function (e) {
    const newOwner_id = $(e.target).val();
    const Hard = Hardwares.findOne(this._id);
    if (newOwner_id) {
      Hardwares.update({ _id: Hard._id }, { $set: { owner_id: newOwner_id } });
      appHd.logsInfo({ log: 'change hard : ' + Hard._id + '  to ' + newOwner_id });
    }
    else {
      Hardwares.update({ _id: Hard._id }, { $set: { owner_id: "" } });
      appHd.logsInfo({ log: 'change hard : ' + Hard._id + '  to nobody' });
    }
  },

  'change [name=share]': function (e) {
    const shareUsername = $(e.target).val();
    //const newOwnerObj = Meteor.users.findOne({username: shareUsername});
    //if (newOwnerObj) {
    //  const hardObj = ConfigsQui.findOne({hard_id: this.hard_id});
    //  ConfigsQui.update({_id: hardObj._id}, {$set: {owner_id: newOwnerObj._id}});
    //}
  },

});
