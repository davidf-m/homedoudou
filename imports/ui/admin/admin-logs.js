import './admin-logs.html';

const tplReady = new ReactiveVar();

const query = {};
const options = { sort: { date: -1 }, limit: 50 };

Template.adminLogs.onCreated(function () {
    const instance = this;
    instance.subscribe('adminHardwares');
    instance.subscribe('adminKeys');
    instance.subscribe('adminUsers');
    instance.autorun(function () {
        const params = Router.current().params;
        if (params.hard_id) query.hard_id = params.hard_id;
        if (query.tags) delete query.tags;
        if (params.query.tag) query.tags = params.query.tag;
        if (params.limit) options.limit = params.limit * 1;

        console.log(params);
        const handle = instance.subscribe('adminLogs', query, options);
        if (handle.ready()) tplReady.set(true);
        else tplReady.set(false);
    });
});

Template.adminLogs.onRendered(function () {
});

Template.adminLogs.helpers({

    getHardId() {
        return Router.current().params.hard_id
    },

    breadcrumbLinks() {
        if (this.hard_id) {
            const HardOne = Hardwares.findOne({ _id: this.hard_id });
            return appHd.buildBreadcrumb(["dashboard", "adminLogs", appHd.aliasname(HardOne)]);
        } else
            return appHd.buildBreadcrumb(["dashboard", "adminLogs"]);
    },

    isTplReady() {
        return tplReady.get();
    },

    hards: function () {
        const hards = Hardwares.find().fetch();
        const out = [];
        _.each(hards, function (hard) {
            let name = 'John Doe';
            if (hard && hard.alias) name = hard.alias;
            out.push({ _id: hard._id, name: name, lastSeen: hard.lastSeen, ddpId: hard.ddpId });
        });
        return out;
    },

    actif: function (ctx, parent) {
        return ctx._id === parent.hard_id ? 'btn-selected' : ''
    },

    getLogs: function () {
        // appHd.log(this)
        return Logs.find(query, options);
    },

    hard_name: function () {
        const hard = Hardwares.findOne(this.hard_id);
        if (!hard) return this.hard_id;
        return appHd.aliasname(Hardwares.findOne(this.hard_id));
    },

    tabActive: function (tabs) {
        return this.limit === tabs ? 'tabActive' : ''
    },

    tagHtml: function (tag) {
        if (tag === "error" || tag === "bug") return '<span class="badge badge-danger">' + tag + '</span>';
        else return tag
    },

    tagClass(tag) {
        return (tag === "error" || tag === "bug") ? "badge-danger" : "badge-primary";
    },

    getAllTags() {
        return Configs.find({ sourceId: "tag" }, { sort: { value: 1 } });
    },

    isTagSelected(tag) {
        return Router.current().params.query.tag === tag;
    }
});

Template.adminLogs.events({

    'click #deleteall': function () {
        console.log(this);
        const hard_id = this.hard_id || "all";
        dfm.confirm('effacer les logs?', (res) => {
            if (!res) return
            Meteor.call('logsRemove', hard_id, function (err, data) {
                !err ? console.log('ok') : console.log('nok'); // FIXME retour visuel, ok effacé?
            });
        })
    },

    'click .nb': function (e) {
        $("#tabs ul li").removeClass('tabActive');
        $(e.target).addClass('tabActive');
        const limit = parseInt($(e.target).attr("content"));
        Session.set("logs_limit", limit);
    }
});
