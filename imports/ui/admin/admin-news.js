/**
 * Created by David on 16/06/2015.
 */

import './admin-news.html';

Template.adminNews.helpers({

  news() {
    return News.find({}, {sort: {flash: -1, created: -1}});
  },

  isEditing() {
    return Session.get('news-edit') === this._id;
  }
});

Template.adminNews.events({


  'submit': function (e) {
    e.preventDefault();

    const news = {title: $('[name=title]').val()};
    Meteor.call('news-insert', news, function (err, result) {
      if(!err) $('[name=title]').val('');
    });
  },

  'click .js-edit': function () {
    Session.set('news-edit', this._id);
  },

  'click .js-edit-submit': function () {
    const set = {title: $('[name=title-edit]').val()};
    Meteor.call('news-update', this._id, set, function (err, result) {
      if(!err) $('[name=title]').val('');
    });

    Session.set('news-edit', undefined);
  },

  'click .js-cancel': function () {
    Session.set('news-edit', undefined);
  },

  'click #delNews': function () {
    if(confirm('sur de vouloir effacer?')) Meteor.call('news-delete', this._id);
  },

});
