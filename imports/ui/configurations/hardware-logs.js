/**
 * Created by davidfm.me on 28 april 2019
 */

import './hardware-logs.html';

Template.configurationHardwareLogs.onCreated(function () {
});

Template.configurationHardwareLogs.onRendered(function () {
});

Template.configurationHardwareLogs.helpers({

  breadcrumbLinks() {
    return appHd.buildBreadcrumb(["dashboard", "configuration", "configurationHardware", { class: "color-config", title: "Logs " + appHd.aliasname(this) }]);
  },

  hardware() {
    return Hardwares.findOne(this.hard_id);
  },

  boots: function () {
    return Boots.findOne({ hard_id: this._id });
  },

  bootLine() {
    let boot = "";
    log(this)
    for (let k in this) {
      if (k === "cle")
        boot += '<span class="badge badge-primary">' + this[k] + '</span>' + " ";
      else if (k === "valeur")
        boot += '<span class="badge badge-success">' + this[k] + '</span>' + " ";
      else if (k === "date")
        boot += dfm.formatDate({ date: this[k] }) + " ";
      else if (k === "ip")
        boot += '<span class="badge badge-warning">' + this[k] + '</span>' + " ";
      else if (k !== "hard_id")
        boot += this[k] + " ";
    }
    return boot;
  },

  states: function () {
    return Etats.findOne({ hard_id: this._id });
  },

  state: function (hard) {
    const devices = hard.devices;
    const state = this.state;
    if (!this.state) return false;
    return state.map(function (e) {
      if (!e.k) return;
      const i = +e.k.split("_")[2];
      if (i === state.length - 2)
        e.k = 'Erreur communication interne';
      else if (i === state.length - 1)
        e.k = 'Erreur communication externe';
      else if (devices && devices[i]) {
        e.k = '<label class="badge badge-default" title="broche">' + devices[i].broche + '</label> ' + devices[i].name;
        let KeyOne = Keys.findOne({ hard_id: hard._id, "device.broche": devices[i].broche });
        e.configCle = '<strong>' + appHd.aliasname(KeyOne) + '</strong>';
      }
      return e;
    });
  },

  stateLine() {
    let state = "";
    for (let key in this) {
      if (key === "k")
        state += '<span class="badge badge-primary">' + this[key] + '</span>' + " ";
      else if (key === "v")
        state += '<span class="badge badge-success">' + this[key] + '</span>' + " ";
    }
    return state;
  },

  hasBugs: function () {
    return Bugs.findOne({ hard_id: this._id });
  },

  bugs: function () {
    return Bugs.find({ hard_id: this._id }, { sort: { createdAt: -1 } });
  },

  bugLine() {
    let bug = "";
    for (let k in this) {
      if (k === "k")
        bug += '<span class="badge badge-primary">' + this[k] + '</span>' + " ";
      else if (k === "v")
        bug += '<span class="badge badge-success">' + this[k] + '</span>' + " ";
    }
    return bug;
  },
});

Template.configurationHardwareLogs.events({

  'click .js-ask-state': function (e) {
    e.preventDefault();

    $(e.target).text('demande en cours');
    Meteor.call('askEtat', this._id, function (err, data) {
      if (err) {
        console.log(err);
        $(e.target).text('error');
      }
      else {
        console.log(data);
        $(e.target).text('demande reçue');
      }
    });
  },
});
