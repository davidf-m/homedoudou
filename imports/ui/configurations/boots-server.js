import Highcharts from 'highcharts';
import Exporting from 'highcharts/modules/exporting';

Exporting(Highcharts);
Highcharts.setOptions({
  global: {
    useUTC: false
  }
});

// NAMESPACE
if(typeof appHd === "undefined") appHd = {};

import './boots-server.html';

Template.configurationBootsServer.helpers({

  breadcrumbLinks() {
    return appHd.buildBreadcrumb(["dashboard", "configuration", "configurationHardware", {link: Router.path("configurationHardwareLogs", {hard_id: this._id}), class: "color-config", title: "Logs " + appHd.aliasname(this)}, "configurationBootsServer"]);
  },

  isTabActive: function (howLongHour) {
    const params = Router.current().params;
    return howLongHour === params.howLongHour * 1 ? 'tabActive' : '';
  },
});

Template.configurationBootsServerGraph.onRendered(function () {
  const instance = this;
  instance.autorun(function () {
    const params = Router.current().params;

    const now = Date.now();
    const dateAfter = now - (params.howLongHour * 60 * 60 * 1000);

    const bootsServer = Boots.find({hard_id: "server", date: {$gt: new Date(dateAfter)}}, {sort: {date: 1}}).fetch().map(function (b) {
      b.date = Date.parse(b.date);
      return b;
    });
    const bootsHard = Boots.find({hard_id: params.hard_id, date: {$gt: dateAfter}}, {sort: {date: 1}}).fetch();

    appHd.bootGraphLaunch(instance.data, params.howLongHour, bootsServer, bootsHard);
  });
});

appHd.bootGraphLaunch = function (hardOne, howLongHour, bootsServer, bootsHard) {

  if(howLongHour === "debug") {
    console.log(this);
  }
  else {
    let volumeServer = [];
    const dataLength = bootsServer.length;

    for(let i = 0; i < dataLength; i++) {
      volumeServer.push([
        parseFloat(bootsServer[i].date),
        parseFloat("1")
      ]);
    }

    let volumeHard = [];
    const data2Length = bootsHard.length;

    for(let i = 0; i < data2Length; i++) {
      volumeHard.push([
        parseFloat(bootsHard[i].date),
        parseFloat("1")
      ]);
    }

    Highcharts.chart('container', {
      chart: {
        type: 'column',
        zoomType: 'x'
      },
      title: {
        text: "Boots"
      },
      xAxis: {
        type: 'datetime',
        labels: {
          rotation: -45,
          style: {
            fontSize: '13px',
            fontFamily: 'Verdana, sans-serif'
          }
        }
      },
      yAxis: {
        title: {
          text: '...'
        }
      },
      legend: {
        enabled: true
      },
      tooltip: {
        pointFormat: '<b>{point.y:.1f}</b>'
      },
      plotOptions: {
        column: {
          pointPadding: 0.2,
          borderWidth: 0
        }
      },
      series: [{
        name: "boots server",
        color: '#FF0000',
        data: volumeServer,
        dataLabels: {
          enabled: true,
          rotation: -90,
          color: '#FFFFFF',
          align: 'right',
          format: '{point.y:.1f}', // one decimal
          y: 10, // 10 pixels down from the top
          style: {
            fontSize: '13px',
            fontFamily: 'Verdana, sans-serif'
          }
        }
      },
        {
          name: "boots hard",
          color: '#0000FF',
          data: volumeHard,
          dataLabels: {
            enabled: true,
            rotation: -90,
            color: '#FFFFFF',
            align: 'right',
            format: '{point.y:.1f}', // one decimal
            y: 10, // 10 pixels down from the top
            style: {
              fontSize: '13px',
              fontFamily: 'Verdana, sans-serif'
            }
          }
        }],
      credits: {
        enabled: false
      }
    });
  }
};