/**
* Created by David FAIVRE-MAÇON  (05 December 2021)
* https://www.davidfm.me
*/

import './mail.html';

Template.configurationNotifsMail.onCreated(function () {

    this.subscribe('sc')
});

Template.configurationNotifsMail.onRendered(function () { });

Template.configurationNotifsMail.helpers({

    mailOnce() {
        return Scenarios.find({ notifiedByMailOnce: true })
    }
});

Template.configurationNotifsMail.events({

    'click .js-reset-mail-once'(event, instance) {
        Scenarios.update({ _id: this._id }, { $set: { notifiedByMailOnce: false } });
    }
});