import './boot.html';

Template.configurationNotifsHardBoot.helpers({

  checked: function (str) {
    return this[str] ? 'checked' : '';
  },

});

Template.configurationNotifsHardBoot.events({

  'click .btn-toggle': function (e) {
    const key = $(e.currentTarget).data('settings');
    const value = !(Meteor.user().settings && Meteor.user().settings[key]);
    Meteor.call('userSettingsUpdate', key, value)
  },
});