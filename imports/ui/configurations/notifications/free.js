import './free.html';

Template.configurationNotifsFree.helpers({

  config: function () {
    const u = Meteor.user();
    return u && u.freeConfig ? u.freeConfig : {};
  }

});

Template.configurationNotifsFree.events({

  'submit': function (e, tpl) {
    e.preventDefault();

    const login = $('[name=login]').val();
    const key = $('[name=key]').val();

    Meteor.call('freeConfigUpdate', login, key);
  },

  'click .js-test': function (e, tpl) {
    e.preventDefault();

    const login = $('[name=login]').val();
    const key = $('[name=key]').val();

    const $btnTest = $(".js-test");
    $btnTest.attr("disabled", "disabled");
    Meteor.call('sendFreeSMS', login, key, '', function (err, result) {
      if(!err) {
        console.log(result);
        $btnTest.removeAttr("disabled");
      }
      else console.log(err)
    });
  },

});