import './web.html';

Template.configurationNotifsWeb.helpers({

  notifPermission() {
    return NotificationsSubs.findOne({"user._id": Meteor.userId()});
  },

  subs() {
    return NotificationsSubs.find({}, {sort: {createdAt: -1}});
  },
});

Template.configurationNotifsWeb.events({

  'click .js-notifs-web': function (e, tpl) {
    e.preventDefault();

    appHd.askForPermissionToReceiveNotifications();
  },

  'click .js-notifs-web-cancel': function (e, tpl) {
    e.preventDefault();

    Meteor.call('hmdNotificationsSubsRemove', Meteor.userId());
  },

  'click .js-push-msg': function (e) {
    e.preventDefault();
    const options = {"title": "test", "body": "Voici les nouveautés !", "eventId": "news"};
    Meteor.call('notificationsPush', this, options);
  },

  'click .js-remove-sub': function (e) {
    e.preventDefault();
    Meteor.call('hmdNotificationsSubsRemove', this.user._id);
  },

});