import './boot.js';
import './mail.js';
import './free.js';
import './web.js';
import './notifications.html';

Template.configurationNotifs.onCreated(function () {
});

Template.configurationNotifs.helpers({

  breadcrumbLinks() {
    return appHd.buildBreadcrumb(["dashboard", "configuration", "configurationNotifs"]);
  },
});

Template.configurationNotifs.events({

  'click .btn-toggle': function (e) {
    const key = $(e.currentTarget).data('settings');
    const value = !(Meteor.user().settings && Meteor.user().settings[key]);
    Meteor.call('userSettingsUpdate', key, value)
  },
});

