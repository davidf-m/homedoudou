import './configuration.html';

Template.configuration.helpers({

  breadcrumbLinks() {
    return appHd.buildBreadcrumb(["dashboard", "configuration"]);
  },

  settings() {
    return (Meteor.user().settings && Meteor.user().settings);
  }
});

Template.configuration.events({

  'click .btn-toggle': function (e) {
    const key = $(e.currentTarget).data('settings');
    const value = !(Meteor.user().settings && Meteor.user().settings[key]);
    log(key, value)
    Meteor.call('userSettingsUpdate', key, value)
  },

  'change input': function (e) {
    const key = $(e.currentTarget).data('settings');
    const value = $(e.currentTarget).val();
    Meteor.call('userSettingsUpdate', key, value)
  }

});