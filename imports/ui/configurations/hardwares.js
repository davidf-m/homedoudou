/**
 * Created by David FAIVRE-MAÇON
 * https://www.davidfm.me
 */

import '../shared/mhz-433-listen-on-off.js';
import './hardwares.html';

Template.configurationHardware.onCreated(function () { });

Template.configurationHardware.onRendered(function () {

  $(function () {
    $('[data-bs-toggle="tooltip"]').tooltip();
  });

  dfm.sortable('.sortable tbody', {
    update: (el) => {
      const list = el.children;
      for (var i = 0; i < list.length; i++) {
        const quiId = $(list[i]).attr('data-sortable-hard_id');
        Meteor.call('orderConfigQui', quiId, i);
      }
    },
    handle: '.handle'
    // axis: 'y',
    // distance: 5,
    // cancel: '.not-dragguable, input'
  });

});

Template.configurationHardware.helpers({

  breadcrumbLinks() {
    return appHd.buildBreadcrumb(["dashboard", "configuration", "configurationHardware"]);
  },

  // breadcrumbLinks: function () {
  //   return [
  //     {link: "/", name: "Accueil"},
  //     {link: "/dashboard", name: "Tableau de Bord"},
  //     {link: Router.path('configuration'), class: "breadcrumb-configuration", name: "Configuration"},
  //     {link: "", class: "breadcrumb-configuration-light", name: "Matériels"}
  //   ];
  // },

  cles() {
    return _.sortBy(this.cles, 'order');
  },

  count(qui, cle) {
    const nbdonnees = Session.get('nbdonnees_' + qui + cle);
    return nbdonnees ? nbdonnees + ' données' : '...';
  },

  isAllHardClosed() {
    return Session.get('qui-open').length === 0;
  }

});

Template.configurationHardware.events({

  'click .showQui': function (e) {
    Hardwares.update({ _id: this._id }, { $set: { show: !this.show } });
  },

  'click .js-toggleShow': function (e) {
    const quiOpen = Session.get('qui-open');

    const i = quiOpen.indexOf(this._id);
    if (i > -1) quiOpen.splice(i, 1);
    else quiOpen.push(this._id);

    Session.set('qui-open', quiOpen)
  },

  'click .js-toggleShowAll': function () {
    if (Session.get('qui-open').length > 0)
      Session.set('qui-open', []);
    else {
      const Quis = Hardwares.find({}, { sort: { order: 1, name: -1 } });
      const tempArr = [];
      Quis.forEach(function (qui) {
        tempArr.push(qui._id)
      });
      Session.set('qui-open', tempArr)
    }
  },

  'click .js-send-box': function (e) {
    //FIXME verifier la version arduino, a partir de quelle version ça marche?

    /* Configuration box jeedom, domoticz*/
    const hard = this;
    const boxData = $(e.currentTarget).data('box');

    let todo = [];
    let boxUsed = 0;

    if (boxData === 'jeedom') {
      todo = ['jeedom'];
      boxUsed = 2;
    }
    if (boxData === 'domoticz') {
      todo = ['domoticz'];
      boxUsed = 1;
    }
    if (boxData === 'les-deux') {
      todo = ['jeedom', 'domoticz'];
      boxUsed = 3;
    }

    for (let i = 0; i < todo.length; i++) {
      const box = todo[i];
      /* envoie raz box*/
      let command = {
        mode: box,
        cle: 'raz',
        valeur: 'yo'
      };
      Meteor.call('talkToArduino', hard._id, command, function (err, result) {
        if (err) console.log(err);
      });

      //Seulement si vert...
      let hasConfig = null;
      if (box === 'jeedom') {
        hasConfig = Keys.findOne({ hard_id: hard._id, $and: [{ jeedomId: { $exists: true } }, { jeedomId: { $ne: "" } }] });
        if (!hasConfig) {
          boxUsed -= 2;
        }
      }
      else if (box === 'domoticz') {
        hasConfig = Keys.findOne({ hard_id: hard._id, $and: [{ domoticzId: { $exists: true } }, { domoticzId: { $ne: "" } }] });
        if (!hasConfig) {
          boxUsed -= 1;
        }
      }
      if (hasConfig) {
        /* envoie identifiant box*/
        const boxId = Meteor.user().settings[box + "UserId"];
        if (!boxId) appHd.alert("Pour information, votre identifiant utilisateur box n'est pas rentré");
        command = {
          mode: box,
          cle: 'user_id',
          valeur: boxId
        };
        Meteor.call('talkToArduino', hard._id, command, function (err, result) {
          if (err) console.log(err);
        });

        /* envoie ip box*/
        const boxIp = Meteor.user().settings[box + "BoxIp"];
        if (!boxIp) return alert("D'abord vous devez rentrer votre ip de box");
        command = {
          mode: box,
          cle: 'box_ip',
          valeur: boxIp
        };
        Meteor.call('talkToArduino', hard._id, command, function (err, result) {
          if (err) console.log(err);
        });

        /* envoie périphériques box*/
        const devices = Keys.find({ hard_id: hard._id });
        devices.forEach(function (d) {
          if (d[box + "Id"]) {
            const value = d[box + "Id"];
            if (value) {
              const command = {
                mode: box,
                cle: d.device.index,
                valeur: d[box + "Id"]
              };

              Meteor.call('talkToArduino', hard._id, command, function (err, result) {
                if (err) console.log(err);
              });
            }
          }
        });
      }
    }

    /* Envoie du nombre d'utilisation de box 0:aucune, 1:domoticz 2:Jeedom 3:tout */
    const command = {
      mode: 'box',
      cle: 'use',
      valeur: boxUsed
    };
    Meteor.call('talkToArduino', hard._id, command, function (err, result) {
      if (err) console.log(err);
    });
  },
});

Template.configurationHardwareItem.helpers({

  openQui() {
    const quiOpen = Session.get('qui-open');
    const i = quiOpen.indexOf(this._id);
    return i > -1;
  },

  hasType(type_id) {
    if (!this.devices) return false;
    return appHd.arrayObjectIndexOf(this.devices, type_id, 'type_id') > -1;
  },

  keyCount() {
    return Keys.find({ hard_id: this._id }).count();
  },

  hasConfJeedom() {
    const hasConfig = Keys.findOne({ hard_id: this._id, $and: [{ jeedomId: { $exists: true } }, { jeedomId: { $ne: "" } }] });
    return hasConfig ? 'btn-primary' : 'btn-default';
  },

  hasConfDomoticz() {
    const hasConfig = Keys.findOne({ hard_id: this._id, $and: [{ domoticzId: { $exists: true } }, { domoticzId: { $ne: "" } }] });
    return hasConfig ? 'btn-primary' : 'btn-default';
  },
});

Template.configurationHardwareItem.events({});
