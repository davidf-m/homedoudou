import './ping.html';
Template.arduinoPing.helpers({

  breadcrumbLinks() {
    return appHd.buildBreadcrumb(["dashboard", "configuration"]);
  },

  // breadcrumbLinks: function () {
  //   return [
  //     {link: Router.path('configuration'), class: "breadcrumb-configuration", name: "Configuration"},
  //     {link: "/configuration/hardware", name: "Matériels"},
  //     {link: "", name: "Ping"}
  //   ];
  // },

  pingDevice: function () {
    let pi = 0;
    return this.devices.map(function (d, i) {
      if(d.type_id === 185) pi++;
      d.pingIndex = pi;
      d.index = i;
      return d;
    });
  }

});

Template.arduinoPing.events({

  'change [name=ip], keyup [name=ip]': function (e, tpl) {
    if(e.type === 'keyup' && e.keyCode !== 13) return;

    const ip = tpl.$(e.currentTarget).val();
    Meteor.call('adping.update.ip', this.parent._id, this.context.index, ip, function (err) {
      if(!err) {
        tpl.$(e.currentTarget).blur();
      }
    })
  },

  'change [name=disableNotifications]': function (e, tpl) {
    e.preventDefault();

    const disableNotifications = e.target.checked;
    Meteor.call('adping.update.disableNotifications', this.parent._id, this.context.index, disableNotifications, function (err) {
      if(!err) {
        tpl.$(e.currentTarget).blur();
      }
    })
  }

});

Template.arduinoPingDevice.onRendered(function () {
});

Template.arduinoPingDevice.helpers({

  checked: function (str) {
    return this.context[str] ? 'checked' : '';
  },

});