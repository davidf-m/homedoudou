/**
 * Created by David on 18/05/2015.
 */

import './hardware-devices.html';

Template.configurationHardwareDevices.onCreated(function () {

  Meteor.call('orphelinsSameIp', function (err, orphelinsSameIp) {
    Session.set("orphelinsSameIp", orphelinsSameIp)
  });

  const Hard = Hardwares.findOne(this.data.hard_id);
  if (Hard && Hard.devices)
    Session.set('devicesToAdd', Hard.devices);
  else
    Session.set('devicesToAdd', []);

  Session.set("addhardware-step", '1');
});

Template.configurationHardwareDevices.onRendered(function () {
});

Template.configurationHardwareDevices.helpers({

  breadcrumbLinks() {
    return appHd.buildBreadcrumb(["dashboard", "configuration", "configurationHardware", "configurationHardwareDevices"]);
  },

  // breadcrumbLinks: function () {
  //   return [
  //     {link: "/", name: "Accueil"},
  //     {link: "/dashboard", name: "Tableau de Bord"},
  //     {link: Router.path('configuration'), class: "breadcrumb-configuration", name: "Configuration"},
  //     {link: Router.path('configurationHardware'), class: "breadcrumb-configuration", name: "Matériels"},
  //     {link: "", class: "breadcrumb-configuration-light", name: "Périphériques " + appHd.aliasname(this)}
  //   ];
  // },

  types: function () {
    return Device.find({}, { sort: { 'type_id': 1 } })
  },

  live: function () {
    return this.live ? 'live' : ''
  },

  'devices': function () {
    return Session.get('devicesToAdd')
  },

  child: function () {
    return this.child ? 'enfant' : null
  }
});

Template.configurationHardwareDevices.events({

  'click .js-add-device': function (e) {
    e.preventDefault();

    const device = {};
    device.broche = $('[name="broche"]').val();
    device.type_id = parseInt($('[name="type"]').val());
    device.name = $('[name="type"] option:selected').text();
    device.live = $('[name="live"]').prop('checked');
    if (device.live)
      device.type_id += 100;

    const devices = Session.get('devicesToAdd');

    const same = devices.some(function (el) {
      return el.broche === device.broche;
    });

    //if (!same) { FIXME sonde am autorisé
    $('[name="broche"]').val('');
    $('[name="live"]').prop('checked', false);

    devices.push(device);
    Session.set('devicesToAdd', devices)
    //}

  },

  'click .js-add-device-child': function (e) {
    e.preventDefault();

    const devices = Session.get('devicesToAdd');

    const newDevice = {};
    newDevice.broche = parseInt($('[name="broche"]').val());
    newDevice.type_id = parseInt($('[name="type"]').val());
    newDevice.name = $('[name="type"] option:selected').text();
    newDevice.child = true;
    newDevice.live = false;

    const newDevices = [];
    const that = this;
    _.each(devices, function (device) {
      newDevices.push(device);
      if (device.broche === that.broche && device.type_id === that.type_id) {
        if (device.live) {
          // newDevice.type_id += 100;
          newDevice.type_id = 9999;
          newDevice.live = true;
        }
        newDevices.push(newDevice);
        console.log('add')
      }
    });
    console.log(newDevices)
    Session.set('devicesToAdd', newDevices);
  },

  'click .js-delete': function (e, tpl) {
    e.preventDefault();

    const that = this;
    let devices = Session.get('devicesToAdd');
    devices.splice(this.i, 1);
    Session.set('devicesToAdd', devices)
  },

  'click .js-validation': function (e) {
    e.preventDefault();

    $('.js-validation').remove();
    $('#wait').html('<span class="badge badge-warning">Patientez...</span>');

    const maj_id = $('input[name=maj_id]').prop('checked');
    const devices = Session.get('devicesToAdd');

    const that = this;

    Meteor.call('hardwareDevicesAdd', this, maj_id, devices, function (err, result) {
      if (err) {
        console.log(err);
        $('#wait').html('<span class="badge badge-danger">Erreur : ' + err.message)
      }
      else {
        console.log(result);
        if (result && result.ok === 1) {
          //FIXME do it server side and remove allow
          Hardwares.update(that._id, { $set: { devices: devices } });
          Router.go('configurationHardware')
        }
        else
          $('#wait').html('<span class="badge badge-danger">Erreur : ' + result.ok)
      }
    })

  },

  'change [name="comment"]'(e) {
    const comment = e.target.value;
    Meteor.call('hardwareBrocheCommentaireAdd', { hardId: this.hard._id, broche: this.device.broche, comment });
  }

});
