import './hardware-edit.html';

Template.configurationHardwareEdit.onCreated(function () {
  if (!this.data) Router.go('configurationHardware')
  Session.set('shared', this.data?.shared || []);
});

Template.configurationHardwareEdit.helpers({

  breadcrumbLinks() {
    return appHd.buildBreadcrumb(["dashboard", "configuration", "configurationHardware", "configurationHardwareEdit"]);
  },

  // breadcrumbLinks: function () {
  //   return [
  //     {link: "/", name: "Accueil"},
  //     {link: "/dashboard", name: "Tableau de Bord"},
  //     {link: Router.path('configuration'), class: "breadcrumb-configuration", name: "Configuration"},
  //     {link: Router.path('configurationHardware'), class: "breadcrumb-configuration", name: "Matériels"},
  //     {link: "", class: "breadcrumb-configuration-light", name: appHd.aliasname(this)}
  //   ];
  // },

  shared: function () {
    return Session.get('shared');
  }

});

Template.configurationHardwareEdit.events({

  'click .js-delQui': function (e) {
    e.preventDefault();

    const that = this;
    dfm.confirm('Supprimer cet appareil?', function (result) {
      if (result) {
        console.log(that);
        Meteor.call('hardDeleteById', that._id, function (err) {
          if (err) console.log(err)
          Router.go('configurationHardware')
        });
      }
    });
  },

  'click .shared-add-item': function (e) {
    e.preventDefault();

    const shared = Session.get('shared');
    shared.push({});
    Session.set('shared', shared);
  },

  'click .shared-remove-item': function (e) {
    e.preventDefault();

    const index = $(e.currentTarget).data('value');
    const shared = Session.get('shared');
    shared.splice(index, 1);
    Session.set('shared', shared);
  },

  'keyup input, change select': function (e, tpl) {
    tpl.$('#submit').text(tpl.formSubmitText);
    tpl.$('#submit').removeClass('btn-primary');
    tpl.$('#submit').addClass('btn-warning');
  },

  'submit': function (e, tpl) {
    e.preventDefault();

    tpl.formSubmitText = tpl.$('#submit').text();
    const form = tpl.$('form').serializeJSON();

    if (!form.shared) form.shared = [];
    Meteor.call('hardUpdate', form, function (err) {
      if (!err) {
        tpl.$('#submit').text('Sauvegardé');
        Router.go('configurationHardware')
      }
      else {
        console.log(err);
      }
    });
  },

});


/**
* Created by David FAIVRE-MAÇON  (29 September 2022)
* https://www.davidfm.me
*/

Template.configurationHardwareItemOpen.onCreated(function () { });

Template.configurationHardwareItemOpen.onRendered(function () {

  const hard = this.data;
  dfm.sortable('.sortable', {
    update: (el) => {
      const hard_id = hard._id;
      const list = el.children;
      for (let i = 0; i < list.length; i++) {
        const key = $(list[i]).attr('data-sortable-key');
        Meteor.call('orderConfigCle', hard_id, key, i);
      }
    },
    handle: '.handle'
    // axis: 'y',
    // distance: 5,
    // cancel: '.not-dragguable, input'
  });
});

Template.configurationHardwareItemOpen.onDestroyed(function () { });

Template.configurationHardwareItemOpen.helpers({

  hasKey() {
    return !!Keys.findOne({ hard_id: this._id, hidden: { $ne: true } });
  },

  keys(hard_id) {
    return Keys.find({ hard_id: hard_id, hidden: { $ne: true } }, { sort: { order: 1 } });
  },
});

Template.configurationHardwareItemOpen.events({

  'click .showCle': function () {
    Meteor.call('keySwitchShow', this._id);
  },

  'click .js-public': function () {
    Meteor.call('keySwitchPublic', this._id);
  },

  'click .js-notified': function () {
    Meteor.call('keySwitchNotified', this._id);
  },

  'click .js-key-delete'(e) {

    const that = this;
    dfm.confirm('Supprimer cette clé et toutes ses données?', function (result) {
      if (result) {
        Meteor.call('keyDeleteById', that._id, function (err) {
          if (err) dfm.toastError(err)
          else dfm.toast('key deleted')
        });
      }
    }, e.shiftKey);

  },

  'click .js-change-style-inc'(e) {

    let i = appHd.arrayObjectIndexOf(appHd.keyStyles, this.style, 'value');
    i++;
    if (i >= appHd.keyStyles.length) i = 0;
    Meteor.call('keys.update', this._id, { style: appHd.keyStyles[i].value })
  },

  'click .js-change-type-inc'(e) {

    let i = appHd.arrayObjectIndexOf(appHd.keyTypes, this.type, "type");
    i++;
    if (i >= appHd.keyTypes.length) i = 0;
    Meteor.call('keys.update', this._id, { type: appHd.keyTypes[i].type })
  },
});