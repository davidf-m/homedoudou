import './scrapers.html';

Template.configurationScrapers.onCreated(function () {
});

Template.configurationScrapers.onRendered(function () {


});

Template.configurationScrapers.helpers({

  breadcrumbLinks() {
    return appHd.buildBreadcrumb(["dashboard", "configuration", "configurationScrapers"]);
  },

  scrapers() {
    return Scrapers.find();
  },

  isEditing() {
    return Session.get('scrappers-edit') === this._id
  }
});

Template.configurationScrapers.events({

  'click .js-scrapers-send': function (e) {
    Meteor.call('scrapersGet', this._id, function (err, result) {
      if(err) return console.log(err);
    });
  },

  'click .js-scrapers-add': function (e) {
    $('#scraper-add').removeClass('hidden');
  },

  'click .js-scrapers-edit': function (e) {
    Session.set('scrappers-edit', this._id)
  },

  'click .js-scrapers-delete': function (e) {
    Meteor.call('scrapersRemove', this._id, function (err, result) {
      if(err) return console.log(err);
    });
  },

  'submit #add'(e, tpl) {
    e.preventDefault();

    const form = tpl.$('form#add').serializeJSON();
    Meteor.call('scrapers.insert', form, function (err, result) {
      if(err) return console.log(err);
      else {
        $('#scraper-add').addClass('hidden');
      }
    });
  },

  'submit #edit'(e, tpl) {
    e.preventDefault();

    const form = tpl.$('form#edit').serializeJSON();
    Meteor.call('scrapers.update', this._id, form, function (err, result) {
      if(err) return console.log(err);
      else {
        Session.set('scrappers-edit', null)
      }
    });
  }

});
