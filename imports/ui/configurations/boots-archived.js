import './boots-archived.html';

Template.configurationBootsArchived.onCreated(function () {
});

Template.configurationBootsArchived.helpers({

  breadcrumbLinks() {
    return appHd.buildBreadcrumb([
      "dashboard",
      "configuration",
      "configurationHardware",
      { link: Router.path("configurationHardwareLogs", { hard_id: this._id }), class: "color-config", title: "Logs " + appHd.aliasname(this) },
      { class: "color-config", title: "Boots archivés" }
    ]);
  },

  boots: function () {
    return Boots.find({ hard_id: this._id }, { sort: { date: -1 } });
  },

  bootLine() {
    // const bootLine = JSON.stringify(this)
    let boot = "";
    for (let k in this) {
      if (k === "cle")
        boot += '<span class="badge badge-primary">' + this[k] + '</span>' + " ";
      else if (k === "valeur")
        boot += '<span class="badge badge-success">' + this[k] + '</span>' + " ";
      else if (k === "date")
        boot += dfm.formatDate({ date: this[k]}) + " ";
      else if (k === "ip")
        boot += '<span class="badge badge-warning">' + this[k] + '</span>' + " ";
      else if (k !== "hard_id")
        boot += this[k] + " ";
    }
    return boot;
  },

  isArchived: function () {
    return this.archived;
  },

  alias: function () {
    const Hard = Hardwares.findOne(this.hard_id);
    if (Hard && Hard.alias)
      return Hard.alias;
    return 'John Doe';
  }

});

Template.configurationBootsArchived.events({

  'click .js-del-boot': function () {
    Meteor.call('bootRemove', this._id);
  }

});
