import './hardware-key-edit.html';

Template.configurationHardwareKeyEdit.onCreated(function () {
});

Template.configurationHardwareKeyEdit.onRendered(function () {
  $('[data-bs-toggle="tooltip"]').tooltip();
});

Template.configurationHardwareKeyEdit.helpers({

  breadcrumbLinks() {
    // const hard = Hardwares.findOne({_id: this.hard_id});
    return appHd.buildBreadcrumb(["dashboard", "configuration", "configurationHardware", "configurationHardwareKeyEdit"]);
  },

  // breadcrumbLinks: function () {
  //   const hard = Hardwares.findOne({_id: this.hard_id});
  //   return [
  //     {link: "/", class: "", title: "Accueil"},
  //     {link: "/dashboard", class: "", title: "Tableau de Bord"},
  //     {link: Router.path('configuration'), class: "breadcrumb-configuration", title: "Configuration"},
  //     {link: "/configuration/hardware", class: "breadcrumb-configuration", title: "Matériels"},
  //     {link: "/configuration/hardware/" + this.hard_id, class: "breadcrumb-configuration", title: appHd.aliasname(hard)},
  //     {link: "", class: "breadcrumb-configuration-light", title: appHd.aliasname(this)}
  //   ];
  // },

  cle: function () {
    return Keys.findOne({hard_id: this.hard_id, name: this.name});
  },

  iskWhHCPriced: function () {
    return this.device?.type_id === 77 && (this.keyName === 'HCHP' || this.keyName === 'HCHC' || this.keyName === 'BASE');
  }

});

Template.configurationHardwareKeyEdit.events({

  'click .js-remove': function (e) {
    e.preventDefault();

    const key = this;
    if(confirm("Sur de vouloir éffacer?")) {
      Meteor.call('keyDeleteById', key._id, function (err) {
        if(!err) Router.go('configurationHardware')
      });
    }
  },

  'submit': function (e, tpl) {
    e.preventDefault();

    tpl.formSubmitText = tpl.$('#submit').text();
    const form = tpl.$('form').serializeJSON();
    console.log(form);
    Meteor.call('keys.update', form._id, form, function (err) {
      if(!err) {
        tpl.$('#submit').text('Sauvegardé');
        Router.go('configurationHardware')
      }
      else {
        console.log(err);
      }
    });
  },

});
