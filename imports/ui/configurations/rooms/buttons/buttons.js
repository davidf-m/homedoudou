import './button-edit.js';
import './buttons.html';

Template.configurationRoomsButtons.onRendered(function () {

  dfm.sortable('#tab-buttons .sortable', {
    update: (el) => {
      _.each(el.children, function (child, newOrder) {
        newOrder = el.children.length - newOrder;
        const buttonId = $(child).attr('data-sortable-button_id');
        Meteor.call('buttons.update', buttonId, { order: newOrder });
      })
    },
    // axis: 'y',
    // distance: 5,
    // cancel: 'input, select'
  });
});

Template.configurationRoomsButtons.helpers({

  isEditing() {
    return appHd.editingRoom.get(this._id) === this._id;
  },

  getRoomName(id) {
    const room = appHd.getRoom(id);
    return room ? room.name : '';
  },
});

Template.configurationRoomsButtons.events({

  'click .js-button-edit'() {
    //FIXME why this reactiveVar?
    appHd.editingRoom.set(this._id)
  },

  'click .js-delete'(e, tpl) {
    Meteor.call('buttons.remove', this._id);
    return appHd.editingRoom.set(false);
  }
});