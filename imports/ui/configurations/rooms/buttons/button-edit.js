import './button-edit.html';

function formatState(state) {
  log(state)
  log(state.text)
  if (state.text === 'choose') {
    return "-- Choisir icon --";
  }
  else if (state.text === 'chauffage') {
    return $('<div><span class="material-icons-outlined">device_thermostat</span> chauffage</div>');
  }
  else if (state.text === 'lumière') {
    return $('<div><span class="material-icons-outlined">emoji_objects</span> lumière</div>');
  }
  return $('<span class="material-icons-outlined">' + state.text + '</span> gg');
};

Template.configurationRoomButtonEdit.onCreated(function () {
});

Template.configurationRoomButtonEdit.onRendered(function () {
  this.$('[name=on]').select2();
  this.$('[name=off]').select2();
  this.$('[name=backId]').select2();
  this.$('[name=icon]').select2({
    templateResult: formatState
  });
  this.$('[name=displayedKey]').select2();
  this.$('select.rooms').select2({
    placeholder: "Groupe",
  })
});

Template.configurationRoomButtonEdit.helpers({

  breadcrumbLinks() {
    return appHd.buildBreadcrumb(["dashboard", "configuration", "configurationRooms", { title: this.room.name, link: Router.path("configurationRoomEdit", { _id: this.room._id }), class: "color-config" }, { title: this.button.name, class: "color-config" }]);
  },

  buttonId() {
    const params = Router.current().params;
    return params.buttonId;
  },

  numberOfActions() {
    console.log(this.button);
    const e = [];
    for (let i = 0; i < this.button.numberOfActions; i++) {
      e.push({});
    }
    return e;
  },

  isActionSelected(attr) {
    console.log(attr.hash)
  }
});

Template.configurationRoomButtonEdit.events({

  'change input, change select'(e, tpl) {
    tpl.$('.js-submit').addClass('btn-warning');
  },

  'change select[name=type]'(e, tpl) {
    Meteor.call('buttons.update', this.button._id, { type: e.currentTarget.value }, function () {
      $('[name=on]').select2();
      $('[name=off]').select2();
    });
  },

  'change input[name=numberOfActions]'(e, tpl) {
    Meteor.call('buttons.update', this.button._id, { numberOfActions: e.currentTarget.value }, function () { });
  },

  'click .js-submit'(e, tpl) {
    const ctx = tpl.data;
    const form = tpl.$('#form-' + ctx.button._id).serializeJSON();
    Meteor.call('buttons.update', ctx.button._id, form, function (err) {
      if (err) return appHd.logsError('buttons.update');
      return Router.go('configurationRoomEdit', { _id: ctx.room._id });
    });
  },

  'click .js-delete'(e, tpl) {
    const ctx = tpl.data;
    Meteor.call('buttons.remove', ctx.button._id, function (err) {
      if (err) return appHd.logsError('buttons.remove');
      else Router.go('configurationRoomEdit', { _id: ctx.room._id });
    });
  }
});

// Template.configurationRoomButtonEdit.onDestroyed(function () {
//   this.$('[name=backId]').select2('destroy');
// });

// SELECT

Template.configurationRoomsSelect.helpers({

  getRoomName(id) {
    const room = appHd.getRoom(id);
    return room ? room.name : '';
  },

  isEditing() {
    return appHd.editingRoom.get() === this._id;
  },
});

Template.configurationRoomsSelect.events({

  'click .js-edit'() {
    appHd.editingRoom.set(this._id)
  }
});

// SELECT EDITING

Template.configurationRoomsSelectEditing.onRendered(function () {
  this.$('select').select2({
    placeholder: "Groupe",
  })
});

Template.configurationRoomsSelectEditing.onDestroyed(function () { });

Template.configurationRoomsSelectEditing.events({});