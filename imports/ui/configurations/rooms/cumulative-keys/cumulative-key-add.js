import './cumulative-key-add.html';

const line = new ReactiveVar([]);
let index = 0;
Template.configurationRoomsCumulativeKeyAdd.onCreated(function () {
  line.set([]);
});

Template.configurationRoomsCumulativeKeyAdd.helpers({

  getHardId() {
    return line.get();
  }
});
Template.configurationRoomsCumulativeKeyAdd.events({

  'click .js-key-add'(e, tpl) {
    const lines = line.get();
    const cumuls = { roomsId: [this._id], lines };
    Meteor.call('cumulativeKeyInsertRoom', cumuls, function (err, buttonId) {
      if (!err) {
        line.set([]);
      }
    })
  },

  'change [name=hardId]'(e, tpl) {
    const lines = line.get();

    const i = $(e.currentTarget).data('index');
    if (i !== undefined) {
      lines[i].hard_id = $(e.currentTarget).val();
    }
    else {
      lines.push({ index: index, hard_id: $(e.currentTarget).val() });
      index++;
    }
    line.set(lines);
    // restore the select
    $(e.currentTarget).val('');
  },

  'change [name=keyId]'(e, tpl) {
    const lines = line.get();
    const i = $(e.currentTarget).data('index');
    lines[i].key_id = $(e.currentTarget).val();
    line.set(lines);
    console.log(lines);
  },
});