/**
 * Created by DavidFM on 04 fébuary 2021
 *
 */

import './cumulative-key-add.js';
import './cumulative-keys.html';
import { ReactiveVar } from "meteor/reactive-var";

// const get = new ReactiveVar();
Template.configurationRoomsCumulativeKeys.onCreated(function () {

  // const self = this;
  // this.autorun(function() {
  //   const data = Template.currentData();
  //   if(!data) return;
  //   self.subscribe('xxx', {serviceId: data._id});
  // });
});

Template.configurationRoomsCumulativeKeys.onRendered(function () {
});

Template.configurationRoomsCumulativeKeys.helpers({

  cumulKeysByRoomId(roomId) {
    return CumulKeys.find({ roomsId: roomId, ownerId: Meteor.userId() })
  },

  isEditing() {
    return appHd.editingRoom.get(this._id) === this._id;
  }
});

Template.configurationRoomsCumulativeKeys.events({

  'click .js-edit'() {
    appHd.editingRoom.set(this._id)
  },

  'click .js-remove'() {
    dfm.confirm('sure?', (res) => {
      if (res) {
        Meteor.call('cumulativeKeyRemoveRoom', this._id, function (err) {
          if (err) dfm.toastError(err)
        })
      }
    })
  },

  'click .js-submit'(e, tpl) {
    const form = tpl.$('#form_' + this._id).serializeJSON();
    log(this)
    log(form.rooms)
    // Meteor.call('rooms.set', this._id, form.rooms); // FIXME do not work
    appHd.editingRoom.set(false);
    tpl.$('select').select2('destroy');
  }
});