/**
 * Created by David FAIVRE-MAÇON on 05/08/2020
 * https://www.davidfm.me
 */

import './scenario-add.js';
import './scenarios.html';

Template.configurationRoomsScenarios.onCreated(function() {
});

Template.configurationRoomsScenarios.onRendered(function() {
});

Template.configurationRoomsScenarios.helpers({

  isEditing() {
    return appHd.editingRoom.get(this._id) === this._id;
  },

  eventName: function (id) {
    const s = Scenarios.findOne(id);
    return s ? s.name : '<span class="badge badge-danger">error</span>';
  },

  actionName: function (id) {
    return Scenarios.findOne(id)?.name;
  },
});

Template.configurationRoomsScenarios.events({

  'click .js-edit'() {
    appHd.editingRoom.set(this._id)
  },

  'click .js-submit'(e, tpl) {
    const form = tpl.$('#form-' + this._id).serializeJSON();
    Meteor.call('rooms.scenario.set', this._id, form.rooms);
    return appHd.editingRoom.set(false);
  }
});