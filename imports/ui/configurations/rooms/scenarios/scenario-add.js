/**
 * Created by David FAIVRE-MAÇON on 05/08/2020
 * https://www.davidfm.me
 */

import './scenario-add.html';

Template.configurationRoomsScenarioAdd.onCreated(function() {
});

Template.configurationRoomsScenarioAdd.onRendered(function() {
});

Template.configurationRoomsScenarioAdd.helpers({
});

Template.configurationRoomsScenarioAdd.events({

  'click .js-scenario-add'(e, tpl) {
    const form = tpl.$('#rooms-scenario-add').serializeJSON();

    Meteor.call('scenarios.push.room', form.scenarioId, this._id, function (err, buttonId) {
      if(!err) {
        tpl.$('#rooms-scenario-add')[0].reset();
      }
    })
  },
});