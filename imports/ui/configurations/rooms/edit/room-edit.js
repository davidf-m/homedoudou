import '../buttons/buttons.js';
import '../keys/keys.js';
import '../scenarios/scenarios.js';
import '../cumulative-keys/cumulative-keys.js';

import './room-edit.html';

Template.configurationRoomEdit.onCreated(function () {
});

Template.configurationRoomEdit.onRendered(function () {
});

Template.configurationRoomEdit.onDestroyed(function () {
  log('Template.configurationRoomEdit.onDestroyed')
});

Template.configurationRoomEdit.helpers({

  breadcrumbLinks() {
    return appHd.buildBreadcrumb(["dashboard", "configuration", "configurationRooms", this.name]);
  },

  room() {
    return Rooms.findOne(this._id);
  }
});

Template.configurationRoomEdit.events({

  'click .js-button-add'() {
    const form = {
      rooms: [this._id]
    };
    Meteor.call('buttons.insert', form, function (err, buttonId) {
      if (err) return appHd.logsError('buttons.insert ');
      else Router.go('configurationRoomButtonEdit', { roomId: form.rooms[0], buttonId: buttonId })
    })
  },
});