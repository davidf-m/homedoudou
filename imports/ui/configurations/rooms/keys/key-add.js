import './key-add.html';

const hardId = new ReactiveVar(false)
Template.configurationRoomsKeyAdd.onCreated(function () {
  hardId.set(false);
});

Template.configurationRoomsKeyAdd.helpers({

  getHardId() {
    return hardId.get();
  }
});
Template.configurationRoomsKeyAdd.events({

  'click .js-key-add'(e, tpl) {
    const form = tpl.$('#rooms-key-add').serializeJSON();
    console.log(form, this);

    Meteor.call('keyPushRoom', form.keyId, this._id, function (err, buttonId) {
      if(!err) {
        tpl.$('#rooms-key-add')[0].reset();
      }
    })
  },

  'change [name=hardId]'(e, tpl) {
    hardId.set($(e.currentTarget).val());
  },
});