import './key-add.js';
import './keys.html';

Template.configurationRoomsKeys.onRendered(function () {
  dfm.sortable('.table.configuration-rooms-keys .sortable', {
    update: (el) => {
      _.each(el.children, function (child, newOrder) {
        // console.log($(child));
        const keyId = $(child).attr('data-sortable-key_id');
        // console.log(keyId);
        Meteor.call('keys.update', keyId, { order: newOrder });
      })
    },
    // axis: 'y',
    // distance: 5,
    // cancel: 'input, select'
  });
});

Template.configurationRoomsKeys.onDestroyed(function () { });

Template.configurationRoomsKeys.helpers({

  isEditing() {
    return appHd.editingRoom.get(this._id) === this._id;
  }
});

Template.configurationRoomsKeys.events({

  'click .js-edit'() {
    appHd.editingRoom.set(this._id)
  },

  'click .js-remove'() {
    dfm.confirm('sure?', (res) => {
      if (res) {
        Meteor.call('keyPullRoom', this.keyId, this.roomId, function (err) {
          if (err) dfm.toastError(err)
        })
      }
    })
  },

  'click .js-submit'(e, tpl) {
    const form = tpl.$('#form-' + this._id).serializeJSON();

    Meteor.call('rooms.set', this._id, form.rooms);
    appHd.editingRoom.set(false);
    tpl.$('select').select2('destroy');
  }
});