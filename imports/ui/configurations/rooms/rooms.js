import './room.js';
import './actions.js';

import './rooms.html';

Template.configurationRooms.onCreated(function () {
});

Template.configurationRooms.onRendered(function () {

  Session.set('configurationRoomsRoomEditing', false);

  dfm.sortable('.sortable', {
    items: "tr:not(.not-dragguable)",
    update: function (el) {
      const orders = [];
      const list = el.children;
      // En deux fois car reactif.
      _.each(list, function (room, newOrder) {
        const roomId = $(room).attr('data-sortable-room_id');
        orders.push({ roomId: roomId, order: newOrder });
      });
      orders.forEach(function (o) {
        Meteor.callAsync('rooms.update', o.roomId, { order: o.order });
      })
    },
    handle: '.handle',
    axis: 'y',
    distance: 5,
    placeholder: "ui-state-highlight",
    cancel: '.not-dragguable, input, select'
  });
});

Template.configurationRooms.helpers({

  breadcrumbLinks() {
    return appHd.buildBreadcrumb(["dashboard", "configuration", "configurationRooms"]);
  },
  //
  // configCle: function () {
  //   return Keys.find({hard_id: this._id, hidden: {$ne: true}});
  // },
  //
  // scenarios: function () {
  //   return Scenarios.find({scenarioType: "scenario"});
  // },

});

Template.configurationRooms.events({

  'click .js-add-new-room, keyup [name=new-room]'(e, tpl) {
    if (e.type === 'keyup' && e.keyCode !== 13) return;

    const name = tpl.$('[name=new-room]').val();
    if (!name) {
      tpl.$('[name=new-room]').focus();
      return;
    }

    Meteor.call('rooms.insert', name, function (err) {
      if (!err) {
        tpl.$('[name=new-room]').val('');
      }
    })
  },
});