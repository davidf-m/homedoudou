import './actions.html';

Template.configurationRoomsActions.onRendered(function () {
  dfm.sortable('#tab-actions .sortable', {
    update: function (el) {
      _.each(el.children, function (action, newOrder) {
        // console.log($(action));
        const actionId = $(action).attr('data-sortable-action_id');
        // console.log(actionId);
        Meteor.call('scenarios.update', actionId, { order: newOrder });
      })
    },
    axis: 'y',
    distance: 5,
    cancel: 'input'
  });
});

Template.configurationRoomsActions.helpers({

  isEditing() {
    return appHd.editingRoom.get(this._id) === this._id;
  },
});

Template.configurationRoomsActions.events({

  'click .js-edit'() {
    appHd.editingRoom.set(this._id)
  },

  'click .js-submit'(e, tpl) {
    const form = tpl.$('#form-' + this._id).serializeJSON();
    Meteor.call('rooms.scenario.set', this._id, form.rooms);
    return appHd.editingRoom.set(false);
  }
});