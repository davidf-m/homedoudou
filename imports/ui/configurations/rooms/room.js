/**
 * Created by David FAIVRE-MAÇON on 05/08/2020
 * https://www.davidfm.me
 */

import './room.html';

Template.configurationRoomsRoom.onCreated(function () {
});

Template.configurationRoomsRoom.onRendered(function () {
  this.$('.select-shared').select2();
});

Template.configurationRoomsRoom.helpers({

  myContacts() {
    //FIXME TODO
    if (dfm.isWebmaster())
      return Meteor.users.find();
    else
      return [];
  },
});

Template.configurationRoomsRoom.events({

  'click .room-show': function (e) {
    Meteor.call('rooms.show', this, function (err) {
      if (err) appHd.logsError('rooms.show')
    })
  },

  'change .select-shared'(e, tpl) {

    const shared = $(e.currentTarget).val();
    Meteor.call('rooms.shared', this._id, shared, function (err) {
      if (err) appHd.logsError('rooms.shared')
    })
  },

  'click .js-remove': function (e, tpl) {
    const that = this;
    dfm.confirm('Supprimer ?', function (result) {
      if (result) {
        Meteor.call('rooms.remove', that._id, function (err) {
        })
      }
    });
  },

  'change [name=room]'(e, tpl) {
    const name = tpl.$(e.currentTarget).val() || tpl.$(e.currentTarget).parent().parent().find('[name=room]').val();

    Meteor.callAsync('rooms.update', this._id, { name: name })
      .then(res => {
        dfm.toastSuccess('Room updated')
      })
      .catch(err => {
        dfm.logError(JSON.stringify(err))
      })
  },
});