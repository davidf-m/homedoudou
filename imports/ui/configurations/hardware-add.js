import DfmConnections from 'meteor/davidfm:core/api/accounts/lib/collections.js';
import './hardware-add.html';

/**
 * Created by David on 18/05/2015.
 */

Template.configurationHardwareAdd.onCreated(function () {

  Meteor.call('orphelinsSameIp', function (err, orphelinsSameIp) {
    Session.set("orphelinsSameIp", orphelinsSameIp)
  });

  Session.set("addhardware-step", '0');

  Meteor.subscribe('configsqui')
});

Template.configurationHardwareAdd.helpers({

  breadcrumbLinks() {
    return appHd.buildBreadcrumb(["dashboard", "configuration", "configurationHardware", "configurationHardwareAdd"]);
  },

  orphelinsSameIp() {
    return Session.get("orphelinsSameIp")
  },

  addhardwarestep(step) {
    return Session.get("addhardware-step") === step
  },

  encours() {
    const encours = Hardwares.find({ 'valid': { $ne: true } });
    return encours.count() === 0 ? false : encours
  },

  hardwareType(type) {
    return Session.get("addhardware-type") === type
  },

  ipPublique() {

    Meteor.call('getMyIP', function (err, ip) {
      if (!err && ip)
        Session.set("myIp", ip);
    });
    return Session.get("myIp");
  },

  arduinoType() {
    return Session.get("addhardware-type");
  },

  blankArduino() {
    //FIXME ip locale ou pas?
    return DfmConnections.find({ arduino_user_id: Meteor.userId() }).fetch();
  },

  oldConfig() {
    return Hardwares.find({ owner_id: Meteor.userId(), type: "arduinodoudou" });
  }
});

Template.configurationHardwareAdd.events({

  'click .claim': function (e) {
    Meteor.call('hardwareClaimDDP', this, function (err, hard_id) {
      if (err) {
        console.log(err);
        appHd.alert(err)
      }
      else {
        Router.go('configurationHardwareEdit', { hard_id: hard_id })
      }
    });
  },

  'click .claim-old': function (e) {
    Meteor.call('hardwareClaimDDP', this.session, this.hard._id, function (err, hard_id) {
      if (err) {
        console.log(err);
        appHd.alert(err)
      }
      else {
        console.log(hard_id);
        Router.go('configurationHardwareEdit', { hard_id: hard_id })
      }
    });
  },

  'change [name="sameIp"]': function (e) {
    Session.set("sameIp", e.target.checked);
  },

  'click .js-step-0': function (e) {
    e.preventDefault();
    const type = $(e.currentTarget).parent().find('[name=type]').val();
    if (type === "other" || type === "nabaztag-pi") {
      Meteor.call('hardwareClaim', type, function (err, result) {
        if (err) {
          console.log(err);
          appHd.alert(err)
        }
        else {
          console.log(result);
          Router.go('configurationHardwareEdit', { hard_id: result.hard_id })
        }
      });
    }
    else {
      Session.set("addhardware-type", type);
      Session.set("addhardware-step", '1')
    }
  },

  'click .js-step-1': function (e) {
    const ip = $(e.currentTarget).parent().find('[name=ip]').val();
    Session.set("addhardware-ip", ip);
    Session.set("addhardware-step", '2')
  },

  'click .js-step-2': function (e) {
    const type = Session.get('addhardware-type');
    const ip = Session.get('addhardware-ip');
    const port = $(e.currentTarget).parent().find('[name=port]').val();

    Meteor.call('hardwareClaim', type, ip, port, function (err, result) {
      if (err) {
        console.log(err);
        appHd.alert(err)
      }
      else {
        console.log(result);

        if (result.type === 'arduinodoudou') {
          if (result.dialogue) {
            if (result.dialogue.statusCode === 200) {
              if (result.dialogue.data.maj_id === result.hard_id) {
                Router.go('configurationHardwareEdit', { hard_id: result.hard_id })
              }
              else
                appHd.alert("Problème de maj_id arduino");
            }
            else {
              //FIXME je dois remonter l'erreur
              appHd.alert("Problème de communication, port redirigé sur la box?");
              console.log("status code not 200");
            }
          }
        }
        else if (result.type === 'karotz') {
          Router.go('configurationHardwareEdit', { hard_id: result.hard_id });
        }
      }
    });

    Session.set("addhardware-step", '3')
  },

});
