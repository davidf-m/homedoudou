import './scenario-event-add-edit.html';

Template.configurationScenarioEventAddOrEdit.onRendered(function () {
});

Template.configurationScenarioEventAddOrEdit.helpers({

  evenementType: function () {
    return Array.from(arguments).indexOf(Session.get("hmd_scenario_evenement_type")) > -1;
  },

  options: function () {
    return appHd.scenarios.events
  },

  selected: function (s) {
    if(this.value === s) {
      Session.set("hmd_scenario_evenement_type", s);
      return "selected"
    }
    else
      return "";
  },

  hardOfKeyId: function () {
    return Hardwares.findOne({_id: this.hard_id});
  },

  myKeySelected: function (s) {
    return this._id === s ? "selected" : "";
  },

  myKeysSelected: function (s) {
    if(!s) return "";
    return s.indexOf(this._id) > -1 ? "checked" : "";
  },
});

Template.configurationScenarioEventAddOrEdit.events({

  'change [name=input]': function (e, tpl) {
    Session.set("hmd_scenario_evenement_type", $(e.currentTarget).val());
  },

  'click .js-event-add-cancel': function (e, tpl) {
    Session.set('scenario-event-adding', false);
    Session.set('scenario-event-editing', false)
  },

  'submit': function (e, tpl) {
    e.preventDefault();

    const data = tpl.$('form').serializeJSON();
    console.log(data);
    if(!data.name) return appHd.alert("please enter a name");
    if(!data.input) return appHd.alert("please enter a input");

    const scenarioId = Session.get('scenario-event-editing');
    if(!scenarioId)
      Meteor.call('scenario.event.add', data, function (err, result) {
        if(err) return console.log(err);
        else {
          Session.set('scenario-event-adding', false);
        }
      });
    else
      Meteor.call('scenario.event.edit', scenarioId, data, function (err, result) {
        if(err) return console.log(err);
        else {
          Session.set('scenario-event-editing', false);
        }
      });
  },
});