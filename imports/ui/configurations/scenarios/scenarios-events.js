import './scenario-event-add-edit.js';
import './scenarios-events.html';

Template.configurationScenariosEvents.onCreated(function () {
});

Template.configurationScenariosEvents.onRendered(function () {

  const sortableInstance = dfm.sortable('.sortable tbody', {
    update: (container) => {
      const updates = $(container).children().map((index, el) => {
        const $el = $(el); // Stocker l'objet jQuery
        const scenarioId = $el.data('sortable-id');
        return { scenarioId, orderScenarios: index };
      }).get(); // Convertir en array JavaScript
      Meteor.call('scenarios.updateRanks', updates); // Exemple d'appel groupé
    },
    handle: '.handle'
  });

});

Template.configurationScenariosEvents.helpers({

  breadcrumbLinks() {
    return appHd.buildBreadcrumb(["dashboard", "configuration", "configurationScenariosEvents"]);
  },

  isEventAdding: function () {
    return Session.get('scenario-event-adding');
  },

  isEventEditing: function () {
    const eventEditingId = Session.get('scenario-event-editing');
    if (!eventEditingId) return false;
    const scenario = Scenarios.findOne(eventEditingId);
    return scenario ? scenario : false;
  },

  events: function () {
    return Scenarios.find({ scenarioType: "event" }, { sort: { orderScenarios: 1, createdAt: 1 } });
  },

  eventInputText() {
    const i = appHd.arrayObjectIndexOf(appHd.scenarios.events, this.input, 'value');
    return appHd.scenarios.events[i]?.text;
  },

  hardOfKeyId2: function () {
    // si eventCleId est un array par exemple pour les checkbox, tous les capteurs à 0
    //FIXME aliasname of array ?
    if (!this.eventCleId) return;
    let k = null;
    if (typeof this.eventCleId === 'string')
      k = Keys.findOne(this.eventCleId);
    else
      k = Keys.findOne(this.eventCleId[0]);
    if (k) return Hardwares.findOne({ _id: k.hard_id });
    else return "";
  },

  cleOfId: function () {
    return Keys.findOne({ _id: this.eventCleId });
  },
});

Template.configurationScenariosEvents.events({

  'click .js-event-duplicate': function (e, tpl) {
    Meteor.call('scenarioEventDuplicate', this._id, function (err) {
      if (err) return appHd.logsError('scenarioEventDuplicate');
    });
  },

  'click .js-event-add': function (e, tpl) {
    Session.set('scenario-event-adding', true);
  },

  'click .js-event-edit': function (e, tpl) {
    const scenarioId = this._id;
    Session.set('scenario-event-editing', scenarioId)
  },

  'click .js-event-delete': function (e, tpl) {
    const that = this;
    dfm.confirm('Supprimer ?', function (result) {
      if (result) {
        Meteor.call('scenario.event.remove', that._id, function (err) {
          if (err) return appHd.logsError('scenario.event.remove');
        });
      }
    });
  },
});