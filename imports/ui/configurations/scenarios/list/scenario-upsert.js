/**
 * Created by DavidFM on 24 november 2020
 *
 */

import "./types/free.js";
import "./types/heater.js";
import "./types/cron.js";
import "./scenario-upsert.html";

const typeScenario = new ReactiveVar();

Template.configurationScenarioUpsert.onRendered(function () {
  const data = this.data;
  this.autorun(function () {
    typeScenario.set(data?.type)
    console.log(data?.type)
  })
});
Template.configurationScenarioUpsert.helpers({

  breadcrumbLinks() {
    return appHd.buildBreadcrumb(["dashboard", "configuration", "configurationScenariosList", "upsert"]);
  },

  isType(type) {
    return typeScenario.get() === type;
  }
});

Template.configurationScenarioUpsert.events({

  'change [name=type]'(e, tpl) {
    typeScenario.set($(e.currentTarget).val())
  },

  'submit': function (e, tpl) {
    e.preventDefault();

    const form = tpl.$('form').serializeJSON({
      customTypes: {
        utcTime: (str) => {
          console.log(str);
          if(str) {
            const time = str.split(':');
            const now = new Date();
            now.setHours(time[0]);
            now.setMinutes(time[1]);
            const utcHours = now.getUTCHours();
            const utcMinutes = now.getUTCMinutes();
            return ("0" + utcHours).slice(-2) + ":" + ("0" + utcMinutes).slice(-2);
          }
          return str;
        },
      }
    });

    console.log(form);
    if(!form.name) return appHd.alert("please enter a name");
    //FIXME by type
    // if(!data.input) return appHd.alert("please enter a input");
    // if(!data.output) return appHd.alert("please enter a output");

    // free
    if(form.type === "free") {
      if(form.output) form.output = form.output.filter(el => el);
    }

    //heater
    if(form.type === "heater") {
      form.rangeDateUTC = [];
      // Nettoies les valeurs vide de plage de fonctionement
      form.fromDate.forEach((f, i) => {
        if(form.fromDate[i] && form.toDate[i]) {
          form.rangeDateUTC.push({fromDate: form.fromDate[i], toDate: form.toDate[i], temperature: form.temperatures[i] * 1})
        }
      })
      delete form.fromDate;
      delete form.toDate;
      delete form.temperatures;
      if(form.temp) form.temp = parseFloat(form.temp);
      if(form.delta) form.delta = parseFloat(form.delta);
    }

    // cron
    if(form.type === "cron") {
      if(form.output) form.output = form.output.filter(el => el);
    }

    const scenarioId = this._id;
    if(!scenarioId)
      Meteor.call('scenario.add', form, function (err, result) {
        if(err) return console.log(err);
        else Router.go('configurationScenariosList');
      });
    else
      Meteor.call('scenarios.update', scenarioId, form, function (err, result) {
        if(err) return console.log(err);
        else Router.go('configurationScenariosList');
      });
  },
});