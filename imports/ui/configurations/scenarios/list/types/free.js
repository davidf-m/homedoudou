/**
 * Created by DavidFM on 24 november 2020
 *
 */

import "./free.html";

Template.configurationScenarioAddOrEditFree.helpers({

  events: function () {
    return Scenarios.find({scenarioType: "event"});
  }
});