/**
 * Created by DavidFM on 24 november 2020
 *
 */

import "./heater.html";

Template.configurationScenarioAddOrEditHeater.helpers({

  getDelta: function () {
    return this.delta || 0.5
  },
});

Template.configurationScenarioAddOrEditHeater.events({});