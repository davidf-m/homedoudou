import './scenarios-list.html';

Template.configurationScenariosList.onCreated(function () {
});

Template.configurationScenariosList.onRendered(function () {

  const sortableInstance = dfm.sortable('.sortable tbody', {
    update: (container) => {
      const updates = $(container).children().map((index, el) => {
        const $el = $(el); // Stocker l'objet jQuery
        const scenarioId = $el.data('sortable-id');
        return { scenarioId, orderScenarios: index };
      }).get(); // Convertir en array JavaScript
      Meteor.call('scenarios.updateRanks', updates); // Exemple d'appel groupé
    },
    handle: '.handle'
  });

});

Template.configurationScenariosList.helpers({

  breadcrumbLinks() {
    return appHd.buildBreadcrumb(["dashboard", "configuration", "configurationScenariosList"]);
  },

  scenarios: function () {
    return Scenarios.find({ scenarioType: "scenario" }, { sort: { orderScenarios: 1, createdAt: 1 } });
  },

  eventName: function (id) {
    const s = Scenarios.findOne(id);
    return s ? s.name : '<span class="badge badge-danger">error</span>';
  },

  actionName: function () {
    return Scenarios.findOne(this.valueOf())?.name;
  },
});

Template.configurationScenariosList.events({

  'click .js-scenario-duplicate': function (e, tpl) {
    Meteor.call('scenarioDuplicate', this._id, function (err) {
      if (err) return appHd.logsError('scenarioDuplicate');
    });
  },

  'click .js-scenario-enable': function (e, tpl) {
    Meteor.call('scenario.status', this._id, true, function (err) {
      if (err) return appHd.logsError('scenario.enable');
    });
  },

  'click .js-scenario-disable': function (e, tpl) {
    Meteor.call('scenario.status', this._id, false, function (err) {
      if (err) return appHd.logsError('scenario.disabled');
    });
  },

  'click .js-scenario-delete': function (e, tpl) {
    const that = this;
    dfm.confirm('Supprimer ?', function (result) {
      if (result) {
        Meteor.call('scenario.remove', that._id, function (err) {
          if (err) return appHd.logsError('scenario.remove');
        });
      }
    });
  },

  'click .js-action-test': function (e, tpl) {
    Meteor.call('scenarioActionLaunch', this.valueOf(), function (error, result) {
      if (!error) {
        console.log(result);
        //FIXME display message, not alert!
        // if(result && result.msg) appHd.alert(result.msg);
      }
      else console.log(error);
    });
  },

  'click .js-summerMode-toggle': function (e, tpl) {
    Meteor.callAsync('scenarios.summerMode.toggle', this.valueOf())
      .then(res => {
        console.log(res);
      })
      .catch(err => {
        console.log(err);
      });
  },
});