import './actions/scenario-status.js';
import './scenario-action-add-edit.html';

Template.configurationScenarioActionAddOrEdit.onRendered(function () {
  if (Session.get('scenario-action-adding'))
    Session.set("hmd_scenario_action_type", false);
});

Template.configurationScenarioActionAddOrEdit.helpers({

  hardwares: function () {
    return Hardwares.find({ type: 'arduinodoudou', owner_id: Meteor.userId() });
  },

  options: function () {
    return [
      { value: "", text: "-- Choisir une action" },
      { value: "getLastValue", text: "Prendre la dernière donnée et la faire dire à votre karotz" },
      { value: "doKarotz", text: "Action karotz" },
      { value: "relaiON", text: "Relai ON" },
      { value: "relaiOFF", text: "Relai OFF" },
      { value: "mhz433", text: "Mhz433" },
      { value: "serie", text: "Port serie" },
      { value: "sms-free", text: "Etre notifié, sms-free" },
      { value: "notif-mail-once", text: "Etre notifié, par mail une fois jusqu'a remise à zéro" },
      { value: "http-request", text: "HTTP Request" },
      { value: "socketSend", text: "Envoie socket" },
      { value: "scenarioStatus", text: "Changer l'état d'un scénario" },
    ]
  },

  selectedType: function (s) {
    if (this.value === s) {
      Session.set("hmd_scenario_action_type", s);
      return "selected"
    }
    else
      return "";
  },

  selectedHard: function (hardId) {
    if (this._id === hardId) {
      console.log("2")
      Session.set("select-relai-hard", hardId);
      return "selected"
    }
    else
      return "";
  },

  selectedBroche: function (broche) {
    console.log(this);
    console.log(broche);
    if (this.broche * 1 === broche * 1) {
      return "selected"
    }
    else
      return "";
  },

  relais: function () {
    const hardId = Session.get("select-relai-hard");
    const hard = Hardwares.findOne(hardId);
    if (!hard || !hard.devices) return false;
    return hard.devices.filter(function (device) {
      return device.type_id === 10 || device.type_id === 3
    });
  },

  karotzs: function () {
    return Hardwares.find({ type: 'karotz', owner_id: Meteor.userId() });
  },

  isHardSelected: function () {
    return Session.get("select-relai-hard");
  },

  type: function (type) {
    return Session.get("hmd_scenario_action_type") === type;
  },

  mhz433: function () {
    const hardId = Session.get("select-relai-hard");
    Meteor.subscribe("mhz433", hardId);
    return Mhz433.find({ hard_id: hardId });
  },
});

Template.configurationScenarioActionAddOrEdit.events({

  'change [name=type]': function (e, tpl) {
    Session.set("hmd_scenario_action_type", $(e.currentTarget).val());
  },

  'change .js-select-relai-hard': function (e, tpl) {
    console.log("1")
    Session.set("select-relai-hard", $(e.currentTarget).val());
  },

  'click .js-action-add-cancel': function (e, tpl) {
    Session.set('scenario-action-adding', false);
    Session.set('scenario-action-editing', false);
  },

  'submit': function (e, tpl) {
    e.preventDefault();

    const data = tpl.$('form').serializeJSON();
    console.log(data);
    const scenarioId = Session.get('scenario-action-editing');
    if (!scenarioId)
      Meteor.call('scenario.action.add', data, function (err, result) {
        if (err) return dfm.logError(JSON.stringify(err));
        else {
          Session.set('scenario-action-adding', false);
        }
      });
    else
      Meteor.call('scenario.action.edit', scenarioId, data, function (err, result) {
        if (err) return console.log(err);
        else {
          Session.set('scenario-action-editing', false);
        }
      });
  },
});