import './scenario-action-add-edit.js';
import './scenarios-actions.html';

Template.configurationScenariosActions.onCreated(function () {
});

Template.configurationScenariosActions.onRendered(function () {
  Session.set('scenario-action-editing', false);

  const sortableInstance = dfm.sortable('.sortable tbody', {
    update: (container) => {
      const updates = $(container).children().map((index, el) => {
        const $el = $(el); // Stocker l'objet jQuery
        const scenarioId = $el.data('sortable-id');
        return { scenarioId, orderScenarios: index };
      }).get(); // Convertir en array JavaScript
      Meteor.call('scenarios.updateRanks', updates); // Exemple d'appel groupé
    },
    handle: '.handle'
  });
});

Template.configurationScenariosActions.helpers({

  breadcrumbLinks() {
    return appHd.buildBreadcrumb(["dashboard", "configuration", "configurationScenariosActions"]);
  },

  isActionsAdding: function () {
    return Session.get('scenario-action-adding');
  },

  isActionEditing: function () {
    const actionEditingId = Session.get('scenario-action-editing');
    if (!actionEditingId) return false;
    const action = Scenarios.findOne(actionEditingId);
    return action ? action : false;
  },
});

Template.configurationScenariosActions.events({

  'click .js-action-new': function (e, tpl) {
    log('t')
    Session.set('scenario-action-adding', true);
  },

  'click .js-action-duplicate': function (e, tpl) {
    Meteor.call('scenarioActionDuplicate', this._id, function (err) {
      if (err) return appHd.logsError('scenarioActionDuplicate');
    });
  },

  'click .js-action-edit': function (e, tpl) {
    const actionId = this._id;
    Session.set('scenario-action-editing', actionId)
  },

  'click .js-action-delete': function (e, tpl) {
    e.preventDefault();
    e.stopPropagation();

    const that = this;

    dfm.confirm('Supprimer ?', function (result) {
      console.log(result);
      if (result) {
        Meteor.call('scenario.action.remove', that._id, function (err) {
          if (err) return appHd.logsError('scenario.action.remove');
        });
      }
    });
  },

  'click .js-action-test': function (e, tpl) {
    Meteor.call('scenarioActionLaunch', this.valueOf(), function (error, result) {
      if (!error) {
        console.log(result);
        //FIXME display message, not alert!
        // if(result && result.msg) appHd.alert(result.msg);
      }
      else console.log(error);
    });
  },
});