/**
 * Created by David FAIVRE-MAÇON on 08/08/2020
 * https://www.davidfm.me
 */

import './mhz-433-listen-on-off.html';

Template.mhz433ListenOnOff.events({

  'click .btn-toggle': function (e) {
    Meteor.call('setArduinoListening433', this._id, !this.listening433);
  },
  //
  // 'click .js-rx-433-repeated-stop': function (e, tpl) {
  //   Meteor.call('setArduinoListening433', this._id, !listening433);
  // },
  //
  // 'click .js-rx-433-repeated-start': function (e, tpl) {
  //   Meteor.call('setArduinoListening433', this._id, true);
  // },
});