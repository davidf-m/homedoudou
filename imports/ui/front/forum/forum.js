Template.forum.helpers({

  breadcrumbLinks() {
    return appHd.buildBreadcrumb(["dashboard", "forum"]);
  },

  posts: function () {
    return ForumQuestion.find();
  },

  created: function () {
    return this.created;
  }

});

Template.form.events({

  'click #submit': function (e) {
    e.preventDefault();
    var title = $('#title').val();
    var body = $('#body').val();

    Forum.insert({
      title: title,
      username: Meteor.user().username,
      body: body
    });

    $('#title, #body').val('');
  }
});
