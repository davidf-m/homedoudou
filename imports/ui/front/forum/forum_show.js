Template.forumShow.helpers({

  breadcrumbLinks() {
    return appHd.buildBreadcrumb(["dashboard", "forum"]);
  },

  post: function () {
    return ForumQuestion.findOne({_id: this.post_id});
  },

  postsReponse: function () {
    return ForumReponse.find({parent_id: this.post_id}, {sort: {created: 1}});
  },

  post_id: function () {
    return this.post_id;
  },

  created: function () {
    return this.created;
  }

});

Template.forumShow.events({

  'click .reponse-text': function (e) {
    var $text = $(e.currentTarget);
    $text.summernote({
      focus: true,
      onInit: function () {
        $text.parent().find('.js-reponse-submit').show();
      },
      onsubmit: function () {
        var body = $('#body').val();

        Forum.insert({
          username: Meteor.user().username,
          body: body
        });

        body.val('');
      }
    });
  },

  'click .js-reponse-submit': function (e) {

    var $text = $(e.currentTarget).parent().parent().find('.reponse-text');
    var body = $text.code();

    Meteor.call('forumReponseUpdate', this._id, body, function () {
      $('.js-reponse-submit').hide();
      $('.note-editor').remove(); // FIXME and what when y en a 2 editabled?
      $($text).show();
    });
  }

});

Template.forumRepondre.events({});
