/**
 * Created by David FAIVRE-MAÇON
 * https://www.davidfm.me
 */

import './contact.html';

Template.contact.onRendered(function () {
});

Template.contact.helpers({

  breadcrumbLinks() {
    return appHd.buildBreadcrumb(["dashboard", "contact"]);
  },
});

Template.contact.events({

  'submit #form-contact': function (e, tpl) {
    e.preventDefault();

    const contact = {
      nom: $('#nom').val(),
      from: $('#email').val(),
      message: $('#message').val(),
    };

    tpl.$('button[type=submit]').hide();
    tpl.$('#message-sending').removeClass('d-none');
    Meteor.call('newFormContact', contact, function (err) {
      tpl.$('#message-sending').addClass('d-none');
      tpl.$('button[type=submit]').show();
      if(!err) {
        $('#nom, #email, #objet, #message').val('');
        dfm.toast("Message envoyé!");
      }
      else dfm.toastError("Erreur pendant l'envoie!");
    });
  }
});

