/**
 * Created by David FAIVRE-MAÇON
 * https://www.davidfm.me
 */

import '../../display/display-key.js';
import '../mhz433.js';
import './arduino.html';

Template.tplArduino.onCreated(function () {
  this.subscribe('mhz433', this.data._id)
});

Template.tplArduino.onRendered(function () {
  $('#hardwareTabs li').first().find('a').tab('show');
});

Template.tplArduino.helpers({

  breadcrumbLinks() {
    return appHd.buildBreadcrumb(["dashboard", appHd.aliasname(this)]);
  },

  hardware: function () {
    return Hardwares.findOne({_id: this.hard_id});
  },

  devices: function () {
    if(this.devices && this.devices.length)
      return this.devices;
    else
      return false;
  },

  keys: function () {
    return Keys.find({hard_id: this._id, show: true, hidden: {$ne: true}}, {sort: {order: 1}});
  },

  isMine: function () {
    return this.owner_id === Meteor.userId();
  }

});

Template.tplArduino.events({});