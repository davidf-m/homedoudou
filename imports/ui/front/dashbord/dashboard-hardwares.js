/**
 * Created by David FAIVRE-MAÇON
 * https://www.davidfm.me
 */

import './dashboard-hardwares.html';

Template.dashboardHardwares.onRendered(function () {
  $('#myTabs li').first().find('a').tab('show');
});

Template.dashboardHardwares.helpers({

  active: function (connected) {
    return Meteor.userId() && connected === 'connected' || !Meteor.userId() && connected === 'notConnected' ? 'active' : '';
  },
});