/**
 * Created by David FAIVRE-MAÇON
 * https://www.davidfm.me
 */

import '../../shared/mhz-433-listen-on-off.js';
import './mhz433.html';

Template.mhz433.onCreated(function () {
});

Template.mhz433.helpers({

  mhz433: function () {
    return Mhz433.find({ hard_id: this._id });
  }
});

Template.mhz433.events({

  'submit #new': function (e, tpl) {
    e.preventDefault();

    const form = $(e.currentTarget).serializeJSON();
    console.log(form);
    form.hard_id = this._id;
    Meteor.call('Mhz433.insert', form, function (err) {
      if (!err) e.currentTarget.reset();
    });
  },

  'submit .send': function (e, tpl) {
    e.preventDefault();

    const hard = this.parent;
    const command = {
      mode: "433E",
      cle: this.context.broche,
      valeur: this.context.signal
    };

    Meteor.call('talkToArduino', hard._id, command, function (err, result) {
      if (err) console.log(err);
      else {
        console.log(result);
        if (result && result.statusCode === 200) {
          $('textarea').text(result.content);
        }
      }
    });
  },

  'change .exist input': function (e, tpl) {
    const set = {};
    const name = e.currentTarget.name;
    set[name] = e.currentTarget.value;
    Meteor.callAsync('Mhz433.update', this.context._id, set)
      .then(res => {
        dfm.toastSuccess('Mhz433 updated')
      })
      .catch(err => {
        dfm.toastError(JSON.stringify(err))
      });
  },

  'click .js-rx-433': function (e, tpl) {
    const $currentTarget = $(e.currentTarget);
    $currentTarget.removeClass('btn-primary');
    $currentTarget.addClass('btn-warning');

    const i = appHd.arrayObjectIndexOf(this.devices, 86, 'type_id');
    const broche = this.devices[i].broche;

    let seconds = $('input[name=seconds]').val() * 1;

    if (isNaN(seconds) || !seconds) seconds = 3000;
    else seconds *= 1000;

    Meteor.call('arduinoCommand', this._id, "433R", broche, seconds, function (err, result) {
      console.log(result);
      if (err) {
        $currentTarget.removeClass('btn-warning');
        $currentTarget.addClass('btn-danger');
        alert('http get FAILED!');
      }
      else {
        $currentTarget.removeClass('btn-warning');
        $currentTarget.removeClass('btn-danger');
        $currentTarget.addClass('btn-primary');
        $('#result').text("ok, pour l'instant voir log pour le retour");
      }
    });
  },

  'click .js-rx-433-start': function (e, tpl) {
    const $currentTarget = $(e.currentTarget);
    $currentTarget.removeClass('btn-primary');
    $currentTarget.addClass('btn-warning');

    const i = appHd.arrayObjectIndexOf(this.devices, 86, 'type_id');
    const broche = this.devices[i].broche;
    Meteor.call('arduinoCommand', this._id, "433R", broche, "0", function (err, result) {
      if (err) {
        console.log(err);
        $currentTarget.removeClass('btn-warning');
        $currentTarget.addClass('btn-danger');
        alert('http get FAILED!');
      }
      else {
        console.log(result);
        $currentTarget.removeClass('btn-warning');
        $currentTarget.addClass('btn-primary');
        $('#result').text("ok, ecoute infinie, pour l'instant voir log pour le retour");
      }
    });
  },

  'click .js-rx-433-stop': function (e, tpl) {
    const $currentTarget = $(e.currentTarget);
    $currentTarget.removeClass('btn-primary');
    $currentTarget.addClass('btn-warning');

    const i = appHd.arrayObjectIndexOf(this.devices, 86, 'type_id');
    const broche = this.devices[i].broche;
    Meteor.call('arduinoCommand', this._id, "433R", broche, "STOP", function (err, result) {
      $currentTarget.removeClass('btn-warning');
      if (err) {
        $currentTarget.addClass('btn-danger');
        alert(err);
      }
      else {
        console.log(result);
        const $btnStart = $('.js-rx-433-start');
        $btnStart.removeClass('btn-warning');
        $btnStart.removeClass('btn-warning');
        $btnStart.addClass('btn-primary');
        $currentTarget.removeClass('btn-warning');
        $currentTarget.removeClass('btn-danger');
        $currentTarget.addClass('btn-primary');
        $('#result').text("ok, pour l'instant voir log pour le retour");
      }
    });
  },

  'click .js-delete': function (e, tpl) {
    e.preventDefault();

    Meteor.call('Mhz433.remove', this.context._id, function (err,) {
      // if(!err){
      //
      // }
    });
  }
});

