/**
 * Created by David FAIVRE-MAÇON
 * https://www.davidfm.me
 */

import './rooms/rooms.js';
import './dashboard-hardwares.js';
import './dashboard.html';

Template.dashboard.onCreated(function () {
  Meteor.call('getAbsoluteLastData', {onlyMine: true}, function (err, data) {
    if(!err) Session.set('lastData', data)
  });
});


Template.dashboard.helpers({

  breadcrumbLinks() {
    return appHd.buildBreadcrumb(["dashboard"]);
  },

  lastData() {
    return Session.get('lastData');
  },

  configGroupBoard: function () {
    return Meteor.user() && Meteor.user().settings && Meteor.user().settings.groupBoard;
    // return Meteor.userId() && Rooms.find().count();
  }

});

Template.dashboard.events({

  'click #myTabs a': function (e) {
    e.preventDefault();

    $(e.currentTarget).tab('show')
  }

});

Template.dashTab.helpers({

  keys(type) {
    if(this.which === 'mine') {
      const keys = Keys.find({type: type, owner_id: Meteor.userId()}, {sort: {alias: 1, name: 1}});
      return keys.count() ? keys : false;
    }
    if(this.which === 'shared') {
      const sharedHards = Hardwares.find({"shared.email": dfm.getUserEmail(Meteor.userId())}).map(function (h) {
        return h._id
      });
      const keys = Keys.find({type: type, owner_id: {$ne: Meteor.userId()}, hard_id: {$in: sharedHards}}, {sort: {alias: 1, name: 1}});
      return keys.count() ? keys : false;
    }
    if(this.which === 'public') {
      const keys = Keys.find({type: type, owner_id: {$ne: Meteor.userId()}, public: true}, {sort: {alias: 1, name: 1}});
      return keys.count() ? keys : false;
    }
  },

  hard() {
    return Hardwares.findOne(this.hard_id)
  },
});

