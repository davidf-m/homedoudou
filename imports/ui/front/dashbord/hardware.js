/**
 * Created by David FAIVRE-MAÇON
 * https://www.davidfm.me
 */

import './arduino/arduino.js';
import './nabaztag/nabaztag.js';
import './karotz/karotz.js';
import './hardware.html';

Template.hardware.onCreated(function () {
});

Template.hardware.onRendered(function () {
});

Template.hardware.helpers({

  breadcrumbLinks() {
    return appHd.buildBreadcrumb(["dashboard", {title: appHd.aliasname(this), class: "color-front"}]);
  },

  hardware: function () {
    return Hardwares.findOne({_id: this.hard_id});
  },

  devices: function () {
    if(this.devices && this.devices.length)
      return this.devices;
    else
      return false;
  },

  keys: function () {
    return Keys.find({hard_id: this._id, show: true, hidden: {$ne: true}});
  },
});

Template.hardware.events({});