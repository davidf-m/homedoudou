/**
 * Created by DavidFM on 23/06/2015.
 */

import './karotz.html';

Template.karotz.onRendered(function () {

  // create canvas and context objects
  const canvas = document.getElementById('picker');
  const ctx = canvas.getContext('2d');

  // drawing active image
  let image = new Image();
  image.onload = function () {
    ctx.drawImage(image, 0, 0, image.width, image.height);
  };
  image.src = '/img/colorwheel5.png';

  $('#picker').mousemove(function (e) { // mouse move handler

    const canvasOffset = $(canvas).offset();
    const canvasX = Math.floor(e.pageX - canvasOffset.left);
    const canvasY = Math.floor(e.pageY - canvasOffset.top);

    const imageData = ctx.getImageData(canvasX, canvasY, 1, 1);
    const pixel = imageData.data;

    const dColor = pixel[2] + 256 * pixel[1] + 65536 * pixel[0];
    $('#hexVal').val(('0000' + dColor.toString(16)).substr(-6));
  });

});


Template.karotz.helpers({

  karotz() {
    return Hardwares.find({ type: 'karotz' });
  },

  photos() {
    return Session.get('photos');
  },

  webcam() {
    return Session.get('webcam');
  },

  radios() {
    return HMD_Karotz.find({ type: 'radios', deleted: { "$exists": false } }, { sort: { idx: 1 } });
  },

  cmdUpdate() {
    let cmd = HMD_Karotz.find({ type: 'cmdUpdate', deleted: { "$exists": false } }, { sort: { idx: 1 } }).fetch().map(function (elem) {
      return elem.value;
    }).join(";");
    return "/cgi-bin/cmd?cmd=" + encodeURI(cmd);
  },

});

Template.karotz.events({

  'click button.js-send-action': function (e, tpl) {
    const command = $('input[name=action]').val();

    Meteor.call('hmdHttpGet', "http://" + this.ip + ":" + this.port + command, function (err, result) {
      if (err) {
        console.log('http get FAILED!');
      }
      else {
        console.log('http get SUCCES');
        if (result.statusCode === 200) {
          console.log('Status code = 200!');
          console.log(result.content);
          $('textarea').text(result.content);
        }
      }
    });

  },

  'click button.js-send-tts': function (e, tpl) {
    const $tts = $('input[name=tts]');
    const command = "/cgi-bin/tts?voice=1&text=" + $tts.val() + "&nocache=0";
    $tts.val('Envoie en cours...');
    Meteor.call('hmdHttpGet', "http://" + this.ip + ":" + this.port + command, function (err, result) {
      if (err) {
        console.log('http get FAILED!');
        $tts.val('Erreur, désolé...');
      }
      else {
        console.log('http get SUCCES');
        if (result.statusCode === 200) {
          console.log('Status code = 200!');
          console.log(result.content);
          $('textarea').text(result.content);
          $tts.val('');
        }
      }
    });

  },

  'click button.js-webcam': function (e, tpl) {
    Session.set('webcam', true);
    Session.set('photos', false);
  },

  'click button.js-get-photos': function (e, tpl) {

    Session.set('webcam', false);

    Meteor.call('hmdHttpGet', "http://" + this.ip + ":" + this.port + '/cgi-bin/snapshot_list', function (err, result) {
      if (err) {
        console.log('http get FAILED!');
      }
      else {
        console.log('http get SUCCES');
        if (result.statusCode === 200) {
          console.log('Status code = 200!');
          console.log(result.content);
          const snapshots = JSON.parse(result.content);
          console.log(snapshots.snapshots);

          Session.set('photos', snapshots.snapshots)
        }
      }
    });

  },

  'click button.js-take-photo': function (e, tpl) {

    Meteor.call('hmdHttpGet', "http://" + this.ip + ":" + this.port + '/cgi-bin/snapshot', function (err, result) {
      if (err) {
        console.log('http get FAILED!');
      }
      else {
        console.log('http get SUCCES');
        if (result.statusCode === 200) {
          console.log('Status code = 200!');
          console.log(result.content);
          const snapshots = JSON.parse(result.content);
          console.log(snapshots.snapshots);
        }
      }
    });

  },

  'click .js-command': function (e, tpl) {
    const backupText = $(e.currentTarget).text();
    $(e.currentTarget).text('En cours...');
    const command = $(e.currentTarget).attr('data-command');
    appHd.karotz.command(this, command, function (res, err) {
      if (err) {
        console.log('http get FAILED!');
        dfm.toastError('Erreur : voir console');
      }
      else {
        dfm.toast('status returned');
        $('textarea').val(res);
      }
      $(e.currentTarget).text(backupText);
    });
  },

  'click .js-update': function (e, tpl) {
    const backupText = $(e.currentTarget).text();
    $(e.currentTarget).text('En cours...');
    const kartoz = this;
    const command = $(e.currentTarget).attr('data-command');
    appHd.karotz.command(kartoz, '/cgi-bin/leds?color=FF0000');
    appHd.karotz.command(kartoz, command, function () {
      appHd.karotz.command(kartoz, '/cgi-bin/doudou/doudou_cmd?cmd=setId&id=' + kartoz._id);
      appHd.karotz.command(kartoz, '/cgi-bin/reboot');
      $(e.currentTarget).text(backupText);
    });
  },

  'change [name=color]': function (e, tpl) {
    let color = $(e.currentTarget).val();
    color = color.replace('#', '');
    const command = "/cgi-bin/leds?color=" + color;
    appHd.karotz.command(this, command);
  },

  'click #picker': function (e) {
    const command = "/cgi-bin/leds?color=" + $('#hexVal').val();
    appHd.karotz.command(this, command);
  },

  'click #btn_radio_play': function (e) {
    const url = $('#cbx_stream').val();
    console.log(url);
    const command = "/cgi-bin/sound?url=" + url;
    appHd.karotz.command(this, command);
  }

});
