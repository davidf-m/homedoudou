/**
 * Created by David FAIVRE-MAÇON
 * https://www.davidfm.me
 */

import './data-graph.js';
import './data-graph-google.js';
import './data-info.js';
import './data-volatile.js';
import './data.html';

Template.dataKey.onCreated(function () {
  Session.set("keyDebug", false);
  Session.set('graphDateFrom', false);
  Session.set('graphDateTo', false);
});

Template.dataKey.onRendered(function () {

  this.autorun(function () {
    const params = Router.current().params;
    Session.set("keyDebug", false);
    Session.set('graphDateFrom', false);
    Session.set('graphDateTo', false);
  });

});

Template.dataKey.helpers({

  breadcrumbLinks() {
    const HardOne = Hardwares.findOne(this.hard_id, { fields: { alias: 1, name: 1 } });
    const KeyOne = Keys.findOne({ hard_id: this.hard_id, name: this.name });

    // if(appHd.isUserSettingsActive('groupBoard'))
    //   return appHd.buildBreadcrumb(["dashboard", {link: Router.path("hardware", {hard_id: this.hard_id}), title: appHd.aliasname(HardOne), class: "color-front"}, {title: appHd.aliasname(KeyOne), class: "color-front"}]);
    // else
    return appHd.buildBreadcrumb(["dashboard", { link: Router.path("hardware", { hard_id: this.hard_id }), title: appHd.aliasname(HardOne), class: "color-front" }, { title: appHd.aliasname(KeyOne), class: "color-front" }]);
  },

  key() {
    return Keys.findOne({ hard_id: this.hard_id, name: this.keyName });
  },

  hardOne: function () {
    return Hardwares.findOne({ _id: this.hard_id });
  },

  lastSeen: function () {
    const Hard = Hardwares.findOne({ _id: this.hard_id });
    if (!Hard) return false;
    return Hard.lastSeen ? Hard.lastSeen : false;
  },

  isTabActive: function (howLongHour) {
    log(howLongHour, this.howLongHour)
    log(this)
    return howLongHour === this.howLongHour * 1 ? 'tabActive' : '';
  },

  data: function () {
    const now = Date.now();
    const dateAfter = now - (this.howLongHour * 60 * 60 * 1000);
    return DataByKey[this._id].find({ cle: this.name, date: { $gt: dateAfter } }, { sort: { date: -1 } }).fetch();
  },

  canDebug: function () {
    return dfm.isAdmin();
  },

  debug: function () {
    return Session.get("keyDebug")
  },

  query() {
    return Router.current().params.query
  },

  isKwhPriced() {
    if (!Session.get("dataKeyGraphDataReady")) return false;
    return this.device?.type_id === 77 && (this.keyName === "HCHP" || this.keyName === "HCHC" || this.keyName === "BASE");
  },

  edfPrice() {
    return appHd.edfPrice(this);
  },

  dateFrom() {
    return Session.get('graphDateFrom') || ''
  },

  dateTo() {
    return Session.get('graphDateTo') || ''
  }
});

Template.dataKey.events({

  // 'click .js-export-excel' (e) {
  //   const nameFile = this.name + '.csv';
  //   $('.js-export-excel').text('En cours');
  //   Meteor.call('exportExcel', this.hard_id, this.name, function (err, fileContent) {
  //     if (fileContent) {
  //       const blob = new Blob([fileContent], { type: "text/plain;charset=utf-8" });
  //       saveAs(blob, nameFile);
  //       $('.js-export-excel').text('Export Excel');
  //     }
  //   });
  // },

  'click .js-export-excel'(e) {
    const nameFile = this.name;
    $('.js-export-excel').text('En cours');
    Meteor.callAsync('exportCollectionToCSV', this.hard_id, this.name)
      .then((fileContent) => {
        const blob = new Blob([fileContent], { type: 'text/csv' });
        const url = window.URL.createObjectURL(blob);
        const a = document.createElement('a');
        a.href = url;
        a.download = nameFile + '.csv';
        document.body.appendChild(a);
        a.click();
        window.URL.revokeObjectURL(url);
        $('.js-export-excel').text('Export Excel');
      })
      .catch((error) => {
        alert('Erreur lors de l\'export CSV : ' + error.reason);
        console.error('Erreur lors de l\'export CSV :', error.reason);
        // Gérer l'erreur côté client
      });
  }
});
