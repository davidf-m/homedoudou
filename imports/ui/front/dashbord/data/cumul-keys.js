import './data-graph.js';
import './cumul-keys.html';

import Highcharts from 'highcharts/highstock'
//const Highcharts = require('highcharts');
// Load module after Highcharts is loaded
//require('highcharts/modules/exporting')(Highcharts);

Highcharts.setOptions({
  global: {
    useUTC: false
  }
});
import { ReactiveVar } from "meteor/reactive-var";

// const get = new ReactiveVar();
Template.cumulativeKeys.onCreated(function () {

  // const self = this;
  // this.autorun(function() {
  //   const data = Template.currentData();
  //   if(!data) return;
  //   self.subscribe('xxx', {serviceId: data._id});
  // });
});

Template.cumulativeKeys.onRendered(function () {

  const instance = this;
  instance.autorun(function () {
    $('#container').html('Loading...');
    const params = Router.current().params;
    console.log(params);
    const cumulKeys = CumulKeys.findOne(params.cumulsKeysId);
    console.log(cumulKeys);
    if (!params.howLongHour) params.howLongHour = "24";

    const now = Date.now();
    const dateAfter = now - (params.howLongHour * 60 * 60 * 1000);

    const seriesOptions = [], names = [];

    if (!cumulKeys) return console.log('no data');
    cumulKeys.lines.forEach(function (line, i) {
      console.log(i);
      console.log(line);
      if (!line.key_id) {
        seriesOptions[i] = {
          name: "null",
          data: []
        };
        return
      }
      const keyOne = Keys.findOne(line.key_id);
      names[i] = keyOne.name;

      console.log(keyOne);
      console.log(DataByKey[keyOne._id].findOne());
      const data = DataByKey[keyOne._id].find({ cle: keyOne.name, date: { $gt: dateAfter } }, { sort: { date: 1 } }).fetch();
      console.log(data);
      let volume = [];
      const dataLength = data.length;

      for (let i = 0; i < dataLength; i++) {
        volume.push([
          parseFloat(data[i].date),
          parseFloat(data[i].valeur)
        ]);
      }

      seriesOptions[i] = {
        name: appHd.aliasname(keyOne),
        data: volume
      };
      console.log(keyOne);

    })

    Session.set("dataKeyGraphDataReady", true);


    Session.set('graphDateFrom', now)
    Session.set('graphDateTo', dateAfter)


    cumulKeysGraph(seriesOptions);

    // Meteor.call('keyUpdateHowLongHour', {howLongHour: params.howLongHour, hard_id: keyOne.hard_id, name: keyOne.name});
    // const isReady = handle.ready();
    // if(isReady) {
    //
    // }
    // else Session.set("dataKeyGraphDataReady", false);
  });


});

Template.cumulativeKeys.helpers({

  breadcrumbLinks() {
    return appHd.buildBreadcrumb(["dashboard"]);
  },

  isTabActive(howLongHour) {
    const params = Router.current().params;
    return howLongHour === params.howLongHour * 1 ? 'tabActive' : '';
  },

  cumulKey() {
    return CumulKeys.findOne(this.cumulsKeysId);
  }
});

Template.cumulativeKeys.events({

  // 'click '(e, tpl){
  //   e.preventDefault();
  // }
});

function cumulKeysGraph(seriesOptions) {

  console.log(seriesOptions);
  Highcharts.stockChart('container', {

    rangeSelector: {
      selected: 4
    },

    yAxis: {
      labels: {
        formatter: function () {
          return (this.value > 0 ? ' + ' : '') + this.value + '%';
        }
      },
      plotLines: [{
        value: 0,
        width: 2,
        color: 'silver'
      }]
    },

    plotOptions: {
      series: {
        compare: 'value',
        showInNavigator: true
      }
    },

    tooltip: {
      // pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b> ({point.change}%)<br/>',
      pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b>',
      valueDecimals: 2,
      split: true
    },

    series: seriesOptions
  });
  // }
}