/**
 * Created by David FAIVRE-MAÇON
 * https://www.davidfm.me
 */

import './data-graph-google.html';

/**
 * Created by DavidFM on 24 november 2020
 *
 */

import './data-graph-google.html';

// const instance = this;
// const keyOne = instance.data;
// console.log(keyOne);
// instance.autorun(function () {
//   $('#container').html('Loading...');
//   const params = Router.current().params;
//   if(!params.howLongHour) {
//     if(!keyOne.howLongHour) params.howLongHour = "24";
//     else params.howLongHour = keyOne.howLongHour;
//   }
//   Meteor.call('keyUpdateHowLongHour', {howLongHour: params.howLongHour, hard_id: keyOne.hard_id, name: keyOne.name});
//   const handle = instance.subscribe('donnees', keyOne.hard_id, keyOne.name, params.howLongHour);
//   const isReady = handle.ready();
//   if(isReady) {
//     Session.set("dataKeyGraphDataReady", true);
//     appHd.graphLaunch(keyOne, params.howLongHour);
//     if(params && params.query) Session.set("keyDebug", params.query.debug);
//   }
//   else Session.set("dataKeyGraphDataReady", false);

Template.dataKeyGraphGoogle.onCreated(function () {
});

Template.dataKeyGraphGoogle.onRendered(function () {
  const instance = this;
  const keyOne = instance.data;
  google.charts.load('current', {packages: ['corechart', 'line'], 'language': 'fr'});
  this.autorun(function () {
    const params = Router.current().params;
    if(!params.howLongHour) {
      if(!keyOne.howLongHour) params.howLongHour = "24";
      else params.howLongHour = keyOne.howLongHour;
    }
    Meteor.call('keyUpdateHowLongHour', {howLongHour: params.howLongHour, hard_id: keyOne.hard_id, name: keyOne.name});
    const handle = instance.subscribe('donnees', keyOne.hard_id, keyOne.name, params.howLongHour);
    const isReady = handle.ready();
    if(isReady) {
      Session.set("dataKeyGraphDataReady", true);
      if(params && params.query) Session.set("keyDebug", params.query.debug);

      const now = Date.now();
      const dateAfter = now - (params.howLongHour * 60 * 60 * 1000);

      const graph = {
        id: '1',
        title: appHd.aliasname(keyOne),
        type: 'line',
        cols: [{"label": "Jour", "type": "datetime"}, {"label": "Total", "type": "number"}],
        rows: function () {
          const data = DataByKey[keyOne._id].find({cle: keyOne.name, date: {$gt: dateAfter}}, {sort: {date: 1}}).fetch();
          const nbDays = data.length;

          const array = [];
          for(let i = 0; i < nbDays; i++) {
            array.push([new Date(data[i]?.date), data[i]?.valeur * 1]);
          }
          return array;
        }(),
      };
      console.log(graph.rows);
      // if(graph.cols?.length && graph.rows?.length) {
      google.charts.setOnLoadCallback(drawChart.bind(this, graph));
      // }


    }
    else Session.set("dataKeyGraphDataReady", false);

  });
});

Template.dataKeyGraphGoogle.helpers({});

Template.dataKeyGraphGoogle.events({});

function drawChart(tplData) {
  const $chartDiv = $('.google-chart')[0];
  const data = new google.visualization.DataTable();

  // console.log(tplData);
  tplData.cols.forEach(function (col) {
    data.addColumn(col.type, col.label);
  });
  data.addRows(tplData.rows);

  const ticks = [];
  tplData.rows.forEach(function (row) {
    ticks.push(row[0]);
  });

  const classicOptions = {
    title: tplData.title,
    width: "100%",
    height: 500,
    hAxis: {
      ticks: ticks,
      title: 'Date',
      format: 'd MMM HH:mm',
    },
    explorer: {
      actions: ['dragToZoom', 'rightClickToReset'],
      maxZoomIn: 0.01, // 1/10 du graph entier
    }, // classic version of google charts
  };
  if(tplData.dual) {
    Object.assign(classicOptions, {
      series: {
        0: {targetAxisIndex: 0},
        1: {targetAxisIndex: 1},
      },
      vAxes: {
        // Adds titles to each axis.
        0: {title: tplData.cols[1].label},
        1: {title: tplData.cols[2].label},
      },

      // vAxis: {
      //   viewWindow: {
      //     max: 30
      //   }
      // },

    });
  }
  const classicChart = new google.visualization.LineChart($chartDiv);
  classicChart.draw(data, classicOptions);
}