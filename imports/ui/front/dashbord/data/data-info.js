/**
 * Created by David FAIVRE-MAÇON
 * https://www.davidfm.me
 */

import './data-info.html';

Template.dataKeyInfo.onRendered(function () {
  const instance = this;
  const keyOne = instance.data;
  console.log(keyOne);
  instance.autorun(function () {
    $('#container').html('Loading...');
    const params = Router.current().params;
    if(!params.howLongHour) {
      if(!keyOne.howLongHour) params.howLongHour = "24";
      else params.howLongHour = keyOne.howLongHour;
    }
    Meteor.call('keyUpdateHowLongHour', {howLongHour: params.howLongHour, hard_id: keyOne.hard_id, name: keyOne.name}, function (err, res) {
    });
    console.log(keyOne);
    console.log(params);
    const handle = instance.subscribe('donnees', keyOne.hard_id, keyOne.name, params.howLongHour);
    const isReady = handle.ready();
    if(isReady) {
      // appHd.graphLaunch(keyOne, params.howLongHour);
      if(params && params.query) Session.set("keyDebug", !!params.query.debug);
    }
  });

});

Template.dataKeyInfo.helpers({

  data: function () {
    const now = Date.now();
    console.log(this)
    const dateAfter = now - (this.howLongHour * 60 * 60 * 1000);
    console.log(DataByKey[this._id].find({cle: this.name, date: {$gt: dateAfter}}, {sort: {date: -1}}).fetch());
    return DataByKey[this._id].find({cle: this.name, date: {$gt: dateAfter}}, {sort: {date: -1}}).fetch();
  },

});