/**
 * Created by David FAIVRE-MAÇON
 * https://www.davidfm.me
 */

import './data-debug.html';

Template.dataKeyDebug.onRendered(function () {
  const instance = this;
  const keyOne = instance.data;
  console.log(keyOne);
  instance.autorun(function () {
    $('#container').html('Loading...');
    const params = Router.current().params;
    if (!params.dateFrom) Router.go('dataKeyDebug', { hard_id: keyOne.hard_id, keyName: keyOne.name, dateFrom: Date.parse(new Date()) - 1000 * 60 * 60 * 24, dateTo: Date.parse(new Date()) })
    const handle = instance.subscribe('dataRange', keyOne.hard_id, keyOne.name, params.dateFrom, params.dateTo);
    const isReady = handle.ready();
    if (isReady) {
      if (params && params.query) Session.set("keyDebug", params.query.debug);
    }
  });

});

Template.dataKeyDebug.helpers({

  debugData: function () {
    const params = Router.current().params;
    Session.set("keyDebug", params.query.debug)

    const dateFrom = params.dateFrom * 1;
    const dateTo = params.dateTo * 1;

    return DataByKey[this._id].find({ date: { $gt: dateFrom, $lt: dateTo } }, { sort: { date: 1 } });
  },

  isDebug() {
    return Session.get('debug-edit') === this._id
  },

  dateFrom() {
    return Router.current().params.dateFrom * 1 || ''
  },

  dateTo() {
    return Router.current().params.dateTo * 1 || ''
  }
});

Template.dataKeyDebug.events({


  'click .js-debug-edit': function () {
    Session.set('debug-edit', this._id)
  },

  'click .js-debug-delete': function () {
    console.log(this);
    const keyId = this.cle;
    const _id = this._id;
    dfm.confirm('Supprimer ?', function (result) {
      if (result) {
        Meteor.call('data_remove', keyId, _id, function (err) {
        });
      }
    });
  },

  'submit #date-range': function (e, tpl) {
    e.preventDefault();

    const form = tpl.$('#date-range').serializeJSON();
    form.dateFrom = Date.parse(form.dateFrom);
    form.dateTo = Date.parse(form.dateTo);
    console.log(form);

    console.log(this)
    Router.go('dataKeyDebug', { hard_id: this.hard_id, keyName: this.name, dateFrom: form.dateFrom + "", dateTo: form.dateTo + "" })

    // Meteor.call('dataEdit', this, form, function (err, result) {
    //   if(err) console.log(err);
    //   else {
    //     Session.set('debug-edit', false)
    //   }
    // });
  },

  'submit #debug-edit': function (e, tpl) {
    e.preventDefault();

    const form = tpl.$('#debug-edit').serializeJSON();
    form.date = Date.parse(form.date);
    Meteor.call('dataEdit', this, form, function (err, result) {
      if (err) console.log(err);
      else {
        Session.set('debug-edit', false)
      }
    });
  },

  'submit #newData': function (e, tpl) {
    e.preventDefault();

    const form = tpl.$('#newData').serializeJSON();
    console.log(this);
    Meteor.call('hmdHttpGet', Meteor.absoluteUrl() + "add?id=" + this.hard_id + "&mode=archive&cle=" + this.name + "&valeur=" + form.valeur, function (err, result) {
      if (err) console.log(err);
      else {
        console.log(result);
        if (result.statusCode === 200) {
          $('[name=valeur]').val('')
        }
      }
    });
  }
});