/**
 * Created by David FAIVRE-MAÇON
 * https://www.davidfm.me
 */

import './data-graph.html';

Template.dataKeyGraph.onRendered(function () {
  const instance = this;
  const keyOne = instance.data;
  console.log(keyOne);
  instance.autorun(function () {
    $('#container').html('Loading...');
    const params = Router.current().params;
    if(!params.howLongHour) {
      if(!keyOne.howLongHour) params.howLongHour = "24";
      else params.howLongHour = keyOne.howLongHour;
    }
    Meteor.call('keyUpdateHowLongHour', {howLongHour: params.howLongHour, hard_id: keyOne.hard_id, name: keyOne.name});
    const handle = instance.subscribe('donnees', keyOne.hard_id, keyOne.name, params.howLongHour);
    const isReady = handle.ready();
    if(isReady) {
      Session.set("dataKeyGraphDataReady", true);
      appHd.graphLaunch(keyOne, params.howLongHour);
      if(params && params.query) Session.set("keyDebug", params.query.debug);
    }
    else Session.set("dataKeyGraphDataReady", false);
  });

});