/**
 * Created by David FAIVRE-MAÇON
 * https://www.davidfm.me
 */

import '../../display/display-key.js';
import './room.html';

Template.dashboardRoom.helpers({


  buttons() {
    return Buttons.find({ rooms: this._id }, { sort: { order: -1 } });
  },

  actions() {
    return Scenarios.find({ scenarioType: "action", rooms: this._id }, { sort: { order: 1 } });
  },

  scenarios() {
    return Scenarios.find({ scenarioType: "scenario", rooms: this._id }, { sort: { orderScenarios: "1", createdAt: 1 } });
  },

  buttonsScenarios() {
    // FIXME il peut y avoir plusieurs scenario pour une action, choisir le scenario correspondant dans le boutton?
    // ça peut etre un scenario chauffage aussi...
    const scenarioHeater = Scenarios.findOne({ scenarioType: "scenario", type: "heater", heaterOn: this.on, heaterOff: this.off })
    if (scenarioHeater) return scenarioHeater;
    const scenarioOn = Scenarios.findOne({ scenarioType: "scenario", output: this.on })
    // console.log(scenarioOn);
    const scenarioOff = Scenarios.findOne({ scenarioType: "scenario", output: this.off })
    // console.log(scenarioOff);
    const a = []
    if (scenarioOn) a.push({ name: "ON", scenario: scenarioOn });
    if (scenarioOff) a.push({ name: "OFF", scenario: scenarioOff });
    return a.length ? a : false
  },

  keys() {
    // FIXME use order in group
    return Keys.find({ rooms: this._id, show: true }, { sort: { order: 1 } }).fetch();

    // ordonne les keys d'abord par hardwares.order puis key.order
    // keys.forEach(function (k) {
    //   const hard = Hardwares.findOne(k.hard_id);
    //   const ho = hard?.order || 0;
    //   const ko = k?.order || 0;
    //   k.bigOrder = ho * 100 + ko
    // });
    // keys.sort(function (a, b) {
    //   return a.bigOrder - b.bigOrder;
    // });
    // return keys
  },

  cumulKeys() {
    return CumulKeys.find({ roomsId: this._id, ownerId: Meteor.userId() });
  },

  isButtonAdding() {
    return Session.get('isButtonAdding');
  },

  isButtonEditing() {
    return Session.get('isButtonEditing') === this._id;
  },

  selected(parent) {
    return this.value === parent.icon ? "selected" : ""
  },

  isActive(opts) {
    opts = opts.hash;
    if (!this.backId) return '';
    const key = appHd.getKeyById(this.backId);
    if (!key) return ''
    let keyType = key.type;
    let keyValue = key.lastData?.value;
    if (!keyValue) return '';
    if (keyType === 'inversed-boolean') {
      if (keyValue === "1" || keyValue.toLowerCase() === "true") keyValue = false;
      else if (keyValue === "0" || keyValue.toLowerCase() === "false") keyValue = true;
    }
    else if (keyValue === "0" || keyValue.toLowerCase() === "false" || keyValue.toLowerCase() === "off") keyValue = false;
    if (opts.off === "off") return keyValue ? (opts.classOff || "") : (opts.classOn || "active");
    return keyValue ? (opts.classOn || "active") : (opts.classOff || "");
  },

  getConsigne() {
    //FIXME time must be reacive
    let consigne = this.temp;
    const time = new Date().toLocaleString('fr-FR', { hour: "numeric", minute: "numeric", "hour12": false });
    this.rangeDateUTC.forEach((customRange) => {
      const fromDate = appHd.utcToLocalTime(customRange.fromDate);
      const toDate = appHd.utcToLocalTime(customRange.toDate);
      if ((toDate > fromDate && time > fromDate && time < toDate) || (fromDate > toDate && (time > fromDate || time < toDate))) {
        consigne = customRange.temperature
      }
    });
    return consigne;
  }
});

Template.dashboardRoom.events({

  'click .js-button-action': function (e, tpl) {
    e.preventDefault();

    e.currentTarget.classList.add('in-progress')
    const url = $(e.currentTarget).data('commande');
    Meteor.call('hmdHttpGet', url, function (err, result) {
      e.currentTarget.classList.remove('in-progress')
      if (!err) {
        console.log(result);
        if (result && result.statusCode === 200) {
          console.log('http get SUCCES');
          console.log(result);
          tpl.$('.action-result').show();
          tpl.$('.result').text(result.content);
        }
      }
      else {
        log(err)
        if (err.error?.response?.content)
          dfm.toastError(err.error.response.content)
        else
          dfm.toastError(err)

        tpl.$('.action-result').show();
        tpl.$('.result').text('http get FAILED! ' + err);
        dfm.logErr(err);
      }
    })

  },

  'click .js-action-delete': function (e, tpl) {

    const that = this;
    dfm.confirm('Supprimer l\'action ?', function (result) {
      if (result) {
        Meteor.call('buttonDelete', that, function (err, result) {
          if (!err) {
            dfm.log(JSON.stringify(result));
            if (result && result.statusCode === 200) {
              dfm.log('http get SUCCES');
              tpl.$('.action-result').show();
              tpl.$('.result').text(result.content);
            }
          }
          else {
            tpl.$('.action-result').show();
            tpl.$('.result').text('http get FAILED! ' + err);
            dfm.logError(JSON.stringify(err))
          }
        })
      }
    });
  },

  'click .js-scenario-enable': function (e, tpl) {
    Meteor.call('scenario.status', this._id, true, function (err) {
      if (err) return appHd.logsError('scenario.enable');
    });
  },

  'click .js-scenario-disable': function (e, tpl) {
    Meteor.call('scenario.status', this._id, false, function (err) {
      if (err) return appHd.logsError('scenario.disabled');
    });
  },
});