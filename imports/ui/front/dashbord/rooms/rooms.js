/**
 * Created by David FAIVRE-MAÇON
 * https://www.davidfm.me
 */

import './room.js';
import './rooms.html';

Template.dashboardRooms.onCreated(function () {
});

Template.dashboardRooms.onRendered(function () {
});

Template.dashboardRooms.helpers({

  rooms: function () {
    return Rooms.find({ show: true }, { sort: { order: 1 } });
  },

  keys: function () {
    return Keys.find({ room: this._id, show: true });
  },
});

Template.dashboardRooms.events({

  'click [data-bs-toggle=tab]': function (e) {
    e.preventDefault();

    const tab = $(e.currentTarget).attr('aria-controls');
    Meteor.users.update({ _id: Meteor.userId() }, { $set: { "profile.lastRoomTabShowed": tab } });
    $('[aria-controls=' + tab + ']').tab('show');
  },
});

// dashboardRoomsTabs

Template.dashboardRoomsTabs.onRendered(function () {

  const up = Meteor.user().profile;
  if (up && up.lastRoomTabShowed) {
    //FIXME verifier si la tab exist
    $('[aria-controls=' + up.lastRoomTabShowed + ']').tab('show');
  }
  else $('#roomsTabs li').first().find('a').tab('show');
});