/**
 * Created by DavidFM on 04/06/2020.
 */

import './nabaztag.html';

Template.tplNabaztag.onRendered(function () {
  console.log(this)
});

Template.tplNabaztag.helpers({

  keys() {
    return Keys.find({hard_id: this._id, hidden: {$ne: true}}, {sort: {show: -1, order: 1}});
  }
});

Template.tplNabaztag.events({

  'click .js-command': function (e, tpl) {
    const command = $(e.currentTarget).attr('data-command');
    $('textarea[name=nadb]').val(command);
  },

  'submit #nadb'(e, tpl) {
    e.preventDefault();

    const form = tpl.$('#nadb').serializeJSON();
    console.log(form);
    if(!form.nadb) return;

    appHd.nabaztag.command(this, form.nadb, function (err) {
      if(err) console.log(err);
    });
  }

});
