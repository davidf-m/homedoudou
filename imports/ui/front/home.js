/**
 * Created by David FAIVRE-MAÇON
 * https://www.davidfm.me
 */

import './home.html';

Template.home.onCreated(function () {
});

Template.home.helpers({

  news() {
    return News.find({}, {sort: {flash: -1, created: -1}});
  },

  lastDataKeys() {
    return Keys.find({}, {sort: {lastDate: -1}, limit: 10});
  }
});

Template.home.events({});

