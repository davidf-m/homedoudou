/**
 * Created by David FAIVRE-MAÇON on 24 january 2020
 * https://www.davidfm.me
 */

import './value.js';
import './display-key.html';

Template.displayKey.helpers({

  hardHidden() {
    return this.hideHard ? 'hide-hard' : '';
  },

  keyType() {
    const type = Configs.findOne({sourceId: "keyType", value: this.key.type})
    return type ? '<i class="' + type.icon + '"></i>' : '';
  },

  status() {
    const type = Configs.findOne({sourceId: "keyType", value: this.key.type});
    return type ? '<i class="' + type.icon + '"></i>' : '';
  },
});

Template.displayKeyLast.helpers({

  isKeyLastOne() {
    const lastValue = this.lastData?.value
    return lastValue * 1 === 1;
  }
});