/**
 * Created by David FAIVRE-MAÇON on 31 december 2020
 * https://www.davidfm.me
 */

import './value.html';

Template.displayKeyValue.onCreated(function() {
});

Template.displayKeyValue.onRendered(function() {});

Template.displayKeyValue.helpers({

  evolution() {
    if(isNaN(this.lastData?.value) || isNaN(this.beforeLast?.value)) return '';
    if(this.lastData.value > this.beforeLast.value)
      return '<small><i class="fa fa-arrow-up" style="font-size:smaller"></i></small>'
    else if(this.lastData.value < this.beforeLast.value)
      return '<small><i class="fa fa-arrow-down" style="font-size:smaller"></i></small>'
    else
      return '<small><i class="fa fa-arrow-right" style="font-size:smaller"></i></small>'
  }
});

Template.displayKeyValue.events({
});