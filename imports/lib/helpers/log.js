if(typeof (appHd) === "undefined") appHd = {};

/* custom console.log
* use blackboxing file source to preserve line number
*/

appHd.log = function (args) {
  if(typeof args === "string")
    console.log("%c" + args, "background-color: black; color: #0f0");
  else
    console.log("%cObject", "background-color: black; color: #0f0", args);
}