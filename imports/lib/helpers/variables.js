globalThis.appHd = globalThis.appHd || {};

appHd.dateFormatsMoment = {
  short: "DD MMMM YYYY",
  long: "dddd DD/MM/YYYY HH:mm",
  dh: "DD MMMM YYYY HH:mm",
  hour: "HH:mm",
  precis: "dddd DD/MM/YYYY HH:mm:ss"
};

// moment.locale('fr', {
//   relativeTime: {
//     future: "dans %s",
//     past: "%s",
//     s: "",
//     m: "1 min",
//     mm: "%d min",
//     h: "1 heure",
//     hh: "%d heures",
//     d: "1 jour",
//     dd: "%d jours",
//     M: "1 mois",
//     MM: "%d mois",
//     y: "1 an",
//     yy: "%d ans"
//   }
// });

// moment.locale(window.navigator.language);

appHd.editingRoom = new ReactiveVar();

appHd.buttons = {
  types: [
    { type: 'single', name: 'simple' },
    { type: 'boolean', name: 'on / off' },
    { type: 'free', name: 'libre' },
  ]
}

appHd.dateFormats = {
  hm: { hour: "numeric", minute: "numeric" },
  vshort: { day: "numeric", month: "numeric" },
  short: { day: "numeric", month: "numeric", year: "numeric" },
  short2: { day: "2-digit", month: "numeric", year: "numeric" },
  shortFullTime2: { day: "2-digit", month: "2-digit", year: "numeric", hour: "numeric", minute: "numeric" },
  medium: { day: "numeric", month: "long", year: "numeric" },
  medium2: { day: "2-digit", month: "long", year: "numeric" },
  mediumWeekday: { weekday: "long", day: "numeric", month: "long", year: "numeric" },
  long: { weekday: "long", day: "numeric", month: "long", year: "numeric", hour: "numeric", minute: "numeric" },
  full: { weekday: "long", day: "numeric", month: "long", year: "numeric", hour: "numeric", minute: "numeric", second: "numeric" },
};

appHd.configPages = [
  { name: "home", class: "color-front", title: "Accueil" },
  { name: "contact", class: "color-front", title: "Contact" },
  { name: "dashboard", class: "color-front", title: "Tableau de Bord" },
  { name: "configuration", class: "color-config", title: "Configuration" },
  { name: "configurationHardware", class: "color-config", title: "Matériels" },
  { name: "configurationHardwareAdd", class: "color-config", title: "Matériel Ajout" },
  { name: "configurationHardwareEdit", class: "color-config", title: "Modification matériel" },
  { name: "configurationHardwareKeyEdit", class: "color-config", title: "Modification clé" },
  { name: "configurationHardwareLogs", class: "color-config", title: "Logs" },
  { name: "configurationHardwareDevices", class: "color-config", title: "Périphériques" },
  { name: "configurationBootsServer", class: "color-config", title: "Boots server" },
  { name: "configurationScenariosList", class: "color-config", title: "Scénarios List" },
  { name: "configurationRooms", class: "color-config", title: "Groupes" },
  { name: "configurationNotifs", class: "color-config", title: "Notifications" },
  { name: "configurationScrapers", class: "color-config", title: "Scrappers" },
  { name: "adminHardwares", class: "color-admin", title: "Matériels" },
  { name: "adminDdp", class: "color-admin", title: "DDP" },
  { name: "adminLogs", class: "color-admin", title: "Logs" },
  { name: "adminMessages", class: "color-admin", title: "Messages" },
  { name: "adminDevice", class: "color-admin", title: "Périphériques" },
];

appHd.keyTypes = [
  { type: "temp", icon: "device_thermostat", name: "Température" },
  { type: "hum", icon: "humidity_mid", name: "Humidité" },
  { type: "watts", icon: "bolt", name: "Puissance" },
  { type: "light", icon: "emoji_objects", name: "Lumière" },
  { type: "brightness", icon: "brightness_medium", name: "Luminosité" },
  { type: "vent", icon: "air", name: "Vent" },
  { type: "boolean", icon: "toggle_on", name: "Binaire" },
  { type: "inversed-boolean", icon: "toggle_off", name: "Binaire inversé" },
];

appHd.keyTypesButton = [
  { type: "heater", icon: "device_thermostat", name: "Température" },
  { type: "light", icon: "emoji_objects", name: "Lumière" }
];

appHd.keyStyles = [
  { value: 'stock', name: 'Courbe', icon: 'monitoring' },
  { value: 'creneau', name: 'Creneau', icon: 'show_chart' },
  { value: 'pie', name: 'Camembert', icon: 'pie_chart' },
  { value: 'column', name: 'Colone', icon: 'bar_chart' },
  { value: 'column_cumul', name: 'Colone, données cumulées', icon: 'bar_chart' },
  { value: 'column_hour', name: 'Colone, données par heure', icon: 'bar_chart' },
  { value: 'info', name: 'Info', icon: 'info' },
];
