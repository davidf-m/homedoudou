// NAMESPACE
import { Meteor } from "meteor/meteor";
import "./log.js";

if (typeof (appHd) === "undefined") appHd = {};

/*
 Use Cases arrayObjectIndexOf:
 arrayObjectIndexOf([2,4,5],4) ;//OUTPUT 1
 arrayObjectIndexOf([{slm:2},{slm:4},{slm:5}],4,'slm');//OUTPUT 1
 arrayObjectIndexOf([{slm:2},{slm:4},{slm:5}],4,function(e,i){return e.slm;});//OUTPUT 1
 arrayObjectIndexOf([{slm:{salat:2}},{slm:{salat:4}},{slm:{salat:5}}],4,function(e,i){return e.slm.salat;});//OUTPUT 1
 */
appHd.arrayObjectIndexOf = function (a, e, fn) {
  if (!a) return console.log('Call of appHd.arrayObjectIndexOf of undefined');
  if (typeof a !== 'object') return console.log('Call of appHd.arrayObjectIndexOf without array');
  if (!fn) {
    return a.indexOf(e);
  }
  else {
    if (typeof fn === 'string') {
      const att = fn;
      fn = function (e) {
        return e[att];
      };
    }
    return a.map(fn).indexOf(e);
  }
};

appHd.isConnected = function () {
  return !!Meteor.user();
};

appHd.aliasname = function (that) {
  if (!that) return '--';
  if (that.alias) return that.alias;
  else if (that.name) return that.name;
  else return 'johnDoe';
};

appHd.encode_utf8 = function (s) {
  return encodeURIComponent(s);
};

appHd.decode_utf8 = function (s) {
  return decodeURIComponent(s);
};

appHd.isFreeConfig = function (u) {
  const uf = u ? u.freeConfig : false;
  return uf && uf.login && uf.key ? uf : false;
};

appHd.hardHasDeviceType = function (hardOrHardId, deviceType) {
  let hard = hardOrHardId;
  if (typeof hardOrHardId === 'string') hard = Hardwares.findOne(hardOrHardId);
  return (hard && hard.devices && hard.devices.length) ? appHd.arrayObjectIndexOf(hard.devices, deviceType, 'type_id') > -1 : false;
};

appHd.isUserPresentAt = function (u, hard) {
  return false;
  // if (hard.ip notifiable)
  if (!u || !hard.devices) return false;
  const devices = hard.devices.filter(function (d) {
    return d.type_id === 185 && d.disableNotifications
  });
  return false;
};

appHd.getMyKeys = function () {
  const hardsPublic = Hardwares.find({ owner_id: Meteor.userId() }).fetch();
  let hard_ids = [];
  _.each(hardsPublic, function (Hard) {
    hard_ids.push(Hard._id);
  });
  return Keys.find({ hard_id: { $in: hard_ids } }, { sort: { alias: 1, name: 1 } });
};

appHd.getMyTempKeys = function () {
  const hardsPublic = Hardwares.find({ owner_id: Meteor.userId() }).fetch();
  let hard_ids = [];
  _.each(hardsPublic, function (Hard) {
    hard_ids.push(Hard._id);
  });
  return Keys.find({ hard_id: { $in: hard_ids }, type: "temp" });
};

appHd.getMyKeysByHardId = function (hard_id) {
  return Keys.find({ hard_id: hard_id });
};

appHd.getHardOneByKeyId = function (keyId) {
  const keyOne = Keys.findOne(keyId);
  return Hardwares.findOne(keyOne.hard_id);
};

appHd.getMyHardwares = function () {
  return Hardwares.find({ "owner_id": Meteor.userId() }, { sort: { order: 1 } });
};

appHd.getMyKeysByRoomId = function (roomId) {
  return Keys.find({ rooms: roomId }, { sort: { show: -1, order: 1 } });
};

appHd.getMyScenarios = function () {
  return Scenarios.find({ scenarioType: "scenario", "createdBy": Meteor.userId() });
};

appHd.getMyScenariosByRoomId = function (roomId) {
  return Scenarios.find({ rooms: roomId }, { sort: { orderScenarios: "1" } });
};

appHd.getMyActions = function () {
  return Scenarios.find({ scenarioType: "action", "createdBy": Meteor.userId() }, { sort: { orderScenarios: 1 } });
};

appHd.getAction = function (actionId) {
  return Scenarios.findOne(actionId);
};

appHd.getMyButtons = function (roomId) {
  if (roomId)
    return Buttons.find({ "createdBy": Meteor.userId(), rooms: roomId }, { sort: { order: -1, name: 1 } });
  else
    return Buttons.find({ "createdBy": Meteor.userId() }, { sort: { order: -1, name: 1 } });
};

appHd.getRoom = function (id) {
  return Rooms.findOne(id);
};

appHd.getKeyById = function (keyId) {
  return Keys.findOne(keyId);
};

appHd.getKeysByHardId = function (hard_id, options) {
  let query = {
    hard_id: hard_id,
    hidden: { $ne: true }
  };
  if (options) query = { ...query, ...options };
  return Keys.find(query);
};

appHd.logs = function (objOrString, options) {
  //  objOrString = {
  //   ip: ip,
  //   hard_id: String,
  //   logs: [{log:""}],
  //   tags: [String]
  // };
  if (typeof objOrString === 'string') objOrString = { log: objOrString };
  if (objOrString.enabled !== false)
    Meteor.callAsync('logs.insert', objOrString, options);
  if (options?.console) appHd.log(JSON.stringify(objOrString))
};

appHd.logsInfo = function (objOrString) {
  if (typeof objOrString === "string") {
    objOrString = {
      logs: [objOrString],
      tags: ['info'],
    }
  }
  else {
    if (!objOrString.tags) objOrString.tags = [];
    if (typeof objOrString.tags === "string") objOrString.tags = [objOrString.tags];
    if (objOrString.tags.indexOf("error") === -1) objOrString.tags.push('info')
  }
  appHd.logs(objOrString)
};

appHd.logsError = function (objOrString, options) {
  if (typeof objOrString === "string") {
    objOrString = {
      logs: [objOrString],
      tags: ['error'],
    }
  }
  else {
    if (!objOrString.tags) objOrString.tags = [];
    if (typeof objOrString.tags === "string") objOrString.tags = [objOrString.tags];
    if (objOrString.tags.indexOf("error") === -1) objOrString.tags.push('error')
  }
  appHd.logs(objOrString, options)
};

appHd.getKeyId = function (hard_id, key_name) {
  if (hard_id && key_name) {
    const config = Keys.findOne({ hard_id: hard_id, name: key_name });
    return config ? config._id : false;
  }
  else if (!key_name) {
    return Keys.find({ hard_id: hard_id }).map(function (k) {
      return k._id;
    });
  }
  else return 'error';
};

appHd.hasRightOnScenario = function (scenarioId) {
  const scenarioOne = Scenarios.findOne(scenarioId);
  if (!scenarioOne) return false;

  /*
  j'ai les droits si :
  - ce screnario est moi
  - si le type est libre et une room qui contient ce scenario est partagée avec moi.
  - si le type est heater et qu'un bouton d'une room partagée avec moi contient ce sceanario
   */

  // - ce screnario est moi?
  if (scenarioOne.createdBy === Meteor.userId()) return true;

  // - si une room qui contient ce scenario est partagée avec moi.
  const roomSharedWithMe = Rooms.find({ shared: Meteor.userId() }, { fields: { _id: 1 } }).map((r) => {
    return r._id
  });

  if (scenarioOne.type === 'free') {
    // ce scenario est-il dans une piece partagée avec moi? //FIXME make better?
    const exist = Scenarios.findOne({ _id: scenarioId, rooms: { $in: roomSharedWithMe } });
    if (exist) return true;
  }

  // - si le type est heater et qu'un bouton d'une room partagée avec moi contient ce sceanario
  if (scenarioOne.type === 'heater') {
    const exist = Buttons.findOne({ on: scenarioOne.heaterOn, off: scenarioOne.heaterOff, rooms: { $in: roomSharedWithMe } })
    if (exist) return true;
  }

}

if (!appHd.scenarios) appHd.scenarios = {};

appHd.scenarios.types = [
  { type: "free", text: "Libre" },
  { type: "heater", text: "Chauffage" },
  { type: "cron", text: "Cron" },
];
appHd.scenarios.events = [
  { value: "strictly-positive", text: "Valeur > 0" },
  { value: "null", text: "Valeur = 0" },
  { value: "different", text: "Valeur differente" },
  { value: "superior-to", text: "Valeur > x" },
  { value: "equal-to", text: "Valeur = x" },
  { value: "inferior-to", text: "Valeur < x" },
  { value: "all-data-on", text: "Tous les capteurs à 1" },
  { value: "all-data-off", text: "Tous les capteurs à 0" },
  { value: "not-all-data-on", text: "Pas tous les capteurs à 1" },
  { value: "not-all-data-off", text: "Pas tous les capteurs à 0" },
];